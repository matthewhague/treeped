/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// RemoplaAssignStmt.java
// Author: Matthew Hague
//
// Assign statements in Remopla
// They are parallel, hence we have a HashMap from vars to expressions
// We are ignoring the quantification for now


package treeped.representation;

import java.lang.StringBuffer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

public class RemoplaAssignStmt extends RemoplaStmt {

    static Logger logger = Logger.getLogger(RemoplaAssignStmt.class);

    public class Assign {
        public RemoplaExpr lhs;
        public RemoplaExpr rhs;

        public Assign(RemoplaExpr lhs, RemoplaExpr rhs) {
            this.lhs = lhs;
            this.rhs = rhs;
        }

        public RemoplaExpr getLeft() {
            return lhs;
        }

        public void setLeft(RemoplaExpr left) {
            assert left != null;
            this.lhs = left;
        }

        public RemoplaExpr getRight() {
            return rhs;
        }

        public void setRight(RemoplaExpr right) {
            assert right != null;
            this.rhs = right;
        }

    }

    private List<Assign> assignments = new ArrayList<Assign>();

    public RemoplaAssignStmt() {
    }

    /** Convenience constructor: call addParallelAssignment on the args **/
    public RemoplaAssignStmt(RemoplaExpr lhs, RemoplaExpr rhs) {
        addParallelAssignment(lhs, rhs);
    }

    /** Convenience constructor: call addForAllParallelAssignment on the args
     */
    public RemoplaAssignStmt(RemoplaIntVar ri, int min, int max, RemoplaExpr lhs, RemoplaExpr rhs) {
        addForAllParallelAssignment(ri, min, max, lhs, rhs);
    }



    /** Remopla allows parallel assignments.  A RemoplaAssignStmt contains a
      * number of parallel lhs = rhs assignments.  Use this method to add to the
      * assignments
      *  @param lhs the variable/expression being assigned to
      *  @param rhs the new value of the variable (in expression form)
      */
    public void addParallelAssignment(RemoplaExpr lhs, RemoplaExpr rhs) {
        assert lhs != null && rhs != null;
        assignments.add(new Assign(lhs, rhs));
    }

    public List<Assign> getAssignments() {
        return assignments;
    }

    /** Remopla allows quantified assignments.  Currently we only support the
     * for all quantified assignment, and it is rather stupidly unrolled into
     * all instantiations.
     * @param ri the variable being quantified over
     * @param min the min bound on i
     * @param max the max bound on i
      *  @param lhs the variable/expression being assigned to
      *  @param rhs the new value of the variable (in expression form)
      */
    public void addForAllParallelAssignment(RemoplaIntVar ri, int min, int max, RemoplaExpr lhs, RemoplaExpr rhs) {
        assert ri != null && min <= max && lhs != null && rhs != null;
        VariableReplacer rep = new VariableReplacer();
        for (int i = min; i <= max; i++) {
            RemoplaIntConstant ival = new RemoplaIntConstant(i);
            assignments.add(new Assign(rep.replaceVar(lhs, ri, ival), rep.replaceVar(rhs, ri, ival)));
        }
    }


    /** Writes the body of a statement to a string buffer.
      * That is, the statement without labels or other paraphernalia.
      *  @param out the buffer
      *  @return the buffer
      */
    public StringBuffer toStringBody(StringBuffer out) {
        if (assignments.size() == 0) {
            (new RemoplaSkipStmt()).toString(out);
        } else {
            Iterator i = assignments.iterator();
            while(i.hasNext()) {
                Assign a = (Assign)i.next();
                a.lhs.toString(out);
                out.append(" = ");
                a.rhs.toString(out);
                if(i.hasNext())
                    out.append(", ");
            }
            out.append(";");
        }
        return out;
    }

    public void accept(RemoplaStmtVisitor visitor) {
        visitor.visit(this);
    }


    private class VariableReplacer extends AbstractRemoplaExprVisitor {

        private RemoplaExpr replaced;
        private RemoplaIntVar v;
        private RemoplaIntConstant i;

        /** returns a copy of e with instances of v replaced by i
         * Does not copy variables, &c.  Just the expression constructions.
         * That is, a binary expression a op b will return a new binary
         * expression a' op b' where a' and b' are as expected.  If either a or
         * b is just a variable, or constant, then a is a' or b is b'.
         */
        public RemoplaExpr replaceVar(RemoplaExpr e, RemoplaIntVar v, RemoplaIntConstant i) {
            assert v != null && i != null;
            RemoplaIntVar ov = this.v;
            RemoplaIntConstant oi = this.i;
            this.v = v;
            this.i = i;
            if (e != null)
                e.accept(this);
            else
                replaced = null;
            this.v = ov;
            this.i = oi;
            return replaced;
        }

        public void visit(RemoplaArrayRefExpr obj) {
            if (obj.getDimension() == 1) {
                RemoplaExpr index = replaceVar(obj.getIndex(), v, i);
                replaced = new RemoplaArrayRefExpr(obj.getArray(), index);
            } else {
                RemoplaExpr index = replaceVar(obj.getIndex(), v, i);
                RemoplaExpr index2 = replaceVar(obj.getIndex2(), v, i);
                replaced = new RemoplaArrayRefExpr(obj.getArray(), index, index2);
            }
        }

        public void visit(RemoplaBinExpr obj) {
            RemoplaExpr l = replaceVar(obj.getLeft(), v, i);
            RemoplaExpr r = replaceVar(obj.getRight(), v, i);
            replaced = new RemoplaBinExpr(l, obj.getType(), r);
        }


        public void visit(RemoplaIntVar obj) {
            if (obj.equals(v))
                replaced = i;
            else
                replaced = obj;
        }

        public void visit(RemoplaUnaryExpr obj) {
            RemoplaExpr e = replaceVar(obj.getExpr(), v, i);
            replaced = new RemoplaUnaryExpr(obj.getType(), e);
        }

        public void defaultCase(RemoplaExpr obj) {
            replaced = obj;
        }
    }

}
