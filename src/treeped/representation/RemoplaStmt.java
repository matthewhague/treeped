/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// RemoplaStmt.java
// Author: Matthew Hague
//
// The information concerning a particular Remopla translation of a Jimple
// statement

package treeped.representation;

import java.lang.StringBuffer;

import java.util.List;

import org.apache.log4j.Logger;

public abstract class RemoplaStmt {

    static Logger logger = Logger.getLogger(RemoplaStmt.class);

    private String label = null;
    private String comment = null;

    /** Writes the statement to a string (including terminating semi-colon)
      *  @param out the buffer to write to
      *  @return the buffer
      */
    public StringBuffer toString(StringBuffer out) {
        if (label != null)
            out.append(label).append(": ");

        toStringBody(out);

        if (comment != null)
            out.append("   ## ").append(comment).append(" ##");

        return out;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public String toString() {
        return toString(new StringBuffer()).toString();
    }


    /** @return The label associated with the statement, null if none exists
      */
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    /** puts statement and all substatements into stmts **/
    public void flatten(List<RemoplaStmt> stmts) {
        assert stmts != null;
        stmts.add(this);
    }

    /** Writes the body of a statement to a string buffer.
      * That is, the statement without labels or other paraphernalia.
      *  @param out the buffer
      *  @return the buffer
      */
    public abstract StringBuffer toStringBody(StringBuffer out);

    public abstract void accept(RemoplaStmtVisitor visitor);
}
