/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// RemopleInvokeStmt.java
// Author: Matthew Hague
//
// Invoke statement class

package treeped.representation;
import java.util.List;

public class RemoplaInvokeStmt extends RemoplaStmt {
    RemoplaMethodCall invokeExpr;

    public RemoplaInvokeStmt(RemoplaMethodCall invokeExpr) {
        assert invokeExpr != null;
        this.invokeExpr = invokeExpr;
    }

    public RemoplaMethodCall getInvokeExpr() {
        return invokeExpr;
    }


    /** Writes the body of a statement to a string buffer.
      * That is, the statement without labels or other paraphernalia.
      *  @param out the buffer
      *  @return the buffer
      */
    public StringBuffer toStringBody(StringBuffer out) {
        return invokeExpr.toString(out).append(";");
    }

    public void accept(RemoplaStmtVisitor visitor) {
        visitor.visit(this);
    }


}
