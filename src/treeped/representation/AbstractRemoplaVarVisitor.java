/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// AbstractRemoplaVarVisitor.java
// Author: Matthew Hague
//
// Implements a default variable visitor that calls the default method unless
// overridden.

package treeped.representation;


public abstract class AbstractRemoplaVarVisitor implements RemoplaVarVisitor {
    public void visit(RemoplaArrayVar obj) { defaultCase(obj); }
    public void visit(RemoplaBoolVar obj) { defaultCase(obj); }
    public void visit(RemoplaEnumVar obj) { defaultCase(obj); }
    public void visit(RemoplaIntVar obj) { defaultCase(obj); }
    public void visit(RemoplaStructVar obj) { defaultCase(obj); }
    public void visit(RemoplaUnsupportedVar obj) { defaultCase(obj); }

    public abstract void defaultCase(RemoplaVar obj);
}
