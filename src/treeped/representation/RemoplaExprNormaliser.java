/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// RemoplaExprNormaliser.java
// Author: Matthew Hague
//
// Takes an expression and returns a "normalised" expression (pushes in
// negations and what not)

package treeped.representation;

import org.apache.log4j.Logger;

import static treeped.representation.RemoplaStaticFactory.*;

public class RemoplaExprNormaliser extends AbstractRemoplaExprVisitor {
    static Logger logger = Logger.getLogger(RemoplaExprNormaliser.class);


    RemoplaExpr visitResult = null;

    /**
     * @param a
     *            remopla expression to normalise
     * @return an equivalent expression that has some redundancy removed (e.g. x
     *         + (-1) becomes (x - 1). The expression does not change the given
     *         expression (e), but may share sub-formulae.
     */
    public RemoplaExpr normaliseExpression(RemoplaExpr e) {
        e.accept(this);
        return visitResult;
    }

    /**
     * {@inheritDoc}
     *
     * @see RemoplaExprVisitor#visit(RemoplaArrayRefExpr)
     */
    public void visit(RemoplaArrayRefExpr obj) {
        if (obj.getDimension() == 1) {
            RemoplaExpr index = normaliseExpression(obj.getIndex());
            setResult(aRef(obj.getArray(), index));
        } else {
            RemoplaExpr index = normaliseExpression(obj.getIndex());
            RemoplaExpr index2 = normaliseExpression(obj.getIndex2());
            setResult(aRef(obj.getArray(), index, index2));
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see RemoplaExprVisitor#visit(RemoplaBinExpr)
     */
    public void visit(RemoplaBinExpr obj) {
        RemoplaExpr left = normaliseExpression(obj.getLeft());
        RemoplaExpr right = normaliseExpression(obj.getRight());

        if (isArithBinExp(obj)) {
            boolean leftIntConst = isIntConstant(left);
            boolean rightIntConst = isIntConstant(right);

            if (leftIntConst && rightIntConst) {
                setResult(evaluateConstBinExp(obj, getIntConstValue(left), getIntConstValue(right)));
            } else if (leftIntConst) {
                setResult(normaliseLeftConstArith(obj, getIntConstValue(left)));
            } else if (rightIntConst) {
                setResult(normaliseRightConstArith(obj, getIntConstValue(right)));
            } else {
                setResult(new RemoplaBinExpr(left, obj.getType(), right));
            }
        } else {
            setResult(new RemoplaBinExpr(left, obj.getType(), right));
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see RemoplaExprVisitor#visit(RemoplaUnaryExpr)
     */
    public void visit(RemoplaUnaryExpr obj) {
        RemoplaExpr e = normaliseExpression(obj.getExpr());
        if (obj.getType() == RemoplaUnaryExpr.Op.NEG) {
            if (e instanceof RemoplaBoolConstant) {
                setResult(rbool(!((RemoplaBoolConstant)e).getVal()));
            } else if (e instanceof RemoplaBinExpr) {
                setResult(negateBinExpr((RemoplaBinExpr)e));
            } else if (e instanceof RemoplaUnaryExpr) {
                 setResult(negateUnaryExpr((RemoplaUnaryExpr)e));
            } else {
                setResult(new RemoplaUnaryExpr(obj.getType(), e));
            }
        } else {
            setResult(new RemoplaUnaryExpr(obj.getType(), e));
        }
    }

    /**
     * {@inheritDoc}
     *
     * @see AbstractRemoplaExprVisitor#defaultCase(RemoplaExpr)
     */
    public void defaultCase(RemoplaExpr obj) {
        setResult(obj);
    }


    ////////////////////////////////////////////////////////////////////
    // Private methods
    //

    private void setResult(RemoplaExpr e) {
        visitResult = e;
    }

    private RemoplaExpr negateBinExpr(RemoplaBinExpr be) {
        RemoplaExpr negated;
        switch (be.getType()) {
            case AND:
                negated = or(normaliseExpression(not(be.getLeft())),
                             normaliseExpression(not(be.getRight())));
                break;

            case OR:
                negated = and(normaliseExpression(not(be.getLeft())),
                              normaliseExpression(not(be.getRight())));
                break;

            case EQ:
                negated = neq(normaliseExpression(be.getLeft()),
                              normaliseExpression(be.getRight()));
                break;

            case NEQ:
                negated = eq(normaliseExpression(be.getLeft()),
                             normaliseExpression(be.getRight()));
                break;

            case LT:
                negated = gte(normaliseExpression(be.getLeft()),
                              normaliseExpression(be.getRight()));
                break;

            case LTE:
                negated = gt(normaliseExpression(be.getLeft()),
                             normaliseExpression(be.getRight()));
                break;

            case GT:
                negated = lte(normaliseExpression(be.getLeft()),
                              normaliseExpression(be.getRight()));
                break;

            case GTE:
                negated = lt(normaliseExpression(be.getLeft()),
                             normaliseExpression(be.getRight()));
                break;

            default:
                negated = not(be);
        }
        return negated;
    }


    private RemoplaExpr negateUnaryExpr(RemoplaUnaryExpr ue) {
        RemoplaExpr negated;
        switch (ue.getType()) {
            case NEG:
                negated = ue.getExpr();
                break;

            default:
                negated = not(ue);
                break;
        }
        return negated;
    }


    private boolean isIntConstant(RemoplaExpr e) {
        return (e instanceof RemoplaIntConstant) || (e instanceof RemoplaIntConstVar);
    }

    private long getIntConstValue(RemoplaExpr e) {
        assert isIntConstant(e);
        if (e instanceof RemoplaIntConstant)
            return ((RemoplaIntConstant)e).getVal();
        else
            return ((RemoplaIntConstVar)e).getValue();
    }

    private boolean isArithBinExp(RemoplaBinExpr e) {
        boolean result;
        switch (e.getType()) {
            case ADD:
            case SUB:
            case MUL:
            case DIV:
            case GTE:
            case GT:
            case LT:
            case LTE:
            case SHL:
            case SHR:
                result = true;
                break;
            default:
                result = false;
                break;
        }
        return result;
    }

    private RemoplaExpr evaluateConstBinExp(RemoplaBinExpr e, long left, long right) {
        RemoplaExpr result = null;
        switch (e.getType()) {
            case ADD:
                result = rint(left + right);
                break;
            case SUB:
                result = rint(left - right);
                break;
            case MUL:
                result = rint(left * right);
                break;
            case DIV:
                result = rint(left / right);
                break;
            case GTE:
                result = rbool(left >= right);
                break;
            case GT:
                result = rbool(left > right);
                break;
            case LT:
                result = rbool(left < right);
                break;
            case LTE:
                result = rbool(left <= right);
                break;
            case SHL:
                result = rint(left<<right);
                break;
            case SHR:
                result = rint(left>>right);
                break;
            default:
                result = e;
                break;
        }
        return result;
    }


    private RemoplaExpr normaliseLeftConstArith(RemoplaBinExpr e, long left) {
        RemoplaExpr result = null;
        RemoplaExpr right = e.getRight();
        switch (e.getType()) {
            case ADD:
                if (left < 0) {
                    result = sub(right, rint(-left));
                } else {
                    result = e;
                }
                break;
            default:
                result = e;
                break;
        }
        return result;
    }

    private RemoplaExpr normaliseRightConstArith(RemoplaBinExpr e, long right) {
        RemoplaExpr result = null;
        RemoplaExpr left = e.getLeft();
        switch (e.getType()) {
            case ADD:
                if (right < 0) {
                    result = sub(left, rint(-right));
                } else {
                    result = e;
                }
                break;
            case SUB:
                if (right < 0) {
                    result = add(left, rint(-right));
                } else {
                    result = e;
                }
            default:
                result = e;
                break;
        }
        return result;
    }

}
