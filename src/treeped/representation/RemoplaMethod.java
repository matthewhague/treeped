/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// RemoplaMethod.java
// Author: Matthew Hague
//
// RemoplaMethod contains info on methods for the translation

package treeped.representation;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.lang.Exception;
import java.lang.StringBuffer;
import java.util.Collection;

import org.apache.log4j.Logger;

public class RemoplaMethod {

    public static enum ReturnType {
        INT("int")   { public RemoplaExpr getDummyValue() { return new RemoplaIntConstant(0); }
                       public RemoplaVar getReturnVar(String name) { return new RemoplaIntVar(name, true); } },
        BOOL("bool") { public RemoplaExpr getDummyValue() { return new RemoplaBoolConstant(false); }
                       public RemoplaVar getReturnVar(String name) { return new RemoplaBoolVar(name, true); } },
        VOID("void") { public RemoplaExpr getDummyValue() { return new RemoplaVoidConstant(); }
                       public RemoplaVar getReturnVar(String name) { return null; } };


        private final String display;

        ReturnType(String display) {
            this.display = display;
        }

        /** @return a constant expression that can be used in a return statement
         * (that is, "return <dummy value>")
         */
        public abstract RemoplaExpr getDummyValue();
        public abstract RemoplaVar getReturnVar(String name);
    }

    static Logger logger = Logger.getLogger(RemoplaMethod.class);

    ReturnType returnType;
    String methodName;
    // parameters, fetch-able by index
    List<RemoplaVar> parameters;
    // local vars, fetch-able by name
    Map<String, RemoplaVar> locals = new HashMap<String, RemoplaVar>();
    List<RemoplaStmt> stmts = new ArrayList<RemoplaStmt>();

    /** Create a RemoplaMethod object.
      *  @param returnType the return type (ReturnType.INT or BOOL or VOID) of
      *  the method
      *  @param methodName the name of the method, non-null
      *  @param parameters a list of parameters (non-null)
      *  parameters, assumed not to contain null items
      */
    public RemoplaMethod(ReturnType returnType,
                         String methodName,
                         List<RemoplaVar> parameters) {
        assert methodName != null && returnType != null && parameters != null;
        this.returnType = returnType;
        this.methodName = methodName;
        for (RemoplaVar par : parameters) {
            assert par != null;
            this.locals.put(par.getName(), par);
        }
        this.parameters = parameters;
    }

    /** Create a RemoplaMethod object.
      *  @param returnType the return type (ReturnType.INT or BOOL or VOID) of
      *  the method
      *  @param methodName the name of the method, non-null
      *  @param parameters . . . a list of parameters, may be null if there are no
      *  parameters, assumed not to contain null items
      */
    public RemoplaMethod(ReturnType returnType,
                         String methodName,
                         RemoplaVar... parameters) {
        assert methodName != null && returnType != null && parameters != null;
        this.parameters = new ArrayList<RemoplaVar>(parameters.length);
        for (int i = 0; i < parameters.length; i++) {
            this.parameters.add(parameters[i]);
            this.locals.put(parameters[i].getName(), parameters[i]);
        }
        this.returnType = returnType;
        this.methodName = methodName;
    }

    /** Writes the method to a string buffer
      *  @param out the buffer to write to
      *  @return the buffer
      */
    public StringBuffer toString(StringBuffer out) {
        assert out != null;
        writeHeader(out);
        for (RemoplaStmt stmt : stmts) {
            stmt.toString(out);
            out.append("\n");
        }
        writeFooter(out);
        return out;
    }

    public String toString() {
        return toString(new StringBuffer()).toString();
    }

    public String getName() {
        return methodName;
    }

    /** @return the string to use when referencing the method in a call
      */
    public String getCallString() {
        return methodName;
    }




    /** returns the RemoplaVar object associated with a local variable name
      * (note: does not search parameters)
      *  @param name variable name
      *  @return RemoplaVar object, or null if not found
      */
    public RemoplaVar getLocalVar(String name) {
        return locals.get(name);
    }

    public Collection<RemoplaVar> getLocalVars() {
        return locals.values();
    }

    /** adds a new local variable, throws an exception if it already exists
      *  @param var the variable, var.getLocal() must return true
      *  @param used whether the variable is used
      */
    public void addLocalVar(RemoplaVar var) throws Exception {
        assert var.getLocal();
        if (locals.containsKey(var.getName())) {
            throw new Exception("Method " +
                                methodName +
                                " already contains variable " +
                                var.getName() + ".");
        }

        locals.put(var.getName(), var);
    }


    /** append a statement to the method
      * @param stmt the statement to add
      */
    public void appendStatement(RemoplaStmt stmt) {
        assert stmt != null;
        stmts.add(stmt);
    }

    /** append a statement to the method
      * @param stmt the statement to add
      */
    public void appendStatements(List<RemoplaStmt> stmts) {
        assert stmts != null;
        for (RemoplaStmt s : stmts) {
            assert s != null;
        }
        this.stmts.addAll(stmts);
    }

    public int getNumParameters() {
        return parameters.size();
    }


    /** gets the parameter at the given index
      *  @param index the index
      *  @return the parameter, null if the index is out of range
      */
    public RemoplaVar getParameter(int index) {
        RemoplaVar par = null;
        if (0 <= index && index <= parameters.size())
            par = parameters.get(index);
        return par;
    }


    public List<RemoplaVar> getParameters() {
        return parameters;
    }


    /** Jimple equates a local var with each param var at the beginning of each
     * function.  This method allows a parameter to be overwritten with a local var.
     *   @param index the index to overwrite (doesn't check for type matches)
     *   @param var the var to overwrite the parameter with, must be a local var
     */
    public void setParameter(int index, RemoplaVar var) {
        assert 0 <= index && index <= parameters.size();
        assert locals.containsValue(var);
        locals.remove(parameters.get(index).getName());
        parameters.set(index, var);
    }


    public ReturnType getReturnType() {
        return returnType;
    }

    public RemoplaVar getDefaultReturnVar(String name) {
        RemoplaVar ret = getLocalVar(name);
        if (ret == null) {
            ret = returnType.getReturnVar(name);
            if (ret != null) {
                try {
                    addLocalVar(ret);
                } catch (Exception e) {
                    // should never happen, since we check existence first
                    logger.error("Error adding default return variable in RemoplaMethod.getDefaultReturnVar().");
                }
            }
        }
        return ret;
    }

    /** @return the declaration string of the method
     * That is, returnType name(args)
     * with no characters after the ).
     */
    public String getDeclaration() {
        return getDeclaration(new StringBuffer()).toString();
    }

    /** Writes the declaration string of the method to the string buffer.
     * That is, returnType name(args)
     * with no characters after the ).
     *  @param out the string buffer
     *  @return the string buffer
     */

    public StringBuffer getDeclaration(StringBuffer out) {
        out.append("module ").append(returnType.display).append(" ").append(methodName).append(" (");

        Iterator i = parameters == null ? null : parameters.iterator();

        if (i != null) {
            while (i.hasNext()) {
                RemoplaVar par = (RemoplaVar)i.next();
                par.getDeclaration(out);
                if (i.hasNext())
                    out.append(", ");
            }
        }

        out.append(")");

        return out;
    }


    public List<RemoplaStmt> getStmts() {
        return stmts;
    }

    /** Statements such as if have sub statements.  This method returns all
     * substatements too.
     *   @param stmts the list to store the statements into
     */
    public void getFlattenedStmts(List<RemoplaStmt> stmts) {
        for(RemoplaStmt s : getStmts()) {
            s.flatten(stmts);
        }
    }




    ////////////////////////////////////////////////////////////////////////////
    // Private methods
    //


    private void writeHeader(StringBuffer out) {
        getDeclaration(out);
        out.append(" {\n");

        for (RemoplaVar l : locals.values()) {
            if (l.getUsed()) {
                if (parameters == null || !parameters.contains(l)) {
                    l.getDeclaration(out);
                    out.append(";\n");
                }
            }
        }

        out.append("\n\n");
    }

    private void writeFooter(StringBuffer out) {
        out.append("}\n");
    }


}
