/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// RemoplaArrayVar.java
// Author: Matthew Hague
//
// A class for Remopla arrays
// The class is general enough for arrays of any dimension, but since Remopla
// only allows two-dimensional arrays at most, we enforce this condition in the
// constructor.


package treeped.representation;

import treeped.main.CmdOptions;

public class RemoplaArrayVar extends RemoplaVar {

    private int minIndex;
    private int maxIndex;
    private int minIndex2;
    private int maxIndex2;
    private RemoplaVar type;


     /** Constructs a Remopla array object.
      * The constructed array cannot be more than two-dimensional.
      *  @param type a variable representing the type of the array.  E.g. an
      *  array "int i[n]" has type "int i" -- not null (critical!) and not an
      *  array var
      *  @param minIndex the min index
      *  @param maxIndex the max index
      */
    public RemoplaArrayVar(RemoplaVar type, int minIndex, int maxIndex, boolean local) {
        this(type, minIndex, maxIndex, -1, -1, local, false);
    }

     /** Constructs a two dimensional Remopla array object.
      * The constructed array cannot be more than two-dimensional.
      *  @param type a variable representing the type of the array.  E.g. an
      *  array "int i[n]" has type "int i" -- not null (critical!) and not an
      *  array var
      *  @param minIndex the min index
      *  @param maxIndex the max index
      *  @param minIndex2 the second min index
      *  @param maxIndex2 the second max index
      */
    public RemoplaArrayVar(RemoplaVar type,
                           int minIndex, int maxIndex,
                           int minIndex2, int maxIndex2,
                           boolean local) {
        this(type, minIndex, maxIndex, minIndex2, maxIndex2, local, false);
    }

    /** Create an array without specifying its size.  Note that an array without
      * a specified size is unsupported and it will toString as such.  The size must
      * be set later.
      *  @param type the type of elements in the array
      */
    public RemoplaArrayVar(RemoplaVar type, boolean local) {
        this(type, -1, -1, -1, -1, local, false);
    }

    /** Constructs a Remopla array object.
      * The constructed array cannot be more than two-dimensional.
      *  @param type a variable representing the type of the array.  E.g. an
      *  array "int i[n]" has type "int i" -- not null (critical!) and not an
      *  array var
      *  @param minIndex the min index
      *  @param maxIndex the max index
      */
    public RemoplaArrayVar(RemoplaVar type, int minIndex, int maxIndex, boolean local, boolean used) {
        this(type, minIndex, maxIndex, -1, -1, local, used);
    }

     /** Constructs a two dimensional Remopla array object.
      * The constructed array cannot be more than two-dimensional.
      *  @param type a variable representing the type of the array.  E.g. an
      *  array "int i[n]" has type "int i" -- not null (critical!) and not an
      *  array var
      *  @param minIndex the min index
      *  @param maxIndex the max index
      *  @param minIndex2 the second min index
      *  @param maxIndex2 the second max index
      */
    public RemoplaArrayVar(RemoplaVar type,
                           int minIndex, int maxIndex,
                           int minIndex2, int maxIndex2,
                           boolean local,
                           boolean used) {
        // we're risking a null pointer error here, but Java won't have it any
        // other way.
        super(type.getName(), local, used);
        assert /*type != null &&*/ minIndex <= maxIndex;
        assert !(type instanceof RemoplaArrayVar);

        this.type = type;
        this.minIndex = minIndex;
        this.maxIndex = maxIndex;
        this.minIndex2 = minIndex2;
        this.maxIndex2 = maxIndex2;
        this.used = used;
    }

    /** Create an array without specifying its size.  Note that an array without
      * a specified size is unsupported and it will toString as such.  The size must
      * be set later.
      *  @param type the type of elements in the array
      */
    public RemoplaArrayVar(RemoplaVar type, boolean local, boolean used) {
        this(type, -1, -1, -1, -1, local, used);
    }


    public void setBounds(int minIndex, int maxIndex) {
        assert minIndex <= maxIndex;
        this.minIndex = minIndex;
        this.maxIndex = maxIndex;
        this.minIndex2 = -1;
        this.maxIndex2 = -1;
    }

    public void setBounds(int minIndex, int maxIndex, int minIndex2, int maxIndex2) {
        assert minIndex <= maxIndex && minIndex2 <= maxIndex2;
        this.minIndex = minIndex;
        this.maxIndex = maxIndex;
        this.minIndex2 = minIndex2;
        this.maxIndex2 = maxIndex2;
    }

    public RemoplaVar getType() {
        return type;
    }

    /** Set the min index of the array.  If minIndex > maxIndex, then maxIndex
      * is set to the new minIndex too.
      */
    public void setMinIndex(int minIndex) {
        this.minIndex = minIndex;
        if (maxIndex < minIndex)
            maxIndex = minIndex;
    }

    public void setMaxIndex(int maxIndex) {
        this.maxIndex = maxIndex;
    }

    public int getMinIndex() {
        return minIndex;
    }

    public int getMaxIndex() {
        return maxIndex;
    }

    /** Set the second min index of the array.  If minIndex > maxIndex, then maxIndex
      * is set to the new minIndex too.
      */
    public void setMinIndex2(int minIndex) {
        this.minIndex2 = minIndex;
        if (maxIndex2 < minIndex2)
            maxIndex2 = minIndex2;
    }

    public void setMaxIndex2(int maxIndex2) {
        this.maxIndex = maxIndex;
    }

    public int getMinIndex2() {
        return minIndex2;
    }

    public int getMaxIndex2() {
        return maxIndex2;
    }

    /** Returns true if a size for the array has been specified
      */
    public boolean sizeSet() {
        return (minIndex != -1 && maxIndex != -1);
    }

    public boolean minIndexSet() {
        return (minIndex != -1);
    }

    public boolean maxIndexSet() {
        return (maxIndex != -1);
    }

    /** Returns true if a size for the array has been specified
      */
    public boolean sizeSet2() {
        return (minIndex != -1 && maxIndex != -1);
    }

    public boolean minIndexSet2() {
        return (minIndex2 != -1);
    }

    public boolean maxIndexSet2() {
        return (maxIndex2 != -1);
    }

    /** @return the dimension of the array (one or two)a
      */
    public int getDimension() {
        if (minIndex2 > -1)
            return 2;
        else
            return 1;
    }


    /** Write the declaration prefix to a string buffer
      *  @param out the buffer
      *  @return the buffer
      */
    public StringBuffer getDeclarationPrefix(StringBuffer out) {
        if (minIndex != -1)
            type.getDeclarationPrefix(out);
        else // use default values
            getDefaultDeclarationPrefix(out);
        return out;
    }

    /** Write the declaration suffix to a string buffer.
      *  @param out the buffer
      *  @return the buffer
      */
    public StringBuffer getDeclarationSuffix(StringBuffer out) {
        if (minIndex == -1)
            getDefaultDeclarationSuffix(out);
        else if (minIndex == 0)
             out.append("[").append(maxIndex).append("]");
        else
            out.append("[").append(minIndex).append(",").append(maxIndex).append("]");

        return type.getDeclarationSuffix(out);
    }

    public void accept(RemoplaExprVisitor visitor) {
        visitor.visit(this);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////
    // Private Methods
    //


    /** Write the default declaration suffix to a string buffer.
      *  @param out the buffer
      *  @return the buffer
      */
    private StringBuffer getDefaultDeclarationSuffix(StringBuffer out) {
        return out.append("[-1, -1]");
    }


    /** Write the default declaration prefix to a string buffer.
      *  @param out the buffer
      *  @return the buffer
      */
    private StringBuffer getDefaultDeclarationPrefix(StringBuffer out) {
        return type.getDeclarationPrefix(out);
    }


    public void accept(RemoplaVarVisitor visitor) {
        visitor.visit(this);
    }


}
