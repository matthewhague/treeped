/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// RemoplaStructType.java
// Author: Matthew Hague
//
// A class for user defined struct types
// We make a simplifying assumption that structs are not nested.  That is, we
// only allow,
//
//      struct a {
//          int b,c;
//      }
//
//      struct d {
//          struct a e;
//      }
//
// and we don't allow,
//
//      struct d {
//          struct {
//              int b,c;
//          } e;
//      }
//
// Furthermore, we don't allow variable declarations after structs.  We must
// write,
//
//      struct a {
//          int b;
//      };
//
//      struct a c;
//
// not,
//
//      struct a {
//          int b;
//      } c;

package treeped.representation;

import java.util.Set;
import java.lang.StringBuffer;


public class RemoplaStructType extends RemoplaDeclaredType {

    private static final String STRUCT = "struct";

    private Set<RemoplaVar> fields;

    public RemoplaStructType(String name, Set<RemoplaVar> fields) {
        super(name);
        for (RemoplaVar var : fields) {
            assert var != null;
        }

        this.fields = fields;
    }

    public Set<RemoplaVar> getFields() {
        return fields;
    }


    /** Writes the type declaration to a string buffer
      *  @param out the buffer
      *  @return the buffer
      */
    public StringBuffer toString(StringBuffer out) {
        out.append(STRUCT).append(" ").append(this.getName()).append(" {\n");
        for(RemoplaVar var : fields) {
            var.getDeclaration(out).append(";\n");
        }
        return out.append("};");
    }


    /** Writes the Remopla declaration prefix string for a variable of the type
     * to the passed buffer
     *   @param out the string buffer to write to
     *   @return the string buffer
     */
    public StringBuffer getDeclarationPrefix(StringBuffer out) {
        return out.append(STRUCT).append(" ").append(this.getName());
    }
}
