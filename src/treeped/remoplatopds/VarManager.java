/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// VarManager
// Author: Matthew Hague (adapted from jMoped2)
//
// Re-implementation of the variable manager class for jMoped, replacing the
// bytecode/jMoped encoding stuff with our Remopla representation.
//
// I have removed multi-threading code.  It was confusing to leave it in.
//
// This class is used by BDDSemiring to handle the variables.


// TODO: move to de.tum.in.blah and make the public methods getting doms and
// stuff protected again


package treeped.remoplatopds;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import treeped.main.CmdOptions;
import treeped.exceptions.VariableNotFoundException;
import treeped.representation.AbstractRemoplaVarVisitor;
import treeped.representation.Remopla;
import treeped.representation.RemoplaArrayVar;
import treeped.representation.RemoplaBinExpr;
import treeped.representation.RemoplaBoolVar;
import treeped.representation.RemoplaEnumConstant;
import treeped.representation.RemoplaEnumVar;
import treeped.representation.RemoplaIntVar;
import treeped.representation.RemoplaMethod;
import treeped.representation.RemoplaStmt;
import treeped.representation.RemoplaStructVar;
import treeped.representation.RemoplaUnaryExpr;
import treeped.representation.RemoplaUnsupportedVar;
import treeped.representation.RemoplaVar;

import net.sf.javabdd.BDD.BDDIterator;
import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDDomain;
import net.sf.javabdd.BDDFactory;
import net.sf.javabdd.BDDPairing;
import net.sf.javabdd.BDDVarSet;

import org.apache.log4j.Logger;

public class VarManager {

    /** Value to use for value (must be passed through encode(FALSE, domain) **/
    public static final long FALSE = 0;

	// Integer bits
	private int bits;

	// 2^bits
	private long size;

	// 2^(bits-1) - 1
	private int maxint;

    // local variables max
	int lvmax;

	static int varcopy = 3;

	int globalcopy;

	/**
	 * Maps global variables to info
	 */
	private Map<RemoplaVar, Variable> globals;

    /**
     * Maps local variables to index information
     */
    private Map<RemoplaVar, Variable> locals;

    /**
     * Assigns integers to enum constants
     */
    private Map<RemoplaEnumConstant, Integer> enumValues;

    /**
     * Maps method names to their method
     */
    private Map<String, RemoplaMethod> callMethods;

	// Domains of all variables
	BDDDomain[] doms;

    // Temporary variable domains, used and unused
    ArrayList<BDDDomain> freeTempDomains = new ArrayList<BDDDomain>(15);
    ArrayList<BDDDomain> usedTempDomains = new ArrayList<BDDDomain>(15);

	// Return variable domain index
	private int retDomIndex;


	/**
	 * Number of globals
	 */
	int gnum;

	// Starting domain index of globals
	int g0;

	// Starting domain index of locals
	int l0;

	// BDDFactory
	BDDFactory factory;

	private int[] gindex;

	/*
	 * Maps a domain index of a variable to a variable set
	 * of all variables without that variable.
	 * Use for abstracting other variables away.
	 */
	private Map<Integer, BDDVarSet> varSetWithout;

	/**
	 * Maps names of constants to their values.
	 */
	private Map<String, Integer> constants = new HashMap<String, Integer>();

	/**
	 * Verbosity level.
	 */
	private static int verbosity = 0;

	/**
	 * The logger.
	 */
	static Logger logger = Logger.getLogger(VarManager.class);

	/**
	 * Records the maximum number of BDD nodes. For statistics purpose.
	 */
	private static int maxNodeNum;


    Map<RemoplaStmt, StmtOptimisations> stmtOptimisations;

    private String bddpackage;
    private int nodenum;
    private int cachesize;

	/**
	 * Constructs a variable manager.
	 *
	 * @param bddpackage the BDD package: "cudd" or "java".
	 * @param nodenum the estimated number of BDD nodes.
	 * @param cachesize the cache size.
	 * @param bits the number of variable bits.
	 * @param g the global variables.
	 * @param methods the methods of the program
	 */

//    Collection<RemoplaVar> g,
//                Collection<RemoplaMethod> methods)
	public VarManager(String bddpackage, int nodenum, int cachesize,
			int bits, Remopla remopla, Map<RemoplaStmt, StmtOptimisations> stmtOptimisations) {
		this.bits = bits;
		this.size = (long) Math.pow(2, bits);
		this.maxint = (int) size/2 - 1;
		this.globalcopy = varcopy;
        this.bddpackage = bddpackage;
        this.nodenum = nodenum;
        this.cachesize = cachesize;

        this.factory = BDDFactory.init(bddpackage, nodenum, cachesize);

        // This looks like it could be removed, but i don't understand the code
        // well enough to try...
	    gindex = new int[] { 0, 1, 2, 3, 4 };
        setRemopla(remopla, stmtOptimisations);
	}


    /**
     * Change the remopla for the manager, returns true if the variable domains
     * had to be changed, which means all previous bdds will no longer be
     * usable.  No change needed if new remopla variables are a subset of the
     * previous.
     *
     * @param remopla the new remopla
     * @param stmtOptimisations the optimisations for the remopla
     * @return true if remopla needed a change to the variable domains
     */
    public boolean setRemopla(Remopla remopla,
                              Map<RemoplaStmt, StmtOptimisations> stmtOptimisations) {
        assert remopla != null && stmtOptimisations != null;
        this.stmtOptimisations = stmtOptimisations;

        if (remoplaMatchesCurrentVars(remopla)) {
            callMethods = remopla.getMethodMap();
            return false;
        }

        free();

		maxNodeNum = 0;

        Collection<RemoplaVar> g = remopla.getGlobals();
        enumValues = new HashMap<RemoplaEnumConstant, Integer>();

		// Set up global variables
        int totalVars = 1; // + 1 for return var
        gnum = 0;
		if (g != null && !g.isEmpty()) {
			globals = new HashMap<RemoplaVar, Variable>((int) (1.4 * g.size()));
			for (RemoplaVar rv : g) {
                if (rv.getUsed()) {
                    Variable v = new Variable(rv, bits);
                    globals.put(rv, v);
                    totalVars += globalcopy * v.getTotalVars();
                    gnum += v.getTotalVars();
                }
			}
		}

        callMethods = remopla.getMethodMap();

        // estimate six local variables per method
        locals = new HashMap<RemoplaVar, Variable>((int)(6 * callMethods.size()));

        lvmax = 0;
        for (RemoplaMethod m : remopla.getMethods()) {
            int totalLocals = 0;
            for (RemoplaVar rv : m.getLocalVars()) {
                if (rv.getUsed()) {
                    Variable v = new Variable(rv, bits);
                    locals.put(rv, v);
                    totalLocals += v.getTotalVars();
                }
            }
            if (totalLocals > lvmax) {
                lvmax = totalLocals;
            }
        }
        totalVars += varcopy * lvmax;

        // Calculate domain sizes and assign indices
        long[] domSize = new long[totalVars];
        int index = 0;
		g0 = index;

        // it would be great to make this static
        AssignDomainsIndices adi = new AssignDomainsIndices();
        if (g != null && !g.isEmpty()) {
			for (Variable v : globals.values()) {
                index = adi.handle(v, index, domSize, globalcopy);
			}
		}

		// Local vars -- note we are assuming that everything will fit inside
		// the default number of bits.  This serious for Remopla in general, but
		// should be ok for java translated remopla...  (FIX)
		l0 = index;

        for (RemoplaMethod m : remopla.getMethods()) {
            index = l0;
            for (RemoplaVar rv : m.getLocalVars()) {
                if (rv.getUsed()) {
                    index = adi.handle(locals.get(rv), index, domSize, varcopy);
                }
            }
        }

		// Return
		retDomIndex = totalVars - 1;
		domSize[totalVars - 1] = size;

		doms = factory.extDomain(domSize);

        return true;
    }

    /**
     * @param remopla some remopla
     * @return true if the local and global vars used by the remopla are already
     * mapped to domains by the bdd manager
     */
    private boolean remoplaMatchesCurrentVars(Remopla remopla) {
        if (locals == null || globals == null)
            return false;

        for (RemoplaVar v : remopla.getGlobals()) {
            if (v.getUsed() && !globals.containsKey(v))
                return false;
        }

        for (RemoplaMethod m : remopla.getMethods()) {
            for (RemoplaVar v : m.getLocalVars()) {
                if (v.getUsed() && !locals.containsKey(v))
                    return false;
            }
        }

        return true;
    }


    public StmtOptimisations getStmtOptimisations(RemoplaStmt stmt) {
        if (stmtOptimisations != null)
            return stmtOptimisations.get(stmt);
        else
            return null;
    }

    public long getSize() {
        return size;
    }


	public int getMaxLocalVars() {
		return lvmax;
	}

	void updateMaxNodeNum() {
		int num = factory.getNodeNum();
		if (num > maxNodeNum) maxNodeNum = num;
	}

	public static int getMaxNodeNum() {
		return maxNodeNum;
	}

    public BDD zero() {
        return factory.zero();
    }

	public BDD initVars() {
		// Initializes global variables
		BDD bdd = factory.one();
//		if (globals != null) {
//			for (Variable var : globals.values()) {
//                initVar(var, bdd);
//			}
//		}
//
//		// Initializes local variables
//		for (int i = 0; i < lvmax; i++)
//			bdd.andWith(getLocalVarDomain(i).ithVar(0));

		return bdd;
	}

    /**
     * Note: cannot use this multiple times on same var
     *
     * @param v to initialise
     * @param val initial value
     * @param bdd bdd to mutate
     */
    public void initBoolVar(Variable v, boolean val, BDD bdd) {
        BDDDomain dom = doms[v.getIndex()];
        bdd.andWith(dom.ithVar(encode(bool(val), dom)));
    }

    /**
     * Note: cannot use this multiple times on same var, sets var in G1 not
     * G0
     *
     * @param v to initialise
     * @param val initial value
     * @param bdd bdd to mutate
     */
    public void initG1BoolVar(Variable v, boolean val, BDD bdd) {
        BDDDomain dom = doms[v.getIndex() + gindex[1]];
        bdd.andWith(dom.ithVar(encode(bool(val), dom)));
    }


    /**
     * Note: cannot use this multiple times on same var
     *
     * @param rv to initialise
     * @param val initial value
     * @param bdd bdd to mutate
     */
    public void initBoolVar(RemoplaVar rv, boolean val, BDD bdd)
    throws VariableNotFoundException {
        Variable v = getVar(rv);
        if (v == null)
            throw new VariableNotFoundException(rv);
        BDDDomain dom = doms[v.getIndex()];
        bdd.andWith(dom.ithVar(encode(bool(val), dom)));
    }

    /**
     * Note: cannot use this multiple times on same var, sets in G1 instead of
     * G0
     *
     * @param rv to initialise
     * @param val initial value
     * @param bdd bdd to mutate
     */
    public void initG1BoolVar(RemoplaVar rv, boolean val, BDD bdd)
    throws VariableNotFoundException {
        Variable v = getVar(rv);
        if (v == null)
            throw new VariableNotFoundException(rv);
        BDDDomain dom = doms[v.getIndex() + gindex[1]];
        bdd.andWith(dom.ithVar(encode(bool(val), dom)));
    }


    /**
     * Updates bdd to assert that v1 in G0L0 = v2 in G1.
     *
     * @param rv1
     * @param rv2
     * @param bdd
     */
	public void makeG0L0VarEqualG1Var(RemoplaVar rv1,
                                      RemoplaVar rv2,
                                      BDD bdd)
    throws VariableNotFoundException {
        Variable v1 = getVar(rv1);
        if (v1 == null)
            throw new VariableNotFoundException(rv1);
        Variable v2 = getVar(rv2);
        if (v2 == null)
            throw new VariableNotFoundException(rv2);

        int i1 = v1.getIndex();
        int i2 = v2.getIndex() + gindex[1];

        bdd.andWith(doms[i1].buildEquals(doms[i2]));
	}




    private void initVar(Variable v, BDD bdd) {
        if (v.getSubVars() == null) {
            bdd.andWith(doms[v.getIndex()].ithVar(0));
        } else {
            for (Variable sv : v.getSubVars()) {
                initVar(sv, bdd);
            }
        }
    }

	public Integer getConstant(String name) {
		return constants.get(name);
	}

	public void putConstant(String name, Integer value) {
		constants.put(name, value);
	}

    public int getEnumValue(RemoplaEnumConstant ec) {
        Integer i = enumValues.get(ec);
        return (i != null) ? i.intValue() : -1;
    }

    /** @param callString the string used to call the method
     *  @return a list of parameters in RemoplaVar format, null if not found
     */
    public List<RemoplaVar> getMethodParameters(String callString) {
        List<RemoplaVar> params = null;
        RemoplaMethod method = callMethods.get(callString);
        if (method != null) {
            params = method.getParameters();
        }
        return params;
    }

    /** @param callString the string used to call the method
     *  @return a list of local variables in RemoplaVar format, null if not found
     */
    public Collection<RemoplaVar> getMethodLocals(String callString) {
        Collection<RemoplaVar> locals = null;
        RemoplaMethod method = callMethods.get(callString);
        if (method != null) {
            locals = method.getLocalVars();
        }
        return locals;
    }

    public Collection<RemoplaVar> getGlobals() {
        return Collections.unmodifiableCollection(globals.keySet());
    }

	/**
	 * Returns a BDD representing all variables having zero values.
	 *
	 * @return A BDD representing all variables having zero values.
	 */
	public BDD allZero() {

		BDD a = factory.one();
		for (int i = 0; i < doms.length; i++) {
			a.andWith(doms[i].ithVar(0));
		}
		return a;
	}

	private BDDVarSet globalVarSet = null;

	/**
	 * Gets BDDVarSet of G0.
	 *
	 * @return
	 */
	private BDDVarSet getGlobalVarSet() {

		if (globalVarSet != null) return globalVarSet;

		int i = 0;
		BDDDomain[] d = new BDDDomain[gnum];
		if (globals != null) {
			for (Variable v : globals.values())
				i = getGlobalSubVars(v, d, i);
		}

		globalVarSet = factory.makeSet(d);
		return globalVarSet;
	}

    /** Helper method for getGlobalVarSet **/
    private int getGlobalSubVars(Variable v, BDDDomain[] d, int i) {
        if (v.getSubVars() == null) {
            d[i++] = doms[v.getIndex()];
        } else {
            for (Variable sv : v.getSubVars()) {
                i = getGlobalSubVars(sv, d, i);
            }
        }
        return i;
    }

	/**
	 * Abstracts global variables (G0) from BDD a.
	 *
	 * @param a the BDD
	 * @return the abstracted BDD
	 */
	public BDD abstractGlobals(BDD a) {

		BDDVarSet gvs = getGlobalVarSet();
		return a.exist(gvs);
	}

	private BDDVarSet localVarSet = null;

	private BDDVarSet getLocalVarSet() {
		if (localVarSet != null) return localVarSet;

		int i = 0;
		BDDDomain[] d = new BDDDomain[lvmax];
		for (int j = 0; j < lvmax; j++) {
			d[i++] = getLocalVarDomain(j);
		}

		localVarSet = factory.makeSet(d);
		return localVarSet;
	}

	/**
	 * Abstracts local variables from BDD a.
	 *
	 * @param a the BDD
	 * @return the abstracted BDD
	 */
	public BDD abstractLocals(BDD a) {
		BDDVarSet lvs = getLocalVarSet();
		return a.exist(lvs);
	}

	/**
	 * Abstracts local variables from BDD a.
	 *
	 * @param a the BDD
	 * @return the abstracted BDD
	 */
	public BDD abstractL0(BDD a) {
		BDDVarSet lvs = getL0VarSet();
		return a.exist(lvs);
	}

	/**
	 * Abstracts local variables from BDD a.
	 *
	 * @param a the BDD
	 * @return the abstracted BDD
	 */
	public BDD abstractL2(BDD a) {
		BDDVarSet lvs = getL2VarSet();
		return a.exist(lvs);
	}

	/**
	 * Abstracts variables from BDD a.
	 *
	 * @param a the BDD
	 * @return the abstracted BDD
	 */
	public BDD abstractG2L2(BDD a) {
		BDDVarSet lvs = getG2L2VarSet();
		return a.exist(lvs);
	}

	/**
	 * Abstracts variables from BDD a.
	 *
	 * @param a the BDD
	 * @return the abstracted BDD
	 */
	public BDD abstractG0L0(BDD a) {
		BDDVarSet lvs = getG0L0VarSet();
		return a.exist(lvs);
	}

	/**
	 * Abstracts variables from BDD a.
	 *
	 * @param a the BDD
	 * @return the abstracted BDD
	 */
	public BDD abstractG1L1(BDD a) {
		BDDVarSet lvs = getG1L1VarSet();
		return a.exist(lvs);
	}


	/**
	 * Returns the default integer bits.
	 *
	 * @return the default integer bits.
	 */
	public int getBits() {
		return bits;
	}

	public int getMaxInt() {
		return maxint;
	}

	/**
	 * Returns 2^bits.
	 *
	 * @return 2^bits.
	 */
	public long size() {

		return size;
	}

	public BDDDomain[] getDomains() {

		return doms;
	}

	public BDDDomain getDomain(int index) {

		return doms[index];
	}

	/**
	 * Gets the BDD factory.
	 *
	 * @return the BDD factory.
	 */
	public BDDFactory getFactory() {
		return factory;
	}

	public BDDVarSet getVarSetWithout(int index) {
		if (varSetWithout == null)
			varSetWithout = new HashMap<Integer, BDDVarSet>();

		BDDVarSet varset = varSetWithout.get(index);
		if (varset != null) return varset;

		BDDDomain[] d = new BDDDomain[factory.numberOfDomains() - 1];
		for (int i = 0, j = 0; i < factory.numberOfDomains(); i++) {
			if (i != index) {
				d[j++] = factory.getDomain(i);
			}

		}
		varset = factory.makeSet(d);
		varSetWithout.put(index, varset);

		return varset;
	}

	/**
	 * Gets all BDD variable set without those having indices
	 * specified by <code>indices</code>.
	 *
	 * @param indices the indices.
	 * @return the variable set.
	 */
	public BDDVarSet getVarSetWithout(Set<Integer> indices) {
		BDDDomain[] d = new BDDDomain[factory.numberOfDomains() - indices.size()];
		for (int i = 0, j = 0; i < factory.numberOfDomains(); i++) {
			if (indices.contains(i)) continue;
			d[j++] = factory.getDomain(i);
		}
		return factory.makeSet(d);
	}

	/**
	 * Returns the variable having the specified name.
	 *
	 * @param name the name of the variable
	 * @return the variable
	 */
	public Variable getGlobalVar(RemoplaVar g) {
		if (globals == null) return null;
		return globals.get(g);
	}

	public BDDDomain getGlobalVarDomain(RemoplaVar g) {
		Variable var = getGlobalVar(g);
		if (var == null) return null;
		return doms[var.getIndex()];
	}

	public BDDDomain getG2VarDomain(RemoplaVar g) {
		Variable var = getGlobalVar(g);
		if (var == null) return null;
		return doms[var.getIndex() + 2];
	}



    public Variable getLocalVar(RemoplaVar local) {
        Variable var = locals.get(local);
        return var;
    }

    public BDDDomain getLocalVarDomain(RemoplaVar l) {
        Variable var = getLocalVar(l);
        if (var == null) return null;
        return doms[var.getIndex()];
	}

    public BDDDomain getL2VarDomain(RemoplaVar l) {
        Variable var = getLocalVar(l);
        if (var == null) return null;
        return doms[var.getIndex() + 2];
	}


    /**
     * Not particularly efficient, get var by name
     * Searches local variables first
     */
    public Variable getVar(String name) throws VariableNotFoundException {
        for (RemoplaVar v : locals.keySet()) {
            if (v.getName().equals(name))
                return getVar(v);
        }

        for (RemoplaVar v : globals.keySet()) {
            if (v.getName().equals(name))
                return getVar(v);
        }

        throw new VariableNotFoundException("Variable " + name + " not found.");
    }

    public Variable getVar(RemoplaVar rv) {
        Variable v;
        if (rv.getLocal()) {
            v = getLocalVar(rv);
        } else {
            v = getGlobalVar(rv);
        }
        return v;
    }

    public BDDDomain getVarDomain(RemoplaVar v) {
        BDDDomain vd;
        if (v.getLocal()) {
            vd = getLocalVarDomain(v);
        } else {
            vd = getGlobalVarDomain(v);
        }
        return vd;
    }

    public BDDDomain getVarDomain(Variable v) {
        BDDDomain res = null;
        if (v.getIndex() > -1) {
            res = doms[v.getIndex()];
        }
        return res;
    }

    public BDDDomain get2VarDomain(RemoplaVar v) {
        BDDDomain vd;
        if (v.getLocal()) {
            vd = getL2VarDomain(v);
        } else {
            vd = getG2VarDomain(v);
        }
        return vd;
    }

    public BDDDomain get2VarDomain(Variable v) {
        return doms[v.getIndex() + 2];
    }

    public List<BDDDomain> getSubVarDomains(Variable v) {
        List<BDDDomain> res;
        if (v == null || v.getSubVars() == null) {
            logger.error("VarManager: Variable had no subvars or was null.");
            res = new ArrayList<BDDDomain>(0);
        } else {
            List<Variable> vars = v.flattenSubVars();
            res = new ArrayList<BDDDomain>(vars.size());
            for (Variable sv : vars) {
                res.add(doms[v.getIndex()]);
            }
        }
        return res;
    }

    public BDDDomain getArrayElementDomain(RemoplaArrayVar rarr, long index) {
        Variable arr = getVar(rarr);
        BDDDomain result = null;
        if (arr != null) {
            Variable element = arr.getSubVars().get((int)index - rarr.getMinIndex());
            if (element != null) {
                result = doms[element.getIndex()];
            }
        }
        return result;
    }

	public BDDDomain getRetVarDomain() {
		if (retDomIndex == -1) return null;	// TODO this never happen because we always add ret var
		return doms[retDomIndex];
	}


    public BDDDomain getTempVarDomain() {
        BDDDomain result;
        if (freeTempDomains.size() > 0) {
            result = freeTempDomains.get(0);
            freeTempDomains.remove(0);
            usedTempDomains.add(result);
        } else {
            result = factory.extDomain(size);
            usedTempDomains.add(result);
        }

        return result;
    }

    public void printTempUsage() {
        logger.info("Currently " + usedTempDomains.size() + " temp variable domains in use, " + freeTempDomains.size() + " free.");
    }

    public boolean isTempDomain(BDDDomain temp) {
        return usedTempDomains.contains(temp);
    }

    public void freeIfTempDomain(BDDDomain temp) {
        int posn = usedTempDomains.indexOf(temp);
        if (posn > -1) {
            usedTempDomains.remove(posn);
            freeTempDomains.add(temp);
        }
    }

    public void freeIfTempDomains(BDDDomain[] temp) {
        for (int i = 0; i < temp.length; ++i)
            freeIfTempDomain(temp[i]);
    }


	public BDDDomain getLocalVarDomain(int index) {
		if (l0 == -1) return null;
		return doms[l0 + varcopy*index];
	}


	/**
	 * Returns the bdd representing the equality of two variables
	 * specified by dom1 and dom2.
	 *
	 * @param dom1
	 * @param dom2
	 * @return
	 */
	public BDD bddEquals(BDDDomain dom1, BDDDomain dom2) {

		BigInteger size1 = dom1.size();
		BigInteger size2 = dom2.size();

		// Uses the library function if the domains have the same size
		if (size1.equals(size2))
			return dom1.buildEquals(dom2);

		BDDDomain less, more;
		if (size1.compareTo(size2) < 0) {
			less = dom1;
			more = dom2;
		} else {
			less = dom2;
			more = dom1;
		}
		int[] lessvars = less.vars();
		int[] morevars = more.vars();

		BDD a = factory.one();
		int i;
		for (i = 0; i < lessvars.length - 1; i++) {
			BDD b = factory.ithVar(lessvars[i]).andWith(factory.ithVar(morevars[i]));
			b.orWith(factory.nithVar(lessvars[i]).andWith(factory.nithVar(morevars[i])));
			a.andWith(b);
		}

		// Sign-bit extension: zero
		BDD b = factory.ithVar(lessvars[i]);
		for (int j = i; j < morevars.length; j++)
			b.andWith(factory.ithVar(morevars[j]));

		// Sign-bit extension: one
		BDD c = factory.nithVar(lessvars[i]);
		for (int j = i; j < morevars.length; j++)
			c.andWith(factory.nithVar(morevars[j]));

		a.andWith(b.orWith(c));
		return a;
	}

	private BDD G0L0equalsG1L1 = null;

	public BDD buildG0L0equalsG1L1() {

		if (G0L0equalsG1L1 != null) return G0L0equalsG1L1;

		G0L0equalsG1L1 = factory.one();
		for (int i = 0; i < gnum; i++) {
			int index = g0 + globalcopy*i;
			G0L0equalsG1L1.andWith(doms[index].buildEquals(doms[index + gindex[1]]));
		}
		for (int i = 0; i < lvmax; i++) {
			int index = l0 + varcopy*i;
			G0L0equalsG1L1.andWith(doms[index].buildEquals(doms[index + 1]));
		}

		return G0L0equalsG1L1;
	}

	private BDD G0equalsG1 = null;

	public BDD buildG0equalsG1() {

		if (G0equalsG1 != null) return G0equalsG1;

		G0equalsG1 = factory.one();
		for (int i = 0; i < gnum; i++) {
			int index = g0 + globalcopy*i;
			G0equalsG1.andWith(doms[index].buildEquals(doms[index + gindex[1]]));
		}

		return G0equalsG1;
	}



	private BDD L0equalsL2 = null;

	public BDD buildL0equalsL2() {

		if (L0equalsL2 != null) return L0equalsL2;

		L0equalsL2 = factory.one();
		for (int i = 0; i < lvmax; i++) {
			int index = l0 + varcopy*i;
			L0equalsL2.andWith(doms[index].buildEquals(doms[index + 2]));
		}

		return L0equalsL2;
	}





	private BDD G0equalsG3;

	public BDD buildG0equalsG3() {

		if (G0equalsG3 != null)
			return G0equalsG3;

		G0equalsG3 = factory.one();
		for (int i = 0; i < gnum; i++) {
			int index = g0 + globalcopy*i;
			G0equalsG3.andWith(doms[index].buildEquals(doms[index + gindex[3]]));
		}

		return G0equalsG3;
	}

	private BDD G3equalsG4;

	public BDD buildG3equalsG4() {

		if (G3equalsG4 != null)
			return G3equalsG4;

		G3equalsG4 = factory.one();
		for (int i = 0; i < gnum; i++) {
			int index = g0 + globalcopy*i;
			G3equalsG4.andWith(doms[index + gindex[3]].buildEquals(doms[index + gindex[4]]));
		}

		return G3equalsG4;
	}

	/**
	 * Returns the bdd representing the inequality of two variables
	 * specified by dom1 and dom2.
	 *
	 * @param dom1
	 * @param dom2
	 * @return
	 */
	public BDD bddNotEquals(BDDDomain dom1, BDDDomain dom2) {

		BigInteger size1 = dom1.size();
		BigInteger size2 = dom2.size();

		// Uses the library function if the domains have the same size
		if (size1.equals(size2))
			return dom1.buildEquals(dom2).not();

		BDDDomain less, more;
		if (size1.compareTo(size2) < 0) {
			less = dom1;
			more = dom2;
		} else {
			less = dom2;
			more = dom1;
		}
		int[] lessvars = less.vars();
		int[] morevars = more.vars();
		BDD a = factory.zero();
		for (int i = 0; i < lessvars.length; i++) {
			a.orWith(factory.nithVar(lessvars[i]).andWith(factory.ithVar(morevars[i])));
			a.orWith(factory.ithVar(lessvars[i]).andWith(factory.nithVar(morevars[i])));
		}

		// Not equal, if the sign bits are different
		a.orWith(factory.nithVar(lessvars[lessvars.length-1])
				.andWith(factory.ithVar(morevars[morevars.length-1])));
		a.orWith(factory.ithVar(lessvars[lessvars.length-1])
				.andWith(factory.nithVar(morevars[morevars.length-1])));
		return a;
	}

	/**
	 * Returns the BDD with domain specified by <code>dom</code>
	 * representing values from <code>min</code> to <code>max</code>.
	 *
	 * @param dom the domain of the BDD.
	 * @param min the minimum value.
	 * @param max the maximium value.
	 * @return the BDD with values from <code>min</code> to <code>max</code>.
	 */
	public BDD bddRange(BDDDomain dom, int min, int max) {

		if (min == max)
			return dom.ithVar(min);

		// Bug in JavaBDD; Handles manually

//		// Handles manually, because the library seems to be wrong in this case
//		if (min == 0 && max == 1)
//			return dom.ithVar(0).orWith(dom.ithVar(1));
//
//		if (min >= 0) {
//			return dom.varRange(encode(min, dom), encode(max, dom));
//		}

		BDD a = factory.zero();
		for (int i = min; i <= max; i++) {
			a.orWith(dom.ithVar(encode(i, dom)));
		}

		return a;
	}

	/**
	 * Gets the BDD iterator from the <code>bdd</code>.
	 * The iterator contains only the variables specified by <code>dom</code>.
	 *
	 * @param bdd the BDD.
	 * @param doms the BDD domain.
	 * @return the BDD iterator.
	 */
	public BDDIterator iterator(BDD bdd, BDDDomain dom) {
        assert bdd != null && dom != null;

		BDDVarSet varset = dom.set();
        BDD tmp = bdd.exist(getVarSetWithout(dom.getIndex()));
		BDDIterator itr = tmp.iterator(varset);
        tmp.free();
		varset.free();

		return itr;
	}

	/**
	 * Gets the BDD iterator from the <code>bdd</code>.
	 * The iterator contains only the variables specified by <code>doms</code>.
	 *
	 * @param bdd the BDD.
	 * @param doms the BDD domains.
	 * @return the BDD iterator.
	 */
	public BDDIterator iterator(BDD bdd, BDDDomain... doms) {
        assert bdd != null && doms != null;


		// Collects var set and indices from doms
		BDDVarSet varset = factory.emptySet();
		Set<Integer> indices = new HashSet<Integer>((int) (1.4*doms.length));
		for (int i = 0; i < doms.length; i++) {
            assert doms[i] != null;
			varset.unionWith(doms[i].set());
			indices.add(doms[i].getIndex());
		}

		// Abstracts and creates iterator
		BDDVarSet abs = getVarSetWithout(indices);
        BDD tmp = bdd.exist(abs);
		BDDIterator itr = tmp.iterator(varset);
        tmp.free();
		abs.free();
		varset.free();

		return itr;
	}


	private int nargs;
	private BDDDomain[] argDoms;

	public BDD saveArgs(int hp, int nargs) {

		this.nargs = nargs;
		if (nargs <= 0) return factory.one();

		long[] domSize = new long[nargs + hp - 1];
		int j = 0;
		for (int i = 0; i < nargs; i++)
			domSize[j++] = size;
		argDoms = factory.extDomain(domSize);
		BDD bdd = factory.one();
		j = 0;
		for (int i = 0; i < nargs; i++)
			bdd.andWith(getLocalVarDomain(i).buildEquals(argDoms[j++]));
		return bdd;
	}

	public List<RawArgument> getRawArguments(BDD a) {

		// Abstracts everything else
		BDDVarSet abs = factory.emptySet();
		for (int i = 0; i < doms.length; i++)
			abs.unionWith(doms[i].set());
		BDD b = a.exist(abs);

		// Gets iterator
		BDDVarSet vs = factory.emptySet();
		for (int i = 0; i < argDoms.length; i++)
			vs.unionWith(argDoms[i].set());
		BDDIterator itr = b.iterator(vs);

		// For each possible argument
		List<RawArgument> args = new ArrayList<RawArgument>();
		while (itr.hasNext()) {
			BDD c = itr.nextBDD();
			RawArgument arg = new RawArgument(nargs);
			for (int i = 0; i < nargs; i++)
				arg.lv[i] = decode(c.scanVar(argDoms[i]).longValue(), argDoms[i]);
			args.add(arg);
			c.free();
		}

		vs.free();
		b.free();
		return args;
	}

	/**
	 * Signature of a: (G0,L0,G1,L1)
	 *
	 * @param a
	 * @return
	 */
	public List<RawArgument> getRawArguments2(BDD a) {
		// Abstracts G0 and L0
		BDDVarSet abs = getGlobalVarSet().union(getLocalVarSet());
		BDD b = a.exist(abs);
		abs.free();

		// Gets iterator
		BDDIterator itr = b.iterator(getG1L1VarSet());

		// For each possible argument
		List<RawArgument> args = new ArrayList<RawArgument>();
		while (itr.hasNext()) {
			BDD c = itr.nextBDD();
			RawArgument arg = new RawArgument(lvmax);
			for (int i = 0; i < lvmax; i++) {
				BDDDomain dom = doms[l0 + varcopy*i + 1];
				arg.lv[i] = decode(c.scanVar(dom).longValue(), dom);
			}
			args.add(arg);
			c.free();
		}

		b.free();
		return args;
	}

	/**
	 * Signature of a: (G0,L0,G1,L1).
	 * Signature of b: (G2,L2,L0,G1,L1).
	 *
	 * The method (i) changes a to (G0,L0,G2,L2),
	 * (ii) abstracts L0 from b,
	 * (iii) conjuncts a (G0,L0,G2,L2) and b (G2,L2,G1,L1), and
	 * (iv) abstracts G2 and L2.
	 *
	 * The bdd a and b remain unchanged.
	 *
	 * @param a
	 * @param b
	 * @return
	 */
	public BDD conjoin(BDD a, BDD b) {

		// (i) Changes a to (G0,L0,G2,L2)
//		BDD ap = replaceG1L1withG2L2(a.id());
		BDD ap = a.id().replaceWith(getG1L1pairG2L2());

		// (ii) Abstracts L0 from b
		BDD bp = b.exist(getLocalVarSet());

		// (iii) Conjuncts a and b
		ap.andWith(bp);

		// (iv) Abstracts G2 and L2
		bp = ap.exist(getG2L2VarSet());
		ap.free();

		return bp;
	}

	private ArrayList<String> strings = new ArrayList<String>();

	public List<String> getStrings() {
		return strings;
	}

	public long encode(String raw, BDDDomain dom) {
		// Adds a dummy string at index zero for preventing NPE
		if (strings.isEmpty())
			strings.add("");

		// Returns the index if the string already exists
		int index = strings.indexOf(raw);
		if (index != -1) return index;

		index = strings.size();
		if (index >= dom.size().intValue())
			System.err.println("Too many strings");
		strings.add(raw);
		return index;
	}

	public String decodeString(long encoded) {
		return strings.get((int) encoded);
	}

	private ArrayList<Float> floats = new ArrayList<Float>();

	public List<Float> getFloats() {
		return floats;
	}

	/**
	 * Encodes the float <code>raw</code>.
	 *
	 * @param raw
	 * @param dom
	 * @return
	 */
	public long encode(float raw, BDDDomain dom) {
		int index = floats.indexOf(raw);
		if (index != -1) return index;

		index = floats.size();
		if (index >= dom.size().intValue())
			System.err.println("Too many floats");
		floats.add(raw);
		return index;
	}

	public float decodeFloat(long encoded) {
		return floats.get((int) encoded);
	}

	/**
	 * Encodes <code>raw</code> in two-complement format.
	 *
	 * @param raw
	 * @return
	 */
	public static long encode(long raw, BDDDomain dom) {
//		long maxint = dom.size().longValue()/2 - 1;
//		if (raw >= 0)
//			return ((long) raw) & maxint;
//
//		if (raw == Integer.MAX_VALUE)
//			return dom.size().longValue()/2 - 1;
//
//		if (raw == Integer.MIN_VALUE)
//			return dom.size().longValue()/2;
//
//		long max = dom.size().longValue() - 1;
//		if (raw >= 0)
//			return ((long) raw) & max;
//
//		long result = (long) raw;
//		do
//			result += dom.size().longValue();
//		while (result < 0);
//		return result;

        long min = CmdOptions.getMinInt();
        long max = min + dom.size().longValue() - 1;

        long value;
        if (raw > max)
            value = max;
        else if (raw < min)
            value = min;
        else
            value = raw;


        // TODO: a proper implementation of integers
        // Subtracting min puts min at 0
        return (value - min);
	}

	public static int decode(long encoded, BDDDomain dom) {
//		int maxint = (int) (dom.size().longValue()/2 - 1);
//		if (maxint == 0 || encoded <= maxint)
//			return (int) encoded;
//
//		return (int) (encoded - dom.size().longValue());
        return (int)(encoded + CmdOptions.getMinInt());
	}

	public static boolean isNeg(long v, BDDDomain dom) {

		return v > dom.size().longValue()/2 - 1;
	}

	public long fneg(long v, BDDDomain dom) {
		float f = decodeFloat(v);
		return encode(-f, dom);
	}

	/**
	 * Returns the negation of v in two-complement format.
	 * For example, if bits = 3, then the following input/output pairs valid:
	 * 0->0, 1->4, 2->5, 3->6, 4->1 5->2, 6->3, 7->3.
	 *
	 * Note that an overflow occurs when negating the minimum (negative) value.
	 * In the example, 7(-4) -> 4(-1).
	 *
	 * @param v the value to be negated.
	 * @return the negation of v.
	 */
	public long neg(long v) {

		if (v == 0) return 0;

		return (v <= maxint) ? v + maxint : v - maxint;
	}

    /** True if the expression has a value based just on its left operand **/
    public boolean binExpHasValueFromLeft(RemoplaBinExpr.Op type, long leftVal, BDDDomain leftDom) {
        boolean hasValue;
		switch (type) {
		case AND:
			hasValue = (decode(leftVal, leftDom) == FALSE);
            break;
		case OR:
			hasValue = (decode(leftVal, leftDom) != FALSE);
            break;
		default:
            hasValue = false;
		}
        return hasValue;
    }

    /** the value of the expression as determined by the left operand.
     * binExpValueFromLeft(type, leftVal, leftDom) must be true.
     */
    public long binExpValueFromLeft(RemoplaBinExpr.Op type, long leftVal, BDDDomain leftDom, BDDDomain resultDom) {
        assert binExpHasValueFromLeft(type, leftVal, leftDom);
        long value;
		switch (type) {
		case AND:
			value = encode(bool(false), resultDom);
            break;
		case OR:
			value = encode(bool(true), resultDom);
            break;
		default:
            value = -1;
		}
        return value;
    }


    public long arith(RemoplaBinExpr.Op type, BDDDomain rdom, long v1, BDDDomain v1dom, long v2, BDDDomain v2dom) {
        v1 = decode(v1, v1dom);
        v2 = decode(v2, v2dom);

		switch (type) {
		case ADD:
			return encode(v1 + v2, rdom);
		case AND:
			return encode(v1 & v2, rdom);
		case DIV:
            if (v2 == 0) {
                logger.error("Divide by zero in VarManager.arith.");
                return encode(maxint, rdom);
            }
			return encode(v1 / v2, rdom);
		case MUL:
			return encode(v1 * v2, rdom);
		case OR:
			return encode(v1 | v2, rdom);
		case SHL:
			return encode(v1 << (v2 & 31), rdom);
		case SHR:
			return encode(v1 >> (v2 & 31), rdom);
		case SUB:
			return encode(v1 - v2, rdom);
		case XOR:
			return encode(v1 ^ v2, rdom);
        case EQ:
            return encode(bool(v1 == v2), rdom);
        case NEQ:
             return encode(bool(v1 != v2), rdom);
        case LT:
             return encode(bool(v1 < v2), rdom);
        case LTE:
             return encode(bool(v1 <= v2), rdom);
        case GT:
             return encode(bool(v1 > v2), rdom);
        case GTE:
             return encode(bool(v1 >= v2), rdom);
		default:
			throw new IllegalArgumentException("Invalid ArithType: " + type);
		}
    }

    public long arith(RemoplaUnaryExpr.Op type, BDDDomain rdom, long v1, BDDDomain v1dom) {
        v1 = decode(v1, v1dom);
		switch (type) {
		case NEG:
			return encode(bool(v1 == FALSE), rdom);
		default:
			throw new IllegalArgumentException("Invalid ArithType: " + type);
		}
	}


    private static long bool(boolean b) {
        return b ? FALSE + 1 : FALSE;
    }

	private static final int TO_STRING_BOUND = 20;

	public String toString(BDD a, String separator, BDD restrictor) {

		// Abstracts G1, L1, G2, L2, unused stack elements, and return variable
		BDDVarSet abs = getG1L1VarSet().union(getG2L2VarSet());
		abs.unionWith(getRetVarDomain().set());
		BDD b = a.exist(abs);
		abs.free();

		if (restrictor != null)
			b.andWith(restrictor);

		// Creates var set over globals, locals, and stacks
		int nargs = (argDoms == null) ? 0 : argDoms.length;
		BDDDomain[] d = new BDDDomain[gnum + lvmax + nargs];
		int j = 0;
		for (int i = 0; i < gnum; i++)
			d[j++] = doms[g0 + globalcopy*i];
		for (int i = 0; i < lvmax; i++)
			d[j++] = doms[l0 + varcopy*i];
		for (int i = 0; i < nargs; i++)
			d[j++] = argDoms[i];
		BDDVarSet vs0 = factory.makeSet(d);

		// Creates iterator
		BDDIterator itr = b.iterator(vs0);
		b.free();
		vs0.free();

		int count = 0;
		ArrayList<String> all = new ArrayList<String>();
		while (itr.hasNext() && count++ < TO_STRING_BOUND) {

			ArrayList<String> state = new ArrayList<String>();
			b = (BDD) itr.next();

			// Global vars
			if (globals != null) {
				ArrayList<String> gv = new ArrayList<String>();
				for (RemoplaVar rv : globals.keySet()) {
					gv.add(String.format("%s: %d", rv.getName(), b.scanVar(getGlobalVarDomain(rv))));
				}
				state.add(toCommaString(gv));
			}

			if (lvmax > 0) {
				ArrayList<Integer> lv = new ArrayList<Integer>((int) (1.4*lvmax));
				for (int i = 0; i < lvmax; i++) {
					lv.add(b.scanVar(getLocalVarDomain(i)).intValue());
				}
				state.add(String.format("lv: [%s]", toCommaString(lv)));
			}

			if (nargs > 0) {
				ArrayList<Integer> args = new ArrayList<Integer>((int) (1.4*nargs));
				for (int i = 0; i < nargs; i++) {
					args.add(b.scanVar(argDoms[i]).intValue());
				}
				state.add(String.format("args: [%s]", toCommaString(args)));
			}

			all.add(toCommaString(state));
			b.free();
		}

		if (count >= TO_STRING_BOUND) {
			all.add("toString() bound reached, skipping the rest");
		}

		return toString(all, separator);
	}

	public String toString(BDD a, BDD restrictor) {

		return toString(a, "\n", restrictor);
	}

	public String toString(BDD a, String separator) {

		return toString(a, separator, null);
	}

	public String toString(BDD a) {

		return toString(a, "\n");
	}

	public static String toString(ArrayList<? extends Object> list, String separator) {

		if (list == null) return null;
		if (list.isEmpty()) return "";

		StringBuilder out = new StringBuilder();
		Iterator<? extends Object> itr = list.iterator();
		out.append(itr.next());
		while (itr.hasNext()) {
			out.append(separator).append(itr.next());
		}

		return out.toString();
	}

	public static String toCommaString(ArrayList<? extends Object> list) {
		return toString(list, ", ");
	}

	/*************************************************************************
	 * Methods for computing BDDDomain[]
	 *************************************************************************/

	private static enum DomainType {
		G0, G1, G2, G3, G4, G0L0, G1L1, G2L2, G0L0G1, G1L1G2, L0, L1, L2;
	}

	private EnumMap<DomainType, BDDDomain[]> domains
			= new EnumMap<DomainType, BDDDomain[]>(DomainType.class);

	private BDDDomain[] putGxDomain(DomainType type, int x) {
		if (domains.containsKey(type))
			return domains.get(type);

		BDDDomain[] d = new BDDDomain[gnum];
		for (int i = 0; i < gnum; i++)
			d[i] = doms[g0 + globalcopy*i + gindex[x]];

		domains.put(type, d);
		return d;
	}

	private BDDDomain[] putLxDomain(DomainType type, int x) {
		if (domains.containsKey(type))
			return domains.get(type);

		BDDDomain[] d = new BDDDomain[lvmax];
		for (int i = 0; i < lvmax; i++)
			d[i] = doms[l0 + varcopy*i + x];

		domains.put(type, d);
		return d;
	}


	private BDDDomain[] getGxDomain(int x) {
		switch (x) {
		case 0: return putGxDomain(DomainType.G0, 0);
		case 1: return putGxDomain(DomainType.G1, 1);
		case 2: return putGxDomain(DomainType.G2, 2);
		case 3: return putGxDomain(DomainType.G3, 3);
		case 4: return putGxDomain(DomainType.G4, 4);
		}

        logger.error("Unexpected argument to VarManager.getGxDomain!");
        return null;
	}

	private BDDDomain[] getLxDomain(int x) {
		switch (x) {
		case 0: return putLxDomain(DomainType.L0, 0);
		case 1: return putLxDomain(DomainType.L1, 1);
		case 2: return putLxDomain(DomainType.L2, 2);
		}

        logger.error("Unexpected argument to VarManager.getLxDomain!");
        return null;
	}


	private BDDDomain[] putGxLxDomain(DomainType type, int x) {
		if (domains.containsKey(type))
			return domains.get(type);

		BDDDomain[] d = new BDDDomain[gnum + lvmax];
		int j = 0;
		for (int i = 0; i < gnum; i++)
			d[j++] = doms[g0 + globalcopy*i + gindex[x]];
		for (int i = 0; i < lvmax; i++)
			d[j++] = doms[l0 + varcopy*i + x];

		domains.put(type, d);
		return d;
	}

    private BDDDomain[] putGxLyGzDomain(DomainType type, int x, int y, int z) {
		if (domains.containsKey(type))
			return domains.get(type);

		BDDDomain[] d = new BDDDomain[2 * gnum + lvmax];
		int j = 0;
		for (int i = 0; i < gnum; i++)
			d[j++] = doms[g0 + globalcopy*i + gindex[x]];
		for (int i = 0; i < lvmax; i++)
			d[j++] = doms[l0 + varcopy*i + y];
		for (int i = 0; i < gnum; i++)
			d[j++] = doms[g0 + globalcopy*i + gindex[z]];

		domains.put(type, d);
		return d;
    }

    private BDDDomain[] getG0L0G1Domain() {
        return putGxLyGzDomain(DomainType.G0L0G1, 0, 0, 1);
    }

    private BDDDomain[] getG1L1G2Domain() {
        return putGxLyGzDomain(DomainType.G1L1G2, 1, 1, 2);
    }

	private BDDDomain[] getG0L0Domain() {
		return putGxLxDomain(DomainType.G0L0, 0);
	}

	private BDDDomain[] getG1L1Domain() {
		return putGxLxDomain(DomainType.G1L1, 1);
	}

	private BDDDomain[] getG2L2Domain() {
		return putGxLxDomain(DomainType.G2L2, 2);
	}

	/*************************************************************************
	 * Methods for computing BDDVarSet
	 *************************************************************************/

	private static enum VarSetType {
		G0L0, G0G1G2L0L1L2, G0L0G1L1, L0G1L1, L0G1L1G2L2, G0G4, G1L1, G2L2, G0, G2, G3, G4, L0, L1, L2;
	}

	private EnumMap<VarSetType, BDDVarSet> varsets
			= new EnumMap<VarSetType, BDDVarSet>(VarSetType.class);

	private BDDVarSet putVarSet(VarSetType type, BDDDomain[] d) {
		BDDVarSet vs = factory.makeSet(d);
		varsets.put(type, vs);
		return vs;
	}

    private BDDVarSet getLxVarSet(VarSetType type, int x) {
        if (varsets.containsKey(type))
            return varsets.get(type);

		BDDDomain[] d = new BDDDomain[lvmax];
		for (int i = 0; i < lvmax; i++)
			d[i] = doms[l0 + VarManager.varcopy*i + x];

		return putVarSet(type, d);
    }

    BDDVarSet getL0VarSet() {
        return getLxVarSet(VarSetType.L0, 0);
    }

    BDDVarSet getL1VarSet() {
        return getLxVarSet(VarSetType.L1, 1);
    }

    BDDVarSet getL2VarSet() {
        return getLxVarSet(VarSetType.L2, 2);
    }


	BDDVarSet getVarSet0() {

		if (varsets.containsKey(VarSetType.G0L0))
			return varsets.get(VarSetType.G0L0);

		int base = gnum + lvmax;
		BDDDomain[] d = new BDDDomain[base];
		int j = 0;
		for (int i = 0; i < gnum; i++)
			d[j++] = doms[g0 + globalcopy*i];
		for (int i = 0; i < lvmax; i++)
			d[j++] = doms[l0 + VarManager.varcopy*i];

		return putVarSet(VarSetType.G0L0, d);
	}

	BDDVarSet getG0G1G2L0L1L2VarSet() {

		if (varsets.containsKey(VarSetType.G0G1G2L0L1L2))
			return varsets.get(VarSetType.G0G1G2L0L1L2);

		BDDDomain[] d = new BDDDomain[3*gnum + 3*lvmax];
		int j = 0;
		for (int i = 0; i < gnum; i++) {
			d[j++] = doms[g0 + globalcopy*i];
			d[j++] = doms[g0 + globalcopy*i + gindex[1]];
			d[j++] = doms[g0 + globalcopy*i + gindex[2]];
		}
		for (int i = 0; i < lvmax; i++) {
			d[j++] = doms[l0 + VarManager.varcopy*i];
			d[j++] = doms[l0 + VarManager.varcopy*i + 1];
			d[j++] = doms[l0 + VarManager.varcopy*i + 2];
		}

		return putVarSet(VarSetType.G0G1G2L0L1L2, d);
	}

	public BDDVarSet getG0L0G1L1VarSet() {
		if (varsets.containsKey(VarSetType.G0L0G1L1))
			return varsets.get(VarSetType.G0L0G1L1);

		BDDDomain[] d = new BDDDomain[2*gnum + 2*lvmax];
		int j = 0;
		for (int i = 0; i < gnum; i++) {
			d[j++] = doms[g0 + globalcopy*i];
			d[j++] = doms[g0 + globalcopy*i + gindex[1]];
		}
		for (int i = 0; i < lvmax; i++) {
			d[j++] = doms[l0 + varcopy*i];
			d[j++] = doms[l0 + varcopy*i + 1];
		}

		return putVarSet(VarSetType.G0L0G1L1, d);
	}

	BDDVarSet getL0G1L1VarSet() {

		if (varsets.containsKey(VarSetType.L0G1L1))
			return varsets.get(VarSetType.L0G1L1);

		BDDDomain[] d = new BDDDomain[gnum + 2*lvmax];
		int j = 0;
		for (int i = 0; i < gnum; i++)
			d[j++] = doms[g0 + globalcopy*i + gindex[1]];
		for (int i = 0; i < lvmax; i++) {
			d[j++] = doms[l0 + varcopy*i];
			d[j++] = doms[l0 + varcopy*i + 1];
		}

		return putVarSet(VarSetType.L0G1L1, d);
	}

	BDDVarSet getL0G1L1G2L2VarSet() {

		if (varsets.containsKey(VarSetType.L0G1L1G2L2))
			return varsets.get(VarSetType.L0G1L1G2L2);

		BDDDomain[] d = new BDDDomain[2*gnum + 3*lvmax];
		int j = 0;
		for (int i = 0; i < gnum; i++){
			d[j++] = doms[g0 + globalcopy*i + gindex[1]];
			d[j++] = doms[g0 + globalcopy*i + gindex[2]];
		}
		for (int i = 0; i < lvmax; i++) {
			d[j++] = doms[l0 + varcopy*i];
			d[j++] = doms[l0 + varcopy*i + 1];
			d[j++] = doms[l0 + varcopy*i + 2];
		}

		return putVarSet(VarSetType.L0G1L1G2L2, d);
	}

	BDDVarSet getG0G4VarSet() {

		if (varsets.containsKey(VarSetType.G0G4))
			return varsets.get(VarSetType.G0G4);

		BDDDomain[] d = new BDDDomain[2*gnum];
		int j = 0;
		for (int i = 0; i < gnum; i++) {
			d[j++] = doms[g0 + globalcopy*i];
			d[j++] = doms[g0 + globalcopy*i + gindex[4]];
		}

		return putVarSet(VarSetType.G0G4, d);
	}

	BDDVarSet getG0L0VarSet() {
		return getVarSet0();
	}

	BDDVarSet getG1L1VarSet() {
		if (varsets.containsKey(VarSetType.G1L1))
			return varsets.get(VarSetType.G1L1);

		return putVarSet(VarSetType.G1L1, getG1L1Domain());
	}

	BDDVarSet getG2L2VarSet() {
		if (varsets.containsKey(VarSetType.G2L2))
			return varsets.get(VarSetType.G2L2);

		return putVarSet(VarSetType.G2L2, getG2L2Domain());
	}

	private BDDVarSet getGxVarSet(VarSetType type, int x) {
		if (varsets.containsKey(type))
			return varsets.get(type);

		return putVarSet(type, getGxDomain(x));
	}

	BDDVarSet getG0VarSet() {
		return getGxVarSet(VarSetType.G0, 0);
	}

	BDDVarSet getG1VarSet() {
		return getGxVarSet(VarSetType.G0, 1);
	}

	BDDVarSet getG2VarSet() {
		return getGxVarSet(VarSetType.G2, 2);
	}

	BDDVarSet getG3VarSet() {
		return getGxVarSet(VarSetType.G3, 3);
	}

	BDDVarSet getG4VarSet() {
		return getGxVarSet(VarSetType.G4, 4);
	}

	/*************************************************************************
	 * Methods for renaming BDD variables
	 *************************************************************************/

	private static enum PairingType {
		G1L1_G2L2, G0_G2, G0_G3, G3_G4, G0L0G1_G1L1G2, G1_G3, G1_G2, G1_G0, G2_G0, L0_L1, L0_L2, L2_L0, G2L2_G0L0;
	}

	private EnumMap<PairingType, BDDPairing> pairings
			= new EnumMap<PairingType, BDDPairing>(PairingType.class);

	private BDDPairing putPairing(PairingType type, BDDDomain[] d1, BDDDomain[] d2) {
		BDDPairing p = factory.makePair();
		p.set(d1, d2);
		pairings.put(type, p);
		return p;
	}

	BDDPairing getG0L0G1pairG1L1G2() {
		if (pairings.containsKey(PairingType.G0L0G1_G1L1G2))
			return pairings.get(PairingType.G0L0G1_G1L1G2);

		return putPairing(PairingType.G0L0G1_G1L1G2, getG0L0G1Domain(), getG1L1G2Domain());
	}

    public BDDPairing getG2L2pairG0L0() {
		if (pairings.containsKey(PairingType.G2L2_G0L0))
			return pairings.get(PairingType.G2L2_G0L0);

		return putPairing(PairingType.G2L2_G0L0, getG2L2Domain(), getG0L0Domain());
	}


	BDDPairing getG1L1pairG2L2() {
		if (pairings.containsKey(PairingType.G1L1_G2L2))
			return pairings.get(PairingType.G1L1_G2L2);

		return putPairing(PairingType.G1L1_G2L2, getG1L1Domain(), getG2L2Domain());
	}

    BDDPairing getLxpairLy(PairingType type, int x, int y) {
		if (pairings.containsKey(type))
			return pairings.get(type);

		return putPairing(type, getLxDomain(x), getLxDomain(y));
	}

    BDDPairing getL0pairL2() {
        return getLxpairLy(PairingType.L0_L2, 0, 2);
    }

    public BDDPairing getL0pairL1() {
        return getLxpairLy(PairingType.L0_L1, 0, 1);
    }

    public BDDPairing getL2pairL0() {
        return getLxpairLy(PairingType.L2_L0, 2, 0);
    }

	private BDDPairing getGxpairGy(PairingType type, int x, int y) {
		if (pairings.containsKey(type))
			return pairings.get(type);

		return putPairing(type, getGxDomain(x), getGxDomain(y));
	}

	BDDPairing getG0pairG2() {
		return getGxpairGy(PairingType.G0_G2, 0, 2);
	}

	BDDPairing getG0pairG3() {
		return getGxpairGy(PairingType.G0_G3, 0, 3);
	}

	BDDPairing getG1pairG3() {
		return getGxpairGy(PairingType.G1_G3, 1, 3);
	}

    BDDPairing getG1pairG2() {
		return getGxpairGy(PairingType.G1_G2, 1, 2);
	}

	public BDDPairing getG2pairG0() {
		return getGxpairGy(PairingType.G2_G0, 2, 0);
	}

	BDDPairing getG3pairG4() {
		return getGxpairGy(PairingType.G3_G4, 3, 4);
	}

    BDDPairing getG1pairG0() {
		return getGxpairGy(PairingType.G1_G0, 1, 0);
	}

	private BDD g3g4ordering;

	public BDD getG3G4ordering() {

		if (g3g4ordering != null) return g3g4ordering;

		g3g4ordering = factory.zero();
		for (int i = 0; i < gnum; i++) {
			int index = g0 + globalcopy*i;
			int[] vars3 = doms[index + gindex[3]].vars();
			int[] vars4 = doms[index + gindex[4]].vars();
			for (int j = 0; j < vars3.length; j++)
				g3g4ordering = factory.nithVar(vars3[j]).andWith(factory.ithVar(vars4[j]))
						.orWith(factory.ithVar(vars3[j]).biimpWith(factory.ithVar(vars4[j]))
								.andWith(g3g4ordering));
		}

		return g3g4ordering;
	}


    /**
	 * Returns the bdd representing vals equal to local variables of l2.
	 *
	 * @param vals the domains of the values to assign to the args
	 * @param args the variables representing the args.  Note that these should
	 * be flattened -- that is, no subvars like you get with structs or arrays
	 * @return
	 */
	BDD bddL0equalsL2params(List<BDDDomain> vals, List<Variable> args) {
		assert vals.size() == args.size();
		BDD d = factory.one();

        Iterator ivals = vals.iterator();
        Iterator iargs = args.iterator();
        while (ivals.hasNext()) {
            BDDDomain dom = (BDDDomain)ivals.next();
            Variable arg = (Variable)iargs.next();
			d.andWith(dom.buildEquals(doms[arg.getIndex() + 2]));
		}

		return d;
	}

	/**
	 * Sets the verbosity level.
	 *
	 * @param level the verbosity level.
	 */
	public static void setVerbosity(int level) {
		verbosity = level;
	}

	public String toString() {
		StringBuilder out = new StringBuilder();
		out.append(String.format("Bits: %d, %n", bits, g0));
		out.append(String.format("g0: %d, gnum: %d", g0, gnum));
		out.append(String.format("l0: %d, lvmax: %d, %n", l0, lvmax, l0));

        out.append("Globals:\n");
        for (Map.Entry<RemoplaVar, Variable> e : globals.entrySet()) {
            out.append("  " + e.getKey() + " = " + e.getValue().getIndex());
        }
        out.append("\nLocals:\n");
        for (Map.Entry<RemoplaVar, Variable> e : locals.entrySet()) {
            out.append("  " + e.getKey() + " = " + e.getValue().getIndex());
        }

		return out.toString();
	}

	public void free() {
        globals = null;
        locals = null;
        enumValues = null;
        callMethods = null;
        doms = null;
        stmtOptimisations = null;
        nargs = -1;
        argDoms = null;

        freeTempDomains.clear();
        usedTempDomains.clear();
        constants.clear();
        strings.clear();
        floats.clear();
        domains.clear();
        pairings.clear();

		if (localVarSet != null) {
            localVarSet.free();
            localVarSet = null;
        }

		if (globalVarSet != null) {
            globalVarSet.free();
            globalVarSet = null;
        }

		if (varSetWithout != null) {
			for (BDDVarSet a : varSetWithout.values())
				a.free();
            varSetWithout = null;
		}

		if (G0L0equalsG1L1 != null) {
            G0L0equalsG1L1.free();
            G0L0equalsG1L1 = null;
        }

        if (G0equalsG1 != null) {
            G0equalsG1.free();
            G0equalsG1 = null;
        }

		if (G0equalsG3 != null) {
            G0equalsG3.free();
            G0equalsG3 = null;
        }

		if (G3equalsG4 != null) {
            G3equalsG4.free();
            G3equalsG4 = null;
        }

        if (L0equalsL2 != null) {
            L0equalsL2.free();
            L0equalsL2 = null;
        }

		for (BDDVarSet vs : varsets.values())
			vs.free();
        varsets.clear();

		if (g3g4ordering != null) {
            g3g4ordering.free();
            g3g4ordering = null;
        }

        if (factory != null)
            factory.clearAllDomains();
	}



    public String stringBDD(BDD bdd) {
        String s = "";
        for (int level = 0; level < varcopy; level++) {
            s += "  BDD L" + level + ":\n";
            for (int j = 0; j < lvmax; j++) {
                BDDDomain lvDom = doms[l0 + varcopy*j + level];
                s += "    Dom " + lvDom + ": " + domainVals(lvDom, bdd) + "\n";
            }
        }

        for (int level = 0; level < globalcopy; level++) {
            s += "  BDD G" + level + ":\n";
            for (int j = 0; j < gnum; j++) {
                BDDDomain gvDom = doms[g0 + globalcopy*j + level];
                s += "    Dom " + gvDom + ": " + domainVals(gvDom, bdd) + "\n";
            }
        }

        s += "  BDD Return Domain: " + domainVals(getRetVarDomain(), bdd) + "\n";

        s += "  Used temp domains: \n";
        for (BDDDomain tmpDom : usedTempDomains) {
            s += "    Tmp " + tmpDom + ": " + domainVals(tmpDom, bdd) + "\n";
        }

        s += "  Free temp domains: \n";
        for (BDDDomain tmpDom : freeTempDomains) {
            s += "    Tmp " + tmpDom + ": " + domainVals(tmpDom, bdd) + "\n";

        }


        return s;
    }

    public String domainVals(BDDDomain dom, BDD bdd) {
        String posVals = "";

        BDDIterator valItr = iterator(bdd, dom);
        while (valItr.hasNext()) {
            BDD valBDD = valItr.nextBDD();
            long val = valBDD.scanVar(dom).longValue();
            valBDD.free();
            posVals += val + ". ";
        }

        return posVals;
    }




    private class AssignDomainsIndices extends AbstractRemoplaVarVisitor {
        private int index;
        private long[] domSize;

        // Note: during recursive calls to handle, this variable needs to be stored
        // locally if it is to be used again (currently no need)
        private Variable v;
        private int varcopy;

        public AssignDomainsIndices() {}

        /** Performs the variable handling for the constructor.  Assigns
         * domains, indices and enum values
         * Note -- assumes all variables take up the default size -- fix!
         *   @param v the var to handle.
         *   @param index the next free index
         *   @param domSize the domain size array to initialise
         *   @param varcopy the number of copies of the variables
         *   @return the next index after some have been used by handle
         */
        public int handle(Variable v, int index, long[] domSize, int varcopy) {
            Variable ov = this.v;
            long[] oDomSize = this.domSize;
            int oVarCopy = this.varcopy;
            this.v = v;
            this.domSize = domSize;
            this.varcopy = varcopy;

            // don't *think* we need to save this
            this.index = index;

            v.getRemoplaVar().accept(this);

            this.v = ov;
            this.domSize = oDomSize;
            this.varcopy = oVarCopy;

            return this.index;
        }

        public void visit(RemoplaIntVar rv) {
            v.setIndex(index);
//            long gsize = (long) Math.pow(2, rv.getBits() != -1 ? rv.getBits() : bits);
            for (int i = 0; i < varcopy; i++)
                domSize[index++] = size;
        }

        public void visit(RemoplaBoolVar rv) {
            v.setIndex(index);
            for (int i = 0; i < varcopy; i++)
                domSize[index++] = size;
        }

        public void visit(RemoplaEnumVar rv) {
            v.setIndex(index);
//            long gsize = rv.getType().getValues().size();
            for (int i = 0; i < varcopy; i++)
                domSize[index++] = size;
            int i = 0;
            for (RemoplaEnumConstant ec : rv.getType().getValues()) {
                enumValues.put(ec, new Integer(i++));
            }
        }

        public void visit(RemoplaArrayVar rv) {
			for (Variable e : v.getSubVars()) {
                index = handle(e, index, domSize, varcopy);
			}
        }

        public void visit(RemoplaStructVar rv) {
            for(Variable f : v.getSubVars()) {
                index = handle(f, index, domSize, varcopy);
            }
        }

        public void visit(RemoplaUnsupportedVar rv) {
            // Ignore
        }

        public void defaultCase(RemoplaVar rv) {
            logger.error("Default case of VarManager.AssignDomainsIndices reached.  Should never occur!");
        }
    }
}

