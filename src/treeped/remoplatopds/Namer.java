/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// Namer.java
// Author: Matthew Hague
//
// Does the naming duties for the remople to pds translation

package treeped.remoplatopds;

import de.tum.in.wpds.Fa;

public class Namer {

    private static final String DEFAULT_LABEL = "DEFAULT_";
    private static final String CONTROL_STATE = Fa.q_i;
    private static final String RETURN_HANDLER = "RETURN_HANDLER_";

    private static int nextLabel = 0;
    private static int nextReturn = 0;

    public static String getNewLabel() {
        return DEFAULT_LABEL + (nextLabel++);
    }


    public static String getControlState() {
        return CONTROL_STATE;
    }


    public static String getNewReturnHandler() {
        return RETURN_HANDLER + (nextReturn++);
    }
}
