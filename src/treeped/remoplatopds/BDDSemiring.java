/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// BDDSemiring.java
// Author: Matthew Hague
//
// Based on the jMoped2 BDDSemiring in the underbone checker.  Implements a
// standard BDD semiring, but with the added capability of dealing with
// degenerate semiring elements of type ExprSemiring.


package treeped.remoplatopds;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import de.tum.in.wpds.CancelMonitor;
import de.tum.in.wpds.PreCallSemiring;
import de.tum.in.wpds.Semiring;

import treeped.representation.*;
import treeped.exceptions.VariableNotFoundException;

import net.sf.javabdd.BDD.BDDIterator;
import net.sf.javabdd.BDD;
import net.sf.javabdd.BDDDomain;
import net.sf.javabdd.BDDFactory;
import net.sf.javabdd.BDDPairing;
import net.sf.javabdd.BDDVarSet;

import org.apache.log4j.Logger;

public class BDDSemiring implements Semiring, RemoplaExprVisitor {

    static Logger logger = Logger.getLogger(BDDSemiring.class);

    VarManager manager;

    public BDD bdd;

    private ExprSemiring currentExtendSemiring = null;

    /**
     * The constructor.
     *
     * @param bdd the BDD.
     */
    public BDDSemiring(VarManager manager, BDD bdd) {
        assert manager != null && bdd != null;
        this.manager = manager;
        this.bdd = bdd;
    }

    /**
     * Disjuncts this BDD with the BDD of <code>a</code>.
     *
     * @param a the semiring.
     * @return the disjuncted semiring.
     *
     */
    public Semiring combine(Semiring a) {
        BDD combined = bdd.or(((BDDSemiring) a).bdd);
        return new BDDSemiring(manager, combined);
    }

	public Semiring diff(Semiring a) {
		BDD thatbdd = ((BDDSemiring) a).bdd;

		if (bdd.equals(thatbdd)) {
			return new BDDSemiring(manager, manager.getFactory().zero());
        }

		BDD newbdd =  bdd.and(thatbdd.not());
		return new BDDSemiring(manager, newbdd);
	}

    // uses the visitor pattern to translate the semiring.  The result is stored
    // in extendedSemiring by the visitor methods.  (Defined above).  The
    // visitor methods are defined immediately below.
    private BDD extendedBDD;
    public BDDSemiring extend(Semiring a, CancelMonitor monitor) {
        BDDFactory factory = bdd.getFactory();
        if (bdd.isZero()) return new BDDSemiring(manager, factory.zero());

        ExprSemiring A = (ExprSemiring) a;

        ExprSemiring oldExtendSemiring = currentExtendSemiring;
        currentExtendSemiring = A;

        extendedBDD = addCondition(bdd.id(), A.getCondition());
        if (extendedBDD.isZero()) {
            currentExtendSemiring = oldExtendSemiring;
            return new BDDSemiring(manager, factory.zero());
        }

        if (A.isReturnAssign()) {
            returnAssignExtendedBDD(A.getReturnAssign());
        } else {
            extendExtendedBDD(A.getStmt());

            StmtOptimisations opts = manager.getStmtOptimisations(A.getStmt());
            if (opts != null && opts.hasDyingVarsInfo()) {
                BDD tmp = abstractVars(extendedBDD, opts.getDyingVars());
                extendedBDD.free();
                extendedBDD = tmp;
            }


        }

        currentExtendSemiring = oldExtendSemiring;

        return new BDDSemiring(manager, extendedBDD);
    }

    /** @param bdd a bdd containing the current set up, consumed by the method
     * @param guard a condition bdd must satisfy
     * @return bdd.andWith(cond)
     */
    private BDD addCondition(BDD bdd, RemoplaExpr guard) {
        convertBDD = bdd;
        if (guard != null) {
            mode = Mode.TRANS_EXPR;
            BDDDomain gDom = translateExpression(guard, null);
            convertBDD.andWith(gDom.ithVar(VarManager.encode(VarManager.FALSE, gDom)).not());
            if (manager.isTempDomain(gDom)) {
                BDD tmp = abstractVars(convertBDD, gDom);
                convertBDD.free();
                convertBDD = tmp;
                manager.freeIfTempDomain(gDom);
            }
        }
        return convertBDD;
    }

    /**
     * @param bdd a bdd containing the current set up, consumed by the method
     * @param dom a bdd domain
     * @param e an expression whos value in the bdd must match the value of
     * dom
     * @return bdd.andWith(dom == e)
     */
    private BDD addEqualityCondition(BDD bdd,
                                     final BDDDomain dom,
                                     final RemoplaExpr e) {
        convertBDD = bdd;
        mode = Mode.TRANS_EXPR;

        final BDDSemiring parent = this;
        e.accept(new AbstractRemoplaExprVisitor() {

            public void visit(RemoplaBoolConstant obj) {
                constantCondition(obj.getVal() ? 1 : 0);
            }

            public void visit(RemoplaIntConstant obj) {
                constantCondition(obj.getVal());
            }

            public void visit(RemoplaIntConstVar obj) {
                constantCondition(obj.getValue());
            }

            public void defaultCase(RemoplaExpr e) {
                BDDDomain tmpDom = parent.translateExpression(e, null);
                parent.convertBDD.andWith(manager.bddEquals(dom, tmpDom));
                if (manager.isTempDomain(tmpDom)) {
                    BDD tmp = abstractVars(convertBDD, tmpDom);
                    parent.convertBDD.free();
                    parent.convertBDD = tmp;
                    manager.freeIfTempDomain(tmpDom);
                }
            }

            private void constantCondition(long val) {
                long e = VarManager.encode(val, dom);
                convertBDD.andWith(dom.ithVar(e));
            }
        });

        return convertBDD;
    }


    private void extendExtendedBDD(RemoplaStmt s) {
        final BDDSemiring ring = this;

        s.accept(new AbstractRemoplaStmtVisitor() {
            public void visit(RemoplaAssignStmt rassign) {
                ring.extendAssign(rassign);
            }

            public void visit(RemoplaInvokeStmt rinv) {
                ring.extendInvoke(rinv);
            }

            public void visit(RemoplaReturnStmt rstmt) {
                ring.extendReturn(rstmt);
            }


            public void visit(RemoplaSkipStmt skip) {
                ring.extendSkip(skip);
            }

            public void defaultCase(RemoplaStmt stmt) {
                ring.logger.error("stmt " +
                                  stmt +
                                  " should not appear on a pushdown rule.");
            }
        });
    }

    public void extendAssign(RemoplaAssignStmt rassign) {
        if (canProcessAssignDangerously(rassign))
            doDangerousAssign(rassign);
        else
            doSafeAssign(rassign);
    }

    public void extendInvoke(RemoplaInvokeStmt rinv) {
        if (currentExtendSemiring.getRuleType() == ExprSemiring.RuleType.PUSH)
            translateMethodCallPush(rinv.getInvokeExpr());
        else
            translateMethodCallRewrite(rinv.getInvokeExpr());
    }

    public void extendReturn(RemoplaReturnStmt rstmt) {
        // Handle return value
        RemoplaExpr retVal = rstmt.getReturnValue();
        if (retVal != null) {
            BDDDomain retDom = manager.getRetVarDomain();
            convertBDD = extendedBDD;
            mode = Mode.TRANS_EXPR;
            translateExpression(retVal, retDom);
            extendedBDD = convertBDD;
        }
    }

    public void extendSkip(RemoplaSkipStmt skip) {
        RemoplaExpr guard = skip.getGuard();
        extendedBDD = addCondition(extendedBDD, guard);
    }

    public void returnAssignExtendedBDD(RemoplaExpr assignTo) {
        convertBDD = extendedBDD;
        mode = Mode.TRANS_EXPR;
        BDDDomain lhs = translateExpression(assignTo, null);
        if (manager.isTempDomain(lhs)) {
            logger.error("Assignment to a temporary domain.");
        }

        BDDDomain retDom = manager.getRetVarDomain();

        BDD tmp = abstractVars(convertBDD, lhs);
        convertBDD.free();
        convertBDD = tmp;
        convertBDD.replaceWith(manager.getFactory().makePair(retDom, lhs));

        extendedBDD = convertBDD;
    }


    /** An unsafe assign writes all values directly to the lhs, which may
     * overwrite variables used in later assigns.
     */
    private void doDangerousAssign(RemoplaAssignStmt rassign) {
        BDDFactory factory = bdd.getFactory();

        // get vals
        convertBDD = extendedBDD;
        mode = Mode.TRANS_EXPR;
        for (RemoplaAssignStmt.Assign a : rassign.getAssignments()) {
            BDDDomain assignDom = translateExpression(a.rhs, null);
            if (a.lhs instanceof RemoplaArrayRefExpr) {
                RemoplaArrayRefExpr lhs = (RemoplaArrayRefExpr)a.lhs;
                if (lhs.getDimension() == 1) {
                    assignToOneDimArray(lhs, assignDom);
                } else {
                    assignToTwoDimArray(lhs, assignDom);
                }

                if (manager.isTempDomain(assignDom)) {
                    BDD tmp = abstractVars(convertBDD, assignDom);
                    convertBDD.free();
                    convertBDD = tmp;
                    manager.freeIfTempDomain(assignDom);
                }
            } else {
                BDDDomain lhs = translateExpression(a.lhs, null);
                if (manager.isTempDomain(lhs)) {
                    logger.error("Assignment to a temporary domain.");
                }

                // Self assign is slightly pathological
                if (lhs != assignDom) {
                    BDD tmp = abstractVars(convertBDD, lhs);
                    convertBDD.free();
                    convertBDD = tmp;

                    if (manager.isTempDomain(assignDom)) {
                        convertBDD.replaceWith(factory.makePair(assignDom, lhs));
                        manager.freeIfTempDomain(assignDom);
                    } else {
                        convertBDD.andWith(manager.bddEquals(assignDom, lhs));
                    }
                }

            }
        }

        extendedBDD = convertBDD;
    }

    /** A safe assign writes all values to a temporary variable (one per assign)
     * then copies the values to the rhs.
     */
    private void doSafeAssign(RemoplaAssignStmt rassign) {
        BDDFactory factory = bdd.getFactory();

        int nassigns = rassign.getAssignments().size();
        BDDDomain[] assignDoms = new BDDDomain[nassigns];
        RemoplaAssignStmt.Assign[] assignments = new RemoplaAssignStmt.Assign[nassigns];

        // get vals
        convertBDD = extendedBDD;
        mode = Mode.TRANS_EXPR;
        int i = -1;
        for (RemoplaAssignStmt.Assign a : rassign.getAssignments()) {
            i = i + 1;
            assignDoms[i] = manager.getTempVarDomain();
            translateExpression(a.rhs, assignDoms[i]);
            assignments[i] = a;
        }

        // do assign
        for (i = 0; i < nassigns; i++) {
            if (assignments[i].lhs instanceof RemoplaArrayRefExpr) {
                RemoplaArrayRefExpr lhs = (RemoplaArrayRefExpr)assignments[i].lhs;
                if (lhs.getDimension() == 1) {
                    assignToOneDimArray(lhs, assignDoms[i]);
                } else {
                    assignToTwoDimArray(lhs, assignDoms[i]);
                }

                BDD tmp = abstractVars(convertBDD, assignDoms[i]);
                convertBDD.free();
                convertBDD = tmp;
                manager.freeIfTempDomain(assignDoms[i]);
            } else {
                BDDDomain lhs = translateExpression(assignments[i].lhs, null);
                if (manager.isTempDomain(lhs)) {
                    logger.error("Assignment to a temporary domain.");
                }

                BDD tmp = abstractVars(convertBDD, lhs);
                convertBDD.free();
                convertBDD = tmp;
                convertBDD.replaceWith(factory.makePair(assignDoms[i], lhs));
                manager.freeIfTempDomain(assignDoms[i]);
            }
        }

        extendedBDD = convertBDD;
    }

    private void assignToOneDimArray(RemoplaArrayRefExpr ar, BDDDomain rhs) {
        if (isConstant(ar.getIndex())) {
            constantIndexAssignToOneDimArray(ar, rhs);
        } else {
            variableIndexAssignToOneDimArray(ar, rhs);
        }
    }

    private void constantIndexAssignToOneDimArray(RemoplaArrayRefExpr ar, BDDDomain rhs) {
        long index = constantValue(ar.getIndex(), rhs);

        if (index < ar.getArray().getMinIndex() || index > ar.getArray().getMaxIndex()) {
            logger.error("Array bound violation (assign): index " + index + " out of [" +
                         ar.getArray().getMinIndex() + "," + ar.getArray().getMaxIndex() + "]");
        } else {
            // Gets arr[index]
            BDDDomain eleDom = manager.getArrayElementDomain(ar.getArray(), index);

            // for the current value of index, abstract the relevant array
            // element, and set the new value
            BDD tmp = abstractVars(convertBDD, eleDom);
            convertBDD.free();
            tmp.andWith(manager.bddEquals(eleDom, rhs));
            convertBDD = tmp;
        }
    }

    private void variableIndexAssignToOneDimArray(RemoplaArrayRefExpr ar, BDDDomain rhs) {
        // Gets the index domains
        BDDDomain indexDom = translateExpression(ar.getIndex(), null);

        // Gets all possible index values
        BDDIterator indItr = manager.iterator(convertBDD, indexDom);

        BDDFactory factory = bdd.getFactory();
        BDD nextBDD = factory.zero();
        while (indItr.hasNext()) {
            // Gets an index value
            BDD f = indItr.nextBDD();
            int index = VarManager.decode(f.scanVar(indexDom).longValue(), indexDom);
            f.free();

            if (index < ar.getArray().getMinIndex() || index > ar.getArray().getMaxIndex()) {
                logger.error("Array bound violation (assign): index " + index + " out of [" +
                             ar.getArray().getMinIndex() + "," + ar.getArray().getMaxIndex() + "]");
                continue;
            }

            // Gets arr[index]
            BDDDomain eleDom = manager.getArrayElementDomain(ar.getArray(), index);

            // for the current value of index, abstract the relevant array
            // element, and set the new value
            f = abstractVars(convertBDD, eleDom);
            f.andWith(indexDom.ithVar(VarManager.encode(index, indexDom)).andWith(manager.bddEquals(eleDom, rhs)));

            nextBDD.orWith(f);
        }

        convertBDD.free();
        convertBDD = nextBDD;

        if (manager.isTempDomain(indexDom)) {
            BDD tmp = abstractVars(convertBDD, indexDom);
            convertBDD.free();
            convertBDD = tmp;
            manager.freeIfTempDomain(indexDom);
        }
    }

    private void assignToTwoDimArray(RemoplaArrayRefExpr lhs, BDDDomain rhs) {
        // TODO: Implement me
        logger.error("BDDSemiring.assignToTwoDimArray() not implemented!");
    }

	/**
	 * Method invocation: (G0,L0,G1,L1) -> (G2,L2,L0,G1,L1)
     *
     * This handles the (standard) case where we create a new stack frame to run
     * the method in.
	 *
	 * @param A
	 * @return
	 */
    private void translateMethodCallPush(RemoplaMethodCall mc) {
        // first clear G2 and L2 since we will be assigning to them
        convertBDD = extendedBDD.exist(manager.getG2L2VarSet());
        extendedBDD.free();


        List<RemoplaExpr> parameters = mc.getParameters();
        List<RemoplaVar> paramVars = manager.getMethodParameters(mc.getCallString());


        int nargs = parameters.size();

        mode = Mode.TRANS_EXPR;

        Iterator iargs = parameters.iterator();
        Iterator ivars = paramVars.iterator();
        List<BDDDomain> valDoms = new ArrayList<BDDDomain>(nargs);
        List<Variable> argVars = new ArrayList<Variable>(nargs);
        while (iargs.hasNext()) {
            RemoplaExpr rarg = (RemoplaExpr)iargs.next();
            RemoplaVar rvar = (RemoplaVar)ivars.next();

            Variable argv = manager.getVar(rvar);

            // if the arg is a straight variable, it will be assigned a
            // sensible expression, else it will be an array or struct
            // transfer
            if (argv.getSubVars() == null) {
                valDoms.add(translateExpression(rarg, null));
                argVars.add(argv);
            } else {
                logger.error("BDDSemiring: passing of arrays or structures is not currently supported.");
            }

        }

        // G0 becomes G2
        convertBDD.replaceWith(manager.getG0pairG2());
        // L2 is the new value of the local vars (?)
        // bddL0equalsL2params just asserts the L2 local var corresponding to a
        // param equals the value specified by the appropriate valDom (which was
        // assigned above)
        convertBDD.andWith(manager.bddL0equalsL2params(valDoms, argVars));

        // abstract temps
        for (BDDDomain d : valDoms) {
            if (manager.isTempDomain(d)) {
                BDD tmp = abstractVars(convertBDD, d);
                convertBDD.free();
                convertBDD = tmp;
                manager.freeIfTempDomain(d);
            }
        }

        extendedBDD = convertBDD;
    }


    /* Sometimes a method call appears at the end of a method, and so the return
     * is directly to the caller's caller.  Hence, treat the rule just as a
     * rewrite of the current stack frame's local variables
     */
    private void translateMethodCallRewrite(RemoplaMethodCall mc) {
        extendedBDD = setupCalledLocals(mc, extendedBDD);
    }





    /**
     * the dual of extend.  I.e. bdd.prepend(r) == r.extend(bdd).  We do it this
     * way because the result is a BDD, so better to have access to the BDD
     * data.
     *
     * @param a the semiring to prepend (either exprsemiring or precallsemiring)
     * @param monitor a monitor i think we'll ignore for now
     * @return a bdd representing a.extend(this)
     */
    public BDDSemiring prepend(Semiring a, CancelMonitor monitor) {
        if (a instanceof ExprSemiring) {
            return prependExprSemiring((ExprSemiring)a, monitor);
        } else if (a instanceof PreCallSemiring) {
            return prependPreCallSemiring((PreCallSemiring)a, monitor);
        } else {
            logger.error("BDDSemiring.prepend of semiring of unknown class!");
            return null;
        }
    }


    private BDDSemiring prependPreCallSemiring(PreCallSemiring a,
                                               CancelMonitor monitor) {
        // a is R1(G0,L0,G1) is them
        // we are R2(G0,L0,G1) is us
        // we want (E)G . R1(G0,L0,G) & R2(G,L0,G1)

        BDD us = bdd.id();
        BDD them = a.getBDD().bdd.id();

		us.replaceWith(manager.getG0pairG2());
        them.replaceWith(manager.getG1pairG2());

        us.andWith(them);

        BDD tmp = us.exist(manager.getG2VarSet());
        us.free();
        us = tmp;

        return new BDDSemiring(manager, us);
    }


    // Prepend below also uses the visitor pattern to translate the semiring.
    // The result is stored in extendedBDD (why not reuse?).
    private BDDSemiring prependExprSemiring(ExprSemiring a, CancelMonitor monitor) {
        BDDFactory factory = bdd.getFactory();
        if (bdd.isZero()) return new BDDSemiring(manager, factory.zero());

        ExprSemiring oldExtendSemiring = currentExtendSemiring;
        currentExtendSemiring = a;

        extendedBDD = bdd.id();

        final BDDSemiring ring = this;
        a.getStmt().accept(new AbstractRemoplaStmtVisitor() {
            public void visit(RemoplaAssignStmt rassign) {
                ring.prependAssign(rassign);
            }

            public void visit(RemoplaInvokeStmt rinv) {
                ring.prependInvoke(rinv);
            }

            public void visit(RemoplaReturnStmt rstmt) {
                ring.prependReturn(rstmt);
            }

            public void visit(RemoplaSkipStmt skip) {
                ring.prependSkip(skip);
            }

            public void defaultCase(RemoplaStmt stmt) {
                BDDSemiring.logger.error("stmt " +
                                         stmt +
                                         " should not appear on a pushdown rule.");
            }
        });

        extendedBDD = addCondition(extendedBDD, a.getCondition());

        currentExtendSemiring = oldExtendSemiring;

        return new BDDSemiring(manager, extendedBDD);
    }

    void prependAssign(RemoplaAssignStmt rassign) {
        // (x1 = e1, ..., xn = en) ++ BDD

        // 1. rename x1, ..., xn into tmp1, ..., tmpn
        // 2. assert tmp1 = e1, ..., tmpn = en
        //    this constrains values of x1, ..., xn appearing in e1, ..., en
        // 3. abstract away tmp1, ..., tmpn since they are are use after
        //    contrain
        //
        // that is:
        //
        //     (E)tmp1,...,tmpn . (BDD[x1/tmp1, ..., xn/tmpn] &
        //                         tmp1 = e1 & ... & tmpn = en)
        BDDFactory factory = bdd.getFactory();

        int nassigns = rassign.getAssignments().size();
        BDDDomain[] x2Doms = new BDDDomain[nassigns];
        BDDDomain[] xDoms = new BDDDomain[nassigns];

        // get tmps and xs
        convertBDD = extendedBDD;
        mode = Mode.TRANS_EXPR;
        int i = -1;
        for (RemoplaAssignStmt.Assign a : rassign.getAssignments()) {
            i = i + 1;

            // ugh -- assuming only ever a var on the lhs...
            x2Doms[i] = manager.get2VarDomain((RemoplaVar)a.lhs);
            xDoms[i] = manager.getVarDomain((RemoplaVar)a.lhs);
        }

        // BDD[x to l2]


        BDDPairing p = factory.makePair();
		p.set(xDoms, x2Doms);
        convertBDD.replaceWith(p);

        // BDD[x to l2] & x1(2) = e1 & ... & xn(2) = en
        i = -1;
        for (RemoplaAssignStmt.Assign a : rassign.getAssignments()) {
            i = i + 1;
            convertBDD = addEqualityCondition(convertBDD, x2Doms[i], a.rhs);
        }



        // (E) x2s. (...)
        BDD tmp = abstractVars(convertBDD, x2Doms);
        convertBDD.free();
        convertBDD = tmp;

        extendedBDD = convertBDD;
    }

    void prependInvoke(RemoplaInvokeStmt rinv) {
        if (currentExtendSemiring.getRuleType() == ExprSemiring.RuleType.PUSH) {
            prependMethodCallPush(rinv.getInvokeExpr());
        } else {
            prependMethodCallRewrite(rinv.getInvokeExpr());
        }
    }

    void prependMethodCallPush(RemoplaMethodCall mc) {
        // 1. store values of local variables into L2 (in particular argument
        // vars a1...an
        // 2. which abstracts them away since they won't be in scope before call
        // 3. add constraints to new local variables (and indeed global, but they
        //    remain in scope and are unchanged by call) to assert that their
        //    values lead to something that could have given the argument values
        //    (old local values)
        //
        //        i.e. ai(in L2) = ei
        //
        //    where e1, ..., en are the expressions for the arguments
        // 4. abstract away L2 since not needed anymore

        convertBDD = extendedBDD;
        mode = Mode.TRANS_EXPR;
        BDDFactory factory = bdd.getFactory();
        BDD tmp;

        // get tmp domains and av domains
        List<RemoplaExpr> parameters = mc.getParameters();
        List<RemoplaVar> paramVars = manager.getMethodParameters(mc.getCallString());

        List<Variable> argVars = new ArrayList<Variable>();
        for (RemoplaVar v : paramVars) {
            argVars.add(manager.getVar(v));
        }

        // BDD[L0->L2]
        convertBDD.replaceWith(manager.getL0pairL2());
        // (E) L0 . BDD[...] (implied by above)


        // ((E) L0 . BDD[...]) & av1(2) = e1 & ... & avn(2) = en
        Iterator<RemoplaExpr> iexprs = parameters.iterator();
        Iterator<Variable> iargs = argVars.iterator();
        while (iexprs.hasNext()) {
            RemoplaExpr e = iexprs.next();
            Variable a = iargs.next();
            BDDDomain adom = manager.get2VarDomain(a);
            convertBDD = addEqualityCondition(convertBDD, adom, e);
        }

        // done with
        tmp = manager.abstractL2(convertBDD);
        convertBDD.free();
        convertBDD = tmp;

        extendedBDD = convertBDD;

    }

    void prependMethodCallRewrite(RemoplaMethodCall mc) {
        logger.error("prepend has not implemented method calls on rewrite rules");
    }

    void prependReturn(RemoplaReturnStmt rstmt) {
        logger.warn("prepend with a return statement should never be called.");
        RemoplaExpr retVal = rstmt.getReturnValue();
        if (retVal != null) {
            logger.error("prepend not implemented for return statements with a return value!");
        }
    }


    void prependSkip(RemoplaSkipStmt skip) {
        RemoplaExpr guard = skip.getGuard();
        extendedBDD = addCondition(extendedBDD, guard);
    }



    ///////////////////////////////////////////////////////////////
    //  Translate expressions
    //
    //  Note on usage of visitor pattern.  It is overloaded.  It is used first
    //  to translate expressions.  The result of a translated expression is the
    //  bdd variable domain containing the result.  This does not make sense
    //  when the expression is an array or a struct var, hence the expression
    //  translator should be able to ignore this case (assuming that expressions
    //  are well formed, and array var should be wrapped in an array reference
    //  expression).
    //
    //  In the case of method calls, we need to be able to pass arrays and
    //  struct vars as values.  In these cases, array vars and struct vars make
    //  sense on their own, but we need the list of domains they represent.
    //  Hence we have a getExprDomains method that deals with this case.
    //  This overloads the visitor pattern, dealing with only the two cases
    //  above.  The mode variable is here to assert that these two uses do not
    //  get mixed up.
    //
    //  All of this is indicative of bad design, and some refactoring should be
    //  done in the future.

    private enum Mode {
        TRANS_EXPR, GETTING_ARR_STRUCT_DOMS, NONE;
    };
    Mode mode;

    /** Returns a domain with the result of an expression in it.  Values are
     * derived from convertBDD, which is consumed in the process, and finishes
     * with the bdd assigning the right value to the returned domain.
     *
     * @param e the expression to translate
     * @param resultDomain the domain to assign the result to.  If null, then a new domain
     * will be created, or, if the expression is a variable, just the variable
     * domain will be used
     * @return the bdd domain for the expression.  If a resultDomain was passed,
     * the result is resultDomain.  If the expression is a variable, the
     * variable domain is returned.  For an expression, a temporary domain will
     * be returned.  As a side effect, convertBDD contains the bdd with the
     * result of the expression in it.  This needs to be free-ed.
     */
    private BDDDomain resultDomain;
    private BDD convertBDD;
    private BDDDomain translateExpression(RemoplaExpr e, BDDDomain resultDomain) {
        assert mode == Mode.TRANS_EXPR && convertBDD != null;

        BDDDomain ord = this.resultDomain;
        this.resultDomain = resultDomain;
        e.accept(this);
        BDDDomain newResult = this.resultDomain;
        this.resultDomain = ord;


        return newResult;
    }

    public void visit(RemoplaArrayRefExpr ar) {
        assert mode == Mode.TRANS_EXPR;
        if (ar.getDimension() == 1) {
            oneDimArrayRef(ar);
        } else {
            twoDimArrayRef(ar);
        }
    }

    private void oneDimArrayRef(RemoplaArrayRefExpr ar) {
        if (isConstant(ar.getIndex())) {
            constantIndexOneDimArrayRef(ar);
        } else {
            variableIndexOneDimArrayRef(ar);
        }
    }

    private void constantIndexOneDimArrayRef(RemoplaArrayRefExpr ar) {
        BDDDomain tempDom = manager.getTempVarDomain();

        long index = constantValue(ar.getIndex(), tempDom);

        if (index < ar.getArray().getMinIndex() || index > ar.getArray().getMaxIndex()) {
            logger.error("Array bound violation (ref): index " + index + " out of [" +
                         ar.getArray().getMinIndex() + "," + ar.getArray().getMaxIndex() + "]");
        } else {
            BDDDomain eleDom = manager.getArrayElementDomain(ar.getArray(), index);
            convertBDD.andWith(manager.bddEquals(tempDom, eleDom));
        }

        if (resultDomain != null) {
            BDD tmp = abstractVars(convertBDD, resultDomain);
            convertBDD.free();
            convertBDD = tmp;
            convertBDD.replaceWith(manager.getFactory().makePair(tempDom, resultDomain));
            manager.freeIfTempDomain(tempDom);
        } else {
            resultDomain = tempDom;
        }
    }


    private void variableIndexOneDimArrayRef(RemoplaArrayRefExpr ar) {
        // Gets the stack domains
        BDDDomain indexDom = translateExpression(ar.getIndex(), null);

        BDDDomain tempDom = manager.getTempVarDomain();

        // Gets all possible index values
        BDDIterator indItr = manager.iterator(convertBDD, indexDom);

        BDDFactory factory = bdd.getFactory();
        BDD d = factory.zero();
        while (indItr.hasNext()) {
            // Gets an index value
            BDD f = indItr.nextBDD();
            int index = VarManager.decode(f.scanVar(indexDom).longValue(), indexDom);
            f.free();

            if (index < ar.getArray().getMinIndex() || index > ar.getArray().getMaxIndex()) {
                logger.error("Array bound violation (ref): index " + index + " out of [" +
                             ar.getArray().getMinIndex() + "," + ar.getArray().getMaxIndex() + "]");
                continue;
            }

            BDDDomain eleDom = manager.getArrayElementDomain(ar.getArray(), index);
            d.orWith(indexDom.ithVar(VarManager.encode(index, indexDom)).andWith(manager.bddEquals(tempDom, eleDom)));
        }

        convertBDD.andWith(d);

        if (manager.isTempDomain(indexDom)) {
            BDD tmp = abstractVars(convertBDD, indexDom);
            convertBDD.free();
            convertBDD = tmp;
            manager.freeIfTempDomain(indexDom);
        }

        if (resultDomain != null) {
            BDD tmp = abstractVars(convertBDD, resultDomain);
            convertBDD.free();
            convertBDD = tmp;
            convertBDD.replaceWith(factory.makePair(tempDom, resultDomain));
            manager.freeIfTempDomain(tempDom);
        } else {
            resultDomain = tempDom;
        }
    }

    private void twoDimArrayRef(RemoplaArrayRefExpr ar) {
        // TODO
    }


    public void visit(RemoplaBinExpr be) {
        assert mode == Mode.TRANS_EXPR;

        boolean constLeft = isConstant(be.getLeft());
        boolean constRight = isConstant(be.getRight());

        if (constLeft) {
            if (constRight) {
                handleConstantBinExpr(be);
            } else {
                handleLeftConstantBinExpr(be);
            }
        } else {
            if (constRight) {
                handleRightConstantBinExpr(be);
            } else {
                handleNonConstantBinExpr(be);
            }
        }
    }

    private void handleConstantBinExpr(RemoplaBinExpr be) {
        if (resultDomain != null) {
            BDD tmp = abstractVars(convertBDD, resultDomain);
            convertBDD.free();
            convertBDD = tmp;
        } else {
            resultDomain = manager.getTempVarDomain();
        }

        long left = constantValue(be.getLeft(), resultDomain);
        long right = constantValue(be.getRight(), resultDomain);


        convertBDD.andWith(resultDomain.ithVar(manager.arith(be.getType(), resultDomain, left, resultDomain, right, resultDomain)));
    }

    private void handleLeftConstantBinExpr(RemoplaBinExpr be) {
        // Gets the operand domains
        BDDDomain op2dom = translateExpression(be.getRight(), null);
        long left = constantValue(be.getLeft(), op2dom);
        BDDDomain tempDom = manager.getTempVarDomain();

        BDDFactory factory = bdd.getFactory();

        // Gets all possible op2 values wrt. op1
        BDDIterator op2itr = manager.iterator(convertBDD, op2dom);

        // Performs arithmetic function with op2 and op1
        BDD d = factory.zero();
        while (op2itr.hasNext()) {
            // Gets an op2 value
            BDD tmp = op2itr.nextBDD();
            long op2 = tmp.scanVar(op2dom).longValue();
            tmp.free();

            long r = manager.arith(be.getType(), tempDom, left, op2dom, op2, op2dom);
            d.orWith(op2dom.ithVar(op2).andWith(tempDom.ithVar(r)));
        }

        convertBDD.andWith(d);

        if (manager.isTempDomain(op2dom)) {
            BDD tmp = abstractVars(convertBDD, op2dom);
            convertBDD.free();
            convertBDD = tmp;
            manager.freeIfTempDomain(op2dom);
        }


        if (resultDomain != null) {
            BDD tmp = abstractVars(convertBDD, resultDomain);
            convertBDD.free();
            convertBDD = tmp;
            convertBDD.replaceWith(factory.makePair(tempDom, resultDomain));
            manager.freeIfTempDomain(tempDom);
        } else {
            resultDomain = tempDom;
        }
    }

    private void handleRightConstantBinExpr(RemoplaBinExpr be) {
        // Gets the operand domains
        BDDDomain op1dom = translateExpression(be.getLeft(), null);
        long right = constantValue(be.getRight(), op1dom);
        BDDDomain tempDom = manager.getTempVarDomain();

        BDDFactory factory = bdd.getFactory();

        // Gets all possible op1 values
        BDDIterator op1itr = manager.iterator(convertBDD, op1dom);
        BDD d = factory.zero();
        while (op1itr.hasNext()) {
            BDD tmp = op1itr.nextBDD();
            long op1 = tmp.scanVar(op1dom).longValue();
            tmp.free();

            long r = manager.arith(be.getType(), tempDom, op1, op1dom, right, op1dom);
            d.orWith(op1dom.ithVar(op1).andWith(tempDom.ithVar(r)));
        }

        convertBDD.andWith(d);

        if (manager.isTempDomain(op1dom)) {
            BDD tmp = abstractVars(convertBDD, op1dom);
            convertBDD.free();
            convertBDD = tmp;
            manager.freeIfTempDomain(op1dom);
        }

        if (resultDomain != null) {
            BDD tmp = abstractVars(convertBDD, resultDomain);
            convertBDD.free();
            convertBDD = tmp;
            convertBDD.replaceWith(factory.makePair(tempDom, resultDomain));
            manager.freeIfTempDomain(tempDom);
        } else {
            resultDomain = tempDom;
        }
    }

    private void handleNonConstantBinExpr(RemoplaBinExpr be) {
        // Gets the operand domains
        BDDDomain op1dom = translateExpression(be.getLeft(), null);
        BDDDomain op2dom = translateExpression(be.getRight(), null);
        BDDDomain tempDom = manager.getTempVarDomain();

        BDDFactory factory = bdd.getFactory();
        BDD d = factory.zero();

        // Gets all possible op1 values
        BDDIterator op1itr = manager.iterator(convertBDD, op1dom);
        while (op1itr.hasNext()) {
            BDD e = op1itr.nextBDD();
            long op1 = e.scanVar(op1dom).longValue();
            e.free();

            // Not much of an optimisation, but it doesn't hurt
            // (Initial idea was to avoid having to translate the right hand
            // side, but delaying translation of the right hand side until after
            // we have an op1 iterator causes problems (allocation of temp
            // domains, i think).  TODO: fix.
            if (manager.binExpHasValueFromLeft(be.getType(), op1, op1dom)) {
                long val = manager.binExpValueFromLeft(be.getType(), op1, op1dom, tempDom);
                d.orWith(op1dom.ithVar(op1).andWith(tempDom.ithVar(val)));
            } else {
                // Gets all possible op2 values wrt. op1
                e = convertBDD.id().andWith(op1dom.ithVar(op1));
                BDDIterator op2itr = manager.iterator(e, op2dom);
                e.free();

                // Performs arithmetic function with op2 and op1
                BDD f = factory.zero();
                while (op2itr.hasNext()) {
                    // Gets an op2 value
                    BDD g = op2itr.nextBDD();
                    long op2 = g.scanVar(op2dom).longValue();
                    g.free();

                    long r = manager.arith(be.getType(), tempDom, op1, op1dom, op2, op2dom);
                    f.orWith(op2dom.ithVar(op2).andWith(tempDom.ithVar(r)));
                }
                d.orWith(op1dom.ithVar(op1).andWith(f));
            }
         }

        // finally, add the appropriate conditions to the result BDD
        convertBDD.andWith(d);

        if (manager.isTempDomain(op1dom)) {
            BDD tmp = abstractVars(convertBDD, op1dom);
            convertBDD.free();
            convertBDD = tmp;
            manager.freeIfTempDomain(op1dom);
        }

        if (manager.isTempDomain(op2dom)) {
            BDD tmp = abstractVars(convertBDD, op2dom);
            convertBDD.free();
            convertBDD = tmp;
            manager.freeIfTempDomain(op2dom);
        }


        if (resultDomain != null) {
            BDD tmp = abstractVars(convertBDD, resultDomain);
            convertBDD.free();
            convertBDD = tmp;
            convertBDD.replaceWith(factory.makePair(tempDom, resultDomain));
            manager.freeIfTempDomain(tempDom);
        } else {
            resultDomain = tempDom;
        }

    }







    public void visit(RemoplaBoolConstant bc) {
        assert mode == Mode.TRANS_EXPR;
        translateConstant(bc.getVal() ? 1 : 0);
    }

    public void visit(RemoplaBoolVar bv) {
        assert mode == Mode.TRANS_EXPR;
        translateVariable(bv);
    }

    public void visit(RemoplaEnumConstant ec) {
        assert mode == Mode.TRANS_EXPR;
        int ev = manager.getEnumValue(ec);
        if (ev > -1) {
            translateConstant(ev);
        }
    }

    public void visit(RemoplaEnumVar ev) {
       assert mode == Mode.TRANS_EXPR;
       translateVariable(ev);
    }

    public void visit(RemoplaIntConstant ic) {
       assert mode == Mode.TRANS_EXPR;
       translateConstant(ic.getVal());
    }

    public void visit(RemoplaIntConstVar icv) {
        assert mode == Mode.TRANS_EXPR;
        translateConstant(icv.getValue());
    }

    public void visit(RemoplaIntVar iv) {
      assert mode == Mode.TRANS_EXPR;
      translateVariable(iv);
    }




    public void visit(RemoplaUnaryExpr ue) {
        assert mode == Mode.TRANS_EXPR;

        // Gets the operand domains
        BDDDomain op1dom = translateExpression(ue.getExpr(), null);

        BDDDomain tempDom = manager.getTempVarDomain();

        // Gets all possible op1 values
        BDDIterator op1itr = manager.iterator(convertBDD, op1dom);

        BDDFactory factory = bdd.getFactory();
        BDD d = factory.zero();
        while (op1itr.hasNext()) {
            BDD e = op1itr.nextBDD();
            long op1 = e.scanVar(op1dom).longValue();
            e.free();

            long r = manager.arith(ue.getType(), tempDom, op1, op1dom);
            d.orWith(op1dom.ithVar(op1).andWith(tempDom.ithVar(r)));
        }

        // finally, add the appropriate conditions to the result BDD
        convertBDD.andWith(d);

        if (manager.isTempDomain(op1dom)) {
            BDD tmp = abstractVars(convertBDD, op1dom);
            convertBDD.free();
            convertBDD = tmp;
            manager.freeIfTempDomain(op1dom);
        }

        if (resultDomain != null) {
            BDD tmp = abstractVars(convertBDD, resultDomain);
            convertBDD.free();
            convertBDD = tmp;
            convertBDD.replaceWith(factory.makePair(tempDom, resultDomain));
            manager.freeIfTempDomain(tempDom);
        } else {
            resultDomain = tempDom;
        }
    }

    public void visit(RemoplaUndefExpr obj) {
        assert mode == Mode.TRANS_EXPR;
        // represent undefined variables as unconstrained variables
        if (resultDomain != null) {
            BDD c = abstractVars(convertBDD, resultDomain);
            convertBDD.free();
            convertBDD = c;
        } else {
            resultDomain = manager.getTempVarDomain();
        }
    }

    public void visit(RemoplaUnsupportedVar obj) {
        // Ignore
    }

    public void visit(RemoplaVoidConstant obj) {
        assert mode == Mode.TRANS_EXPR;
        translateConstant(0);
    }

    private void translateVariable(RemoplaVar v) {
        if (resultDomain == null) {
            resultDomain = manager.getVarDomain(v);
        } else {
            BDDDomain varDom = manager.getVarDomain(v);
            BDD tmp = abstractVars(convertBDD, resultDomain);
            convertBDD.free();
            convertBDD = tmp;
            convertBDD.andWith(manager.bddEquals(resultDomain, varDom));
        }
    }

    private void translateConstant(long c) {

        if (resultDomain != null) {
            BDD tmp = abstractVars(convertBDD, resultDomain);
            convertBDD.free();
            convertBDD = tmp;
            long e = manager.encode(c, resultDomain);
            convertBDD.andWith(resultDomain.ithVar(e));
        } else {
            BDDDomain constDom = manager.getTempVarDomain();
            long e = manager.encode(c, constDom);
            convertBDD.andWith(constDom.ithVar(e));
            resultDomain = constDom;
        }
    }

    ///////////////////////////////////////////////////////////////////////////////
    // For collection domains from special elements.  These do not make sense as
    // expressions with a single domain

    List<BDDDomain> multiEleDoms = null;
    boolean createTemp = false;
    // if creating temp domains equal, then we need a bdd to say they are equal
    BDD getExprDomBDD;
    /** @param expr the expression to get the multi domains for
     * @param createTemp whether to create a temporary domain equal to the actual
     * @param getExprDomBDD add the constraints on the temp variable to this bdd
     * domain, or just to return the actual domain
     */
    private List<BDDDomain> getExprDomains(RemoplaExpr var, boolean createTemp, BDD getExprDomBDD) {
        mode = Mode.GETTING_ARR_STRUCT_DOMS;

        multiEleDoms = null;
        this.createTemp = createTemp;

        var.accept(this);
        List<BDDDomain> res = multiEleDoms;

        return res;
    }


    public void visit(RemoplaArrayVar ra) {
        logger.error("visit(RemoplaArrayVar ra) not implemented.");
//        assert mode == Mode.GETTING_ARR_STRUCT_DOMS;
//        List<BDDDomain> doms = manager.getSubVarDomains(manager.getVar(ra));

//        if (createTemp) {
//            multiEleDoms = new ArrayList<BDDDomain>(doms.size());
//            for (BDDDomain d : doms) {
//                BDDDomain temp = manager.getTempVarDomain();
//                multiEleDoms.add(temp);
//                getExprDomBDD.andWith(manager.bddEquals(temp, d));
//            }
//        } else {
//            multiEleDoms = doms;
//        }
    }

    public void visit(RemoplaStructVar rs) {
        logger.error("visit(RemoplaStructVar ra) not implemented.");
//        assert mode == Mode.GETTING_ARR_STRUCT_DOMS;
//        List<BDDDomain> doms = manager.getSubVarDomains(manager.getVar(rs));

//        if (createTemp) {
//            multiEleDoms = new ArrayList<BDDDomain>(doms.size());
//            for (BDDDomain d : doms) {
//                BDDDomain temp = manager.getTempVarDomain();
//                multiEleDoms.add(temp);
//                getExprDomBDD.andWith(manager.bddEquals(temp, d));
//            }
//        } else {
//            multiEleDoms = doms;
//        }
    }



    /**
     * Gets all possible integer values of this bdd at domain dom.
     *
     * @param dom the domain
     * @return the set of all possible integers
     */
    public Set<Long> valuesOf(BDDDomain dom) {
        return null;
    }


    /**
     * Abstract the variables specified by doms from bdd.
     * Does not consume bdd.
     *
     * @param bdd
     * @param doms
     * @return
     */
    public static BDD abstractVars(BDD bdd, BDDDomain... doms) {
        if (doms.length == 0) return bdd.id();

        BDDVarSet abs = doms[0].set();
        for (int i = 1; i < doms.length; i++) {
            abs.unionWith(doms[i].set());
        }
        BDD out = bdd.exist(abs);
        abs.free();

        return out;
    }




    /**
     * Abstract the variables specified by doms from bdd.  Does not consume bdd.
     *
     * @param bdd
     * @param doms
     * @return
     */
    public BDD abstractVars(BDD bdd, Collection<RemoplaVar> vars) {
        if (vars.isEmpty()) return bdd.id();

        BDDDomain[] doms = new BDDDomain[vars.size()];
        int i = 0;
        for (RemoplaVar v : vars) {
            doms[i++] = manager.getVarDomain(v);
        }

        return abstractVars(bdd, doms);
    }


	/**
	 * (G0,G2,L2) + (G2,L2,L0,G1,L1) -> (G0,L0,G1,L1)
     *
     * The above comment does not totally apply (it assumes handle return was
     * called first, so we had to move the pop handling code from handle return
     * to here)
	 */
    public Semiring extendPop(Semiring a, CancelMonitor monitor) {
        BDD us = bdd;
        BDD them = ((BDDSemiring)a).bdd;

        us = manager.abstractLocals(us);
        us.replaceWith(manager.getG1L1pairG2L2());

        them = abstractVars(them, manager.getRetVarDomain());

        BDD d = us.and(them);
        us.free();
        them.free();

        BDD c = d.exist(manager.getG2L2VarSet());
        d.free();

        return new BDDSemiring(manager, c);
    }


    /**
     * I guess this is really extendPop, but it's for the purpose of running a
     * pushdown system and applying a pop rule.  I think it won't behave quite
     * the same as extendPop so i'm implementing how i think it should work...
     *
     * @param a the semiring on the pop rule (R(G0, L0, G1), but in Expr form)
     * @param monitor does nothing
     * @return a new semiring after the pop has been applied (i.e. G0 is the
     * value of the global variables, and if the semiring had an L1 (i.e. a
     * valuation on the locals after pop, then these are the new local values
     * L0).  Everything else abstracted.
     */
    public Semiring applyPop(Semiring a, CancelMonitor monitor) {
        // basically just adds the guard on the return statement
        BDDSemiring next = extend(a, monitor);
        // get rid of the locals
        BDD nextBDD = manager.abstractL0(next.bdd);
        next.free();
        nextBDD.andWith(manager.buildG0L0equalsG1L1().id());
        BDD tmp = manager.abstractG1L1(nextBDD);
        nextBDD.free();
        return new BDDSemiring(manager, tmp);
    }

    /**
     * <pre>
     * (G0,L0,G1,L1) -> (G0,L0) -> (G0,L0,G1,L1)
     * </pre>
     *
     * where new G1,L1 are copies of G0,L0.
     */
    public Semiring extendPush(Semiring a, CancelMonitor monitor) {
        ExprSemiring A = (ExprSemiring)a;
        BDD extendedBDD = addCondition(bdd.id(), A.getCondition());

        if (!(A.getStmt() instanceof RemoplaInvokeStmt || A.getStmt() instanceof RemoplaInvokeAssignStmt)) {
            logger.error("BDDSemiring.extendPush called without a RemoplaInvokeStmt.");
        } else if (!extendedBDD.isZero()) {
            BDDFactory factory = bdd.getFactory();

            RemoplaMethodCall invoke;
            if (A.getStmt() instanceof RemoplaInvokeStmt) {
                invoke = ((RemoplaInvokeStmt)A.getStmt()).getInvokeExpr();
            } else {
                invoke = ((RemoplaInvokeAssignStmt)A.getStmt()).getRight();
            }

            BDD tmp = extendedBDD.exist(manager.getG1L1VarSet());
            extendedBDD.free();
            extendedBDD = setupCalledLocals(invoke, tmp);
            extendedBDD.andWith(manager.buildG0L0equalsG1L1().id());
        }

        return new BDDSemiring(manager, extendedBDD);
    }


    /**
     * From p a --R--> p' creates transition p -- a, R' --> p' where R' is a BDD
     * and R is an expression semiring (return statement).
     *
     * @param ret expression semiring on return statement (assumed to be a
     * return, will complain if it's a return assign)
     * @param manager the variable manager
     * @param monitor cancel monitor that is ignored for now
     * @return bdd (G0, L0, G0) where G0, L0 can be anything constrained only by
     * the guard on the semiring.
     */
    public static BDDSemiring createPrePop(Semiring ret,
                                           VarManager manager,
                                           CancelMonitor monitor) {
        ExprSemiring e = (ExprSemiring)ret;
        if (!(e.getStmt() instanceof RemoplaReturnStmt)) {
            logger.error("createPrePop only implemented for \"return;\" statements!");
            return null;
        }

        BDD bdd = manager.buildG0equalsG1().id();
        BDDSemiring ring = new BDDSemiring(manager, bdd);

        return ring;
    }


    /** returns the bdd which is a copy of callBDD with L0 containing the values of the local variables of
     * the method being called.
     *
     * Consumes callBDD.
     */
    private BDD setupCalledLocals(RemoplaMethodCall invoke, BDD callBDD) {
        List<RemoplaVar> mparams = manager.getMethodParameters(invoke.getCallString());
        Collection<RemoplaVar> mlocals = manager.getMethodLocals(invoke.getCallString());

        int nparams = mparams.size();

        // We are going to store the new local values to temporary variables
        // in the case that the local is a param.  (Otherwise the new value
        // is 0 and no temp domain is required.)  After storing the values
        // of the params, we abstract the locals and assign their new
        // values.
        List<BDDDomain> tempParams = new ArrayList(nparams);


        convertBDD = callBDD;
        mode = Mode.TRANS_EXPR;
        Iterator args = invoke.getParameters().iterator();
        for (RemoplaVar rv : mparams) {
            BDDDomain paramTemp = manager.getTempVarDomain();
            tempParams.add(paramTemp);

            Variable v = manager.getVar(rv);
            // direct param, copy direct val
            if (v.getSubVars() == null) {
                translateExpression((RemoplaExpr)args.next(), paramTemp);
            } else {
                logger.error("BDDSemiring: passing arrays or structs not currently supported.");
            }
        }

        BDD tmp = manager.abstractLocals(convertBDD);
        convertBDD.free();
        convertBDD = tmp;

        // set up local vals to 0 or temp val for param
        Iterator argi = tempParams.iterator();
        for (RemoplaVar rv : mparams) {
            Variable v = manager.getVar(rv);
            if (v.getSubVars() == null) {
                BDDDomain lvdom = manager.getVarDomain(v);
                BDDDomain tempdom = (BDDDomain)argi.next();
                convertBDD.replaceWith(manager.getFactory().makePair(tempdom, lvdom));
                manager.freeIfTempDomain(tempdom);
            } else {
                logger.error("Passing of arrays and structs not supported.");
            }
        }

        for (RemoplaVar rv : mlocals) {
            if (!mparams.contains(rv)) {
                Variable v = manager.getVar(rv);
                if (v.getSubVars() == null) {
                    BDDDomain varDom = manager.getVarDomain(v);
                    convertBDD.andWith(varDom.ithVar(VarManager.encode(0, varDom)));
                } else {
                    for (Variable sv : v.flattenSubVars()) {
                        BDDDomain varDom = manager.getVarDomain(sv);
                        convertBDD.andWith(varDom.ithVar(VarManager.encode(0, varDom)));
                    }
                }
            }
        }

        return convertBDD;
    }



    public BDDSemiring id() {
        return new BDDSemiring(manager, bdd.id());
    }

    public void free() {
        if (bdd != null) bdd.free();
    }

    public boolean equals(Object o) {
        if (!(o instanceof BDDSemiring))
            return false;

        return ((BDDSemiring)o).bdd.equals(bdd);

    }

    public String toRawString() {
        return bdd.toStringWithDomains();
    }

    public String toString() {
        return manager.stringBDD(bdd);
    }

    /**
     * Symbolic:
     * <pre>
     * (G0,G3,G1,L1) -> (G3,L0)
     * </pre>
     *
     * Explicit:
     * <pre>
     * (G0,L0,G1,L1) -> (L0)
     * </pre>
     *
     * NOT IMPLEMENTED: DPN FUNCTION.
     *
     */
    public Semiring extendDynamic(Semiring a, CancelMonitor monitor) {
        logger.error("extendDynamic called in remoplatopds.BDDSemiring -- not implemented.");
        return null;
    }

    /**
     * [Eager] Returns the set of all different global values from this BDD.
     *
     * @return the set of all global values.
     *
     * NOT IMPLEMENTED: DPN FUNCTION.
     *
     */
    public Set<Semiring> getGlobals() {
        logger.error("getGlobals called in remoplatopds.BDDSemiring -- not implemented.");
        return null;
    }


    /**
     * [Eager & Lazy] Lifts this BDD.
     *
     * NOT IMPLEMENTED: DPN FUNCTION.
     *
     */
    public Semiring lift(Semiring a) {
        logger.error("lift called in remoplatopds.BDDSemiring -- not implemented.");
        return null;
    }


    /**
     * [Eager] Conjuncts this BDD and the BDD of <code>a</code>, and
     * and abstracts all shared variables away.
     *
     * The BDD <code>a</code> remains unchanged.
     *
     * @return the restricted bdd.
     */
    public Semiring restrict(Semiring a) {
        logger.error("restrict called in remoplatopds.BDDSemiring -- not implemented.");
        return null;
    }



    public Semiring andWith(Semiring a) {
        bdd.andWith(((BDDSemiring) a).bdd);
        return this;
    }

    /**
     * [Lazy] Gets an equivalence class.
     *
     * This BDD: (G3,G4)
     *
     * @return an equivalence class (G3).
     *
     * NOT IMPLEMENTED: DPN FUNCTION.
     *
     */
    public Semiring getEqClass(int approach) {
        logger.error("getEqClass called in remoplatopds.BDDSemiring -- not implemented.");
        return null;
    }

    /**
     * [Lazy] Gets equivalence relations.
     *
     * This BDD: (G0,G3,L0,G1,L1) or (G3,L0,G1,L1)
     *
     * @return (G3,G4)
     *
     * NOT IMPLEMENTED: DPN FUNCTION
     *
     */
    public Semiring getEqRel(int approach) {
        logger.error("getEqRel called in remoplatopds.BDDSemiring -- not implemented.");
        return null;
    }

    /**
     * [Lazy] Abstracts L0,G1,L1 from this BDD.
     *
     * This BDD:
     * - (G0,G3,L0,G1,L1) or
     * - (G0,G3,G2,L2) in case of epsilon transition.
     *
     * @return (G0,G3)
     *
     * NOT IMPLEMENT: DPN FUNCTION
     *
     */
    public Semiring getGlobal() {
       logger.error("getGlobal called in remoplatopds.BDDSemiring -- not implemented.");
       return null;
    }

    /**
     * [Lazy] Updates globals of this BDD with <code>a</code>.
     *
     * This BDD: (G3,L0,G1,L1) or (G0,G3,L0,G1,L1)
     * a: (G0,G3)
     *
     * Conjoins this bdd with a; abstracts G3; and renames G0 to G3.
     *
     * @return (G3,L0,G1,L1)
     */
    public void updateGlobal(Semiring a) {
       logger.error("updateGlobal called in remoplatopds.BDDSemiring -- not implemented.");
       // return null
    }


    /**
     * [Lazy] Slices this equivalence relation with <code>eqclass</code>.
     *
     * @param eqclass an equivalence class.
     * @param approach one or two.
     *
     * NOT IMPLEMENTED: DPN FUNCTION
     *
     */
    public void sliceWith(Semiring eqclass, int approach) {
       logger.error("sliceWith called in remoplatopds.BDDSemiring -- not implemented.");
       // return null
    }

    public boolean isZero() {
        return bdd.isZero();
    }

    public Semiring orWith(Semiring a) {
        bdd.orWith(((BDDSemiring) a).bdd);
        return this;
    }


    private boolean isConstant(RemoplaExpr e) {
        return (e instanceof RemoplaIntConstant || e instanceof RemoplaIntConstVar);
    }

    private long constantValue(RemoplaExpr e, BDDDomain dom) {
        assert isConstant(e);
        long raw;
        if (e instanceof RemoplaIntConstant)
            raw = ((RemoplaIntConstant)e).getVal();
        else
            raw = ((RemoplaIntConstVar)e).getValue();
        return VarManager.encode(raw, dom);
    }



    private boolean canProcessAssignDangerously(RemoplaAssignStmt stmt) {
        boolean can = false;
        if (stmt.getAssignments().size() <= 1) {
            can = true;
        } else {
            StmtOptimisations opts = manager.getStmtOptimisations(stmt);
            if (opts != null) {
                can = opts.canDangerousAssign();
            }
        }
        return can;
    }
}

