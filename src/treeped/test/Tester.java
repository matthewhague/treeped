/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// Tester.java
// Author: Matthew Hague
//
// A set of test cases for BDD programming


package treeped.test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.tum.in.wpds.Pds;

import treeped.remoplatopds.StmtOptimisations;

import treeped.representation.Remopla;
import treeped.representation.RemoplaArrayVar;
import treeped.representation.RemoplaAssignStmt;
import treeped.representation.RemoplaBoolConstant;
import treeped.representation.RemoplaDoStmt;
import treeped.representation.RemoplaExpr;
import treeped.representation.RemoplaIfStmt;
import treeped.representation.RemoplaIntConstVar;
import treeped.representation.RemoplaIntConstant;
import treeped.representation.RemoplaIntVar;
import treeped.representation.RemoplaMethod;
import treeped.representation.RemoplaSkipStmt;
import treeped.representation.RemoplaStmt;
import treeped.representation.RemoplaUndefExpr;

import org.apache.log4j.Logger;

import static treeped.representation.RemoplaStaticFactory.*;


public class Tester {

    private static final int STRESS_REPEATS = 10;
    private static final RemoplaBoolConstant TRUE = new RemoplaBoolConstant(true);
    private static final RemoplaBoolConstant FALSE = new RemoplaBoolConstant(false);
    private static final RemoplaIntConstant ZERO = new RemoplaIntConstant(0);
    private static final RemoplaIntConstant ONE = new RemoplaIntConstant(1);
    private static final RemoplaIntConstant TWO = new RemoplaIntConstant(2);
    private static final RemoplaIntConstant THREE = new RemoplaIntConstant(3);
    private static final RemoplaIntConstant FOUR = new RemoplaIntConstant(4);
    private static final RemoplaIntConstant FIVE = new RemoplaIntConstant(5);
    private static final RemoplaIntConstant SIX = new RemoplaIntConstant(6);
    private static final RemoplaIntConstant SEVEN = new RemoplaIntConstant(7);
    private static final RemoplaIntConstant EIGHT = new RemoplaIntConstant(8);
    private static final RemoplaIntConstant NINE = new RemoplaIntConstant(9);
    private static final RemoplaIntConstant TEN = new RemoplaIntConstant(10);
    private static final RemoplaIntConstant ELEVEN = new RemoplaIntConstant(11);
    private static final RemoplaIntConstant TWELVE = new RemoplaIntConstant(12);
    private static final RemoplaIntConstant THIRTEEN = new RemoplaIntConstant(13);
    private static final RemoplaIntConstant FOURTEEN = new RemoplaIntConstant(14);
    private static final RemoplaIntConstant FIFTEEN = new RemoplaIntConstant(15);
    private static final RemoplaUndefExpr UNDEF = new RemoplaUndefExpr();

    private static final RemoplaIntVar L1 = new RemoplaIntVar("l1", true, true);
    private static final RemoplaIntVar L2 = new RemoplaIntVar("l2", true, true);
    private static final RemoplaIntVar L3 = new RemoplaIntVar("l3", true, true);
    private static final RemoplaIntVar L4 = new RemoplaIntVar("l4", true, true);
    private static final RemoplaIntVar L5 = new RemoplaIntVar("l5", true, true);

    private static final RemoplaIntVar G1 = new RemoplaIntVar("g1", false, true);
    private static final RemoplaIntVar G2 = new RemoplaIntVar("g2", false, true);

    private static final RemoplaIntConstVar C1 = new RemoplaIntConstVar("C1", 10, true);

    private static final String GENERATE_TEST_PREFIX = "generateTest";

    static Logger logger = Logger.getLogger(Tester.class);

    private Map<RemoplaStmt, StmtOptimisations> stmtOptimisations = new HashMap<RemoplaStmt, StmtOptimisations>();

    private class Test {
        public Remopla remopla;
        public Set<String> expected;
        public Set<String> unexpected;
        public String name;

        public Test(String name, Remopla remopla, Set<String> expected, Set<String> unexpected) {
            this.name = name;
            this.remopla = remopla;
            this.expected = expected;
            this.unexpected = unexpected;
        }

        public boolean runTest() {
            logger.info("Running test " + name);
            Pds pds = CheckPds.getPds(remopla, stmtOptimisations);
            if (internal) {
                logger.info("##############################################################");
                logger.info(pds);
                logger.info("##############################################################");
            }

            String init = remopla.getInitialMethod().getStmts().get(0).getLabel();
            Set<String> reached = CheckPds.doCheckPDS(pds, init, remopla, stmtOptimisations);
            Set<String> allReached = new HashSet<String>(reached);

            Set<String> allLabs = new HashSet<String>(expected);
            allLabs.addAll(unexpected);
            reached.retainAll(allLabs);

            boolean ok = reached.equals(expected);


            logger.info("Result of '" + name + "' = " + ok);

            if (!ok || verbose) {
                if (!ok)
                    logger.error("Failed test Remopla: ");
                logger.info(remopla.toString());
                logger.info("########################################");
                logger.info("Expected: " + expected);
                logger.info("Unexpected: " + unexpected);
                logger.info("Reached: " + reached);
                if (internal) {
                    logger.info("Reached internal: " + allReached);
                }
            }

            return ok;
        }

        private void addExpected(String... ls) {
            for (int i = 0; i < ls.length; i++) {
                expected.add(ls[i]);
            }
        }

        private void addUnexpected(String... ls) {
            for (int i = 0; i < ls.length; i++) {
                unexpected.add(ls[i]);
            }
        }

        private void generateUnexpected() {
            unexpected = remopla.getLabels();
            unexpected.removeAll(expected);
        }
    }


    private List<Test> tests = new ArrayList<Test>();
    private Set<String> runTests = null;
    private boolean verbose = false;
    private boolean internal = false;
    private boolean block = false;


    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    public void setInternal(boolean internal) {
        this.internal = internal;
    }

    public void setBlock(boolean block) {
        this.block = block;
    }




    public void doTest(String test) {
        assert test != null;

        if (runTests == null)
            runTests = new HashSet<String>();

        runTests.add(test);
    }

    public boolean runTests() {
        boolean ok = true;
        for (Test test : tests) {
            if (runTests == null) {
                ok &= test.runTest();
            } else if (!block && runTests.contains(test.name)) {
                ok &= test.runTest();
            } else if (block && !runTests.contains(test.name)) {
                ok &= test.runTest();
            }
        }
        return ok;
    }

    public boolean stressTests() {
        boolean ok = true;
        for (int i = 1; i < STRESS_REPEATS; i++) {
            ok &= runTests();
        }
        return ok;
    }


    public static void main(String[] args) {
        Tester tester = new Tester();
        tester.generateAllTests();

        boolean passed;
        boolean stress = false;

        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-stress")) {
                stress = true;
            } else if (args[i].equals("-v")) {
                tester.setVerbose(true);
            } else if (args[i].equals("-i")) {
                tester.setInternal(true);
            } else if (args[i].equals("-b")) {
                tester.setBlock(true);
            } else {
                tester.doTest(args[i]);
            }
        }

        if (stress) {
            passed = tester.stressTests();
        } else {
            passed = tester.runTests();
        }

        if (passed) {
            logger.info("ALL TESTS PASSED.");
        } else {
            logger.error("TESTS FAILED.");
        }
    }

    public void generateAllTests() {
        try {
            Method[] methods = Tester.class.getDeclaredMethods();
            for (int i = 0; i < methods.length; i++) {
                if (methods[i].getName().startsWith(GENERATE_TEST_PREFIX)) {
                    methods[i].invoke(this);
                }
            }
        } catch (InvocationTargetException e) {
            logger.error("InvocationTargetException: " + e.getTargetException());
        } catch (Exception e) {
            logger.error("Error generating tests: " + e);
        }
    }


    //////////////////////////////////////////////////////////////////////////////////////
    // Test generator helpers
    //

    private void addTest(Test test) {
        tests.add(test);
    }

    private Test makeBasicTest(String name) {
        Remopla remopla = new Remopla();
        RemoplaMethod main = method(rvoid(), "main");
        remopla.addMethod(main);
        remopla.setInitialMethod(main);

        Set<String> labels = new HashSet<String>();

        return new Test(name, remopla, labels, new HashSet());
    }

    private RemoplaStmt label(RemoplaStmt s, String l) {
        s.setLabel(l);
        return s;
    }

    private RemoplaStmt labL(String l) {
        return label(new RemoplaSkipStmt(), l);
    }

    private RemoplaStmt check(RemoplaExpr test, String pass, String fail) {
        return ifthenelse(test, labL(pass), labL(fail));
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Test generators
    //

    private void generateTest1() {
        Test test = makeBasicTest("skip");

        RemoplaMethod main = test.remopla.getInitialMethod();

        main.appendStatement(label(new RemoplaSkipStmt(), "L1"));
        main.appendStatement(label(new RemoplaSkipStmt(), "L2"));

        test.addExpected("L1", "L2");

        test.generateUnexpected();

        addTest(test);
    }

    private void generateTest2() {
        Test test = makeBasicTest("if");

        RemoplaMethod main = test.remopla.getInitialMethod();

        main.appendStatement(ifthenelse(TRUE, labL("L1"), null));
        main.appendStatement(ifthenelse(FALSE, labL("L2"), null));

        test.addExpected("L1");
        test.generateUnexpected();

        addTest(test);
    }

    private void generateTest3() {
        Test test = makeBasicTest("ifThen");

        RemoplaMethod main = test.remopla.getInitialMethod();

        main.appendStatement(check(TRUE, "L1", "L2"));
        main.appendStatement(check(FALSE, "L3", "L4"));
        main.appendStatement(check(UNDEF, "L5", "L6"));

        test.addExpected("L1", "L4", "L5", "L6");
        test.generateUnexpected();

        addTest(test);
    }

    private void generateTest4() {
        Test test = makeBasicTest("ifThenNumbers");

        RemoplaMethod main = test.remopla.getInitialMethod();

        main.appendStatement(check(gte(ONE, ONE), "L1", "L2"));
        main.appendStatement(check(gt(ONE, TWO), "L3", "L4"));
        main.appendStatement(check(gt(FIFTEEN, ZERO), "L5", "L6"));
        main.appendStatement(check(gt(ZERO, FIFTEEN), "L7", "L8"));

        test.addExpected("L1", "L4", "L5", "L8");
        test.generateUnexpected();

        addTest(test);
    }


    private void generateTest5() {
        Test test = makeBasicTest("and");

        RemoplaMethod main = test.remopla.getInitialMethod();

        main.appendStatement(check(and(TRUE, TRUE), "Land11", "Land12"));
        main.appendStatement(check(and(TRUE, FALSE), "Land21", "Land22"));
        main.appendStatement(check(and(FALSE, TRUE), "Land31", "Land32"));
        main.appendStatement(check(and(FALSE, FALSE), "Land41", "Land42"));

        test.addExpected("Land11", "Land22", "Land32", "Land42");
        test.generateUnexpected();

        addTest(test);
    }

    private void generateTest6() {
        Test test = makeBasicTest("or");

        RemoplaMethod main = test.remopla.getInitialMethod();

        main.appendStatement(check(or(TRUE, TRUE), "Lor11", "Lor12"));
        main.appendStatement(check(or(TRUE, FALSE), "Lor21", "Lor22"));
        main.appendStatement(check(or(FALSE, TRUE), "Lor31", "Lor32"));
        main.appendStatement(check(or(FALSE, FALSE), "Lor41", "Lor42"));

        test.addExpected("Lor11", "Lor21", "Lor31", "Lor42");
        test.generateUnexpected();

        addTest(test);
    }

    private void generateTest7() {
        Test test = makeBasicTest("xor");

        RemoplaMethod main = test.remopla.getInitialMethod();

        main.appendStatement(check(xor(TRUE, TRUE), "Lxor11", "Lxor12"));
        main.appendStatement(check(xor(TRUE, FALSE), "Lxor21", "Lxor22"));
        main.appendStatement(check(xor(FALSE, TRUE), "Lxor31", "Lxor32"));
        main.appendStatement(check(xor(FALSE, FALSE), "Lxor41", "Lxor42"));

        test.addExpected("Lxor12", "Lxor21", "Lxor31", "Lxor42");
        test.generateUnexpected();

        addTest(test);
    }

    private void generateTest8() {
        Test test = makeBasicTest("morelogic");

        RemoplaMethod main = test.remopla.getInitialMethod();

        main.appendStatement(check(not(xor(TRUE, TRUE)), "L11", "L12"));
        main.appendStatement(check(and(xor(TRUE, FALSE), or(FALSE, TRUE)), "L21", "L22"));
        main.appendStatement(check(or(not(xor(FALSE, TRUE)), not(and(TRUE, TRUE))), "L31", "L32"));
        main.appendStatement(check(xor(xor(FALSE, FALSE),xor(FALSE,FALSE)), "L41", "L42"));

        test.addExpected("L11", "L21", "L32", "L42");
        test.generateUnexpected();

        addTest(test);
    }


    private void generateTest9() {
        Test test = makeBasicTest("add");

        RemoplaMethod main = test.remopla.getInitialMethod();

        main.appendStatement(ifthenelse(eq(add(ONE,TWO),THREE),labL("L11"), labL("L12")));
        main.appendStatement(ifthenelse(neq(add(ONE,TWO),THREE),labL("L21"), labL("L22")));
        main.appendStatement(ifthenelse(eq(add(ZERO,FIFTEEN),FIFTEEN),labL("L31"), labL("L32")));
        main.appendStatement(ifthenelse(eq(add(ZERO,FIFTEEN),TEN),labL("L41"), labL("L42")));
        main.appendStatement(ifthenelse(eq(add(THREE,FIFTEEN),FIFTEEN),labL("L51"), labL("L52")));

        test.addExpected("L11", "L22", "L31", "L42", "L51");
        test.generateUnexpected();

        addTest(test);
    }

    private void generateTest10() {
        Test test = makeBasicTest("sub");

        RemoplaMethod main = test.remopla.getInitialMethod();

        main.appendStatement(ifthenelse(eq(sub(TWO,ONE),ONE),labL("L11"), labL("L12")));
        main.appendStatement(ifthenelse(neq(sub(TEN,NINE),ONE),labL("L21"), labL("L22")));
        main.appendStatement(ifthenelse(eq(sub(FIFTEEN,FIFTEEN),ZERO),labL("L31"), labL("L32")));
        main.appendStatement(ifthenelse(eq(sub(FIFTEEN,ZERO),TEN),labL("L41"), labL("L42")));
        main.appendStatement(ifthenelse(eq(sub(THREE,FIFTEEN),ZERO),labL("L51"), labL("L52")));

        test.addExpected("L11", "L22", "L31", "L42", "L51");
        test.generateUnexpected();

        addTest(test);
    }

    private void generateTest11() {
        Test test = makeBasicTest("morearith");

        RemoplaMethod main = test.remopla.getInitialMethod();

        main.appendStatement(check(eq(mul(TWO, THREE), div(TWELVE, TWO)), "L11", "L12"));
//        main.appendStatement(check(eq(shl(TWO, THREE), shr(TWELVE, FOUR)), "L21", "L22"));
        main.appendStatement(check(lte(mul(sub(TEN, EIGHT), mul(TWO, TWO)), EIGHT), "L31", "L32"));
        main.appendStatement(check(lt(mul(sub(TEN, EIGHT), mul(TWO, TWO)), EIGHT), "L41", "L42"));
        main.appendStatement(check(gt(shl(TWO, TWO), shr(TWELVE, ONE)), "L51", "L52"));

        test.addExpected("L11", "L31", "L42", "L51");
        test.generateUnexpected();

        addTest(test);
    }


    private void generateTest12() {
        try {
            Test test = makeBasicTest("basicLVar");

            RemoplaMethod main = test.remopla.getInitialMethod();
            main.addLocalVar(L1);
            main.addLocalVar(L2);
            main.appendStatement(assign(L1, ONE));
            main.appendStatement(check(eq(L1, ONE), "L11", "L12"));
            main.appendStatement(assign(L2, L1));
            main.appendStatement(assign(L1, TWO));
            main.appendStatement(check(eq(L1, ONE), "L21", "L22"));
            main.appendStatement(check(eq(L1, TWO), "L31", "L32"));
            main.appendStatement(check(eq(L2, ONE), "L41", "L42"));

            test.addExpected("L11", "L22", "L31", "L41");
            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 12: " + e);
        }
    }

    private void generateTest13() {
        try {
            Test test = makeBasicTest("basicGVar");

            RemoplaMethod main = test.remopla.getInitialMethod();
            test.remopla.addGlobalVar(G1);
            test.remopla.addGlobalVar(G2);
            main.appendStatement(assign(G1, ONE));
            main.appendStatement(check(eq(G1, ONE), "L11", "L12"));
            main.appendStatement(assign(G2, G1));
            main.appendStatement(assign(G1, TWO));
            main.appendStatement(check(eq(G1, ONE), "L21", "L22"));
            main.appendStatement(check(eq(G1, TWO), "L31", "L32"));
            main.appendStatement(check(eq(G2, ONE), "L41", "L42"));

            test.addExpected("L11", "L22", "L31", "L41");
            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 13: " + e);
        }
    }

    private void generateTest14() {
        try {
            Test test = makeBasicTest("basicLGVar");

            RemoplaMethod main = test.remopla.getInitialMethod();
            test.remopla.addGlobalVar(G1);
            main.addLocalVar(L1);
            main.appendStatement(assign(G1, ONE));
            main.appendStatement(check(eq(G1, ONE), "L11", "L12"));
            main.appendStatement(assign(L1, G1));
            main.appendStatement(assign(G1, TWO));
            main.appendStatement(check(eq(G1, ONE), "L21", "L22"));
            main.appendStatement(check(eq(G1, TWO), "L31", "L32"));
            main.appendStatement(check(eq(L1, ONE), "L41", "L42"));

            test.addExpected("L11", "L22", "L31", "L41");
            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 14: " + e);
        }
    }

    private void generateTest15() {
        try {
            Test test = makeBasicTest("basicVarArith");

            RemoplaMethod main = test.remopla.getInitialMethod();
            test.remopla.addGlobalVar(G1);
            main.addLocalVar(L1);
            main.appendStatement(assign(G1, ONE));
            main.appendStatement(assign(L1, mul(TWO, G1)));
            main.appendStatement(check(eq(L1, TWO), "L11", "L12"));
            main.appendStatement(assign(G1, add(G1, L1)));
            main.appendStatement(check(eq(G1, THREE), "L21", "22"));

            test.addExpected("L11", "L21");

            main.appendStatement(assign(L1, UNDEF));
            main.appendStatement(assign(L1, add(G1, L1)));
            main.appendStatement(check(gte(L1, THREE), "L31", "L32"));

            test.addExpected("L31");

            main.appendStatement(assign(L1, UNDEF));
            main.appendStatement(skip(lte(L1, TEN)));
            main.appendStatement(assign(L1, add(G1, L1)));
            main.appendStatement(check(gte(L1, THREE), "L41", "L42"));

            test.addExpected("L41");

            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 15: " + e);
        }
    }


    private void generateTest16() {
        try {
            Test test = makeBasicTest("arrAssign");

            RemoplaMethod main = test.remopla.getInitialMethod();

            RemoplaArrayVar arr = oneDimIntArr("a", 0, 1, true, true);
            main.addLocalVar(arr);

            main.appendStatement(assign(aRef(arr, ZERO), ONE));
            main.appendStatement(assign(aRef(arr, ONE), ZERO));
            main.appendStatement(check(eq(aRef(arr, ZERO), ONE), "L11", "L12"));
            main.appendStatement(check(eq(aRef(arr, ONE), ZERO), "L21", "L22"));

            test.addExpected("L11", "L21");

            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 16: " + e);
        }
    }

    private void generateTest17() {
        try {
            Test test = makeBasicTest("arrExpAssign");

            RemoplaMethod main = test.remopla.getInitialMethod();

            RemoplaArrayVar arr = oneDimIntArr("a", 0, 2, true, true);
            main.addLocalVar(arr);
            main.addLocalVar(L1);

            main.appendStatement(assign(aRef(arr, ZERO), TWO));
            main.appendStatement(assign(aRef(arr, ONE), THREE));
            main.appendStatement(assign(L1, add(aRef(arr,ONE), aRef(arr, ZERO))));
            main.appendStatement(check(eq(L1, FIVE), "L11", "L12"));

            test.addExpected("L11");

            main.appendStatement(assign(aRef(arr, ZERO), mul(add(L1, aRef(arr, ZERO)), sub(aRef(arr, ONE), ONE))));
            main.appendStatement(assign(aRef(arr, ONE), shr(aRef(arr, ZERO), ONE)));
            main.appendStatement(check(eq(aRef(arr, ZERO), FOURTEEN), "L21", "L22"));
            main.appendStatement(check(eq(aRef(arr, ONE), SEVEN), "L31", "L32"));

            test.addExpected("L21", "L31");

            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 17: " + e);
        }
    }

    private void generateTest18() {
        try {
            Test test = makeBasicTest("arrVarIndex");

            RemoplaMethod main = test.remopla.getInitialMethod();

            RemoplaArrayVar arr = oneDimIntArr("a", 0, 3, true, true);
            main.addLocalVar(arr);
            main.addLocalVar(L1);

            main.appendStatement(skip(eq(L1, ZERO)));
            main.appendStatement(assign(aRef(arr, L1), TWELVE));
            main.appendStatement(check(eq(aRef(arr, ZERO), TWELVE), "L11", "L12"));

            test.addExpected("L11");

            main.appendStatement(assign(L1, UNDEF));
            main.appendStatement(skip(or(eq(L1, ZERO), eq(L1, ONE))));
            main.appendStatement(assign(aRef(arr, ZERO), ONE));
            main.appendStatement(assign(aRef(arr, ONE), ONE));
            main.appendStatement(assign(aRef(arr, L1), ZERO));

            main.appendStatement(check(or(and(eq(aRef(arr, ZERO), ONE), eq(aRef(arr, ONE), ZERO)),
                                               and(eq(aRef(arr, ZERO), ZERO), eq(aRef(arr, ONE), ONE))),
                                       "L21",
                                       "L22"));
            main.appendStatement(check(eq(aRef(arr, L1), ZERO), "L31", "L32"));
            test.addExpected("L21", "L31");

            main.appendStatement(assign(L1, UNDEF));
            main.appendStatement(skip(or(eq(L1, TWO), eq(L1, ONE))));
            main.appendStatement(assign(aRef(arr, ZERO), ONE));
            main.appendStatement(assign(aRef(arr, ONE), ONE));
            main.appendStatement(assign(aRef(arr, TWO), ONE));
            main.appendStatement(assign(aRef(arr, THREE), ONE));

            main.appendStatement(assign(aRef(arr, L1), ZERO));

            main.appendStatement(check(or(eq(aRef(arr, ZERO), ZERO), eq(aRef(arr,THREE), ZERO)), "L41", "L42"));

            test.addExpected("L42");


            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 18: " + e);
        }
    }

    private void generateTest19() {
        try {
            Test test = makeBasicTest("basicLParAssign");

            RemoplaMethod main = test.remopla.getInitialMethod();
            main.addLocalVar(L1);
            main.addLocalVar(L2);

            RemoplaAssignStmt parallel = assign(L1, ONE);
            parallel.addParallelAssignment(L2, TWO);

            main.appendStatement(parallel);
            main.appendStatement(check(and(eq(L1, ONE), eq(L2, TWO)), "L11", "L12"));

            test.addExpected("L11");

            parallel = assign(L1, L2);
            parallel.addParallelAssignment(L2, L1);

            main.appendStatement(parallel);
            main.appendStatement(check(and(eq(L1, TWO), eq(L2, ONE)), "L21", "L22"));

            test.addExpected("L21");


            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 19: " + e);
        }
    }

    private void generateTest20() {
        try {
            Test test = makeBasicTest("basicLGParAssign");

            RemoplaMethod main = test.remopla.getInitialMethod();
            main.addLocalVar(L1);
            test.remopla.addGlobalVar(G2);

            RemoplaAssignStmt parallel = assign(L1, ONE);
            parallel.addParallelAssignment(G2, TWO);

            main.appendStatement(parallel);
            main.appendStatement(check(and(eq(L1, ONE), eq(G2, TWO)), "L11", "L12"));

            test.addExpected("L11");

            parallel = assign(L1, G2);
            parallel.addParallelAssignment(G2, L1);

            main.appendStatement(parallel);
            main.appendStatement(check(and(eq(L1, TWO), eq(G2, ONE)), "L21", "L22"));

            test.addExpected("L21");


            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 20: " + e);
        }
    }

    private void generateTest21() {
        try {
            Test test = makeBasicTest("basicArrParAssign");

            RemoplaMethod main = test.remopla.getInitialMethod();
            RemoplaArrayVar arr = oneDimIntArr("a", 0, 1, true, true);
            main.addLocalVar(arr);


            RemoplaAssignStmt parallel = assign(aRef(arr, ZERO), ONE);
            parallel.addParallelAssignment(aRef(arr, ONE) , TWO);

            main.appendStatement(parallel);
            main.appendStatement(check(and(eq(aRef(arr, ZERO), ONE), eq(aRef(arr, ONE), TWO)), "L11", "L12"));

            test.addExpected("L11");

            parallel = assign(aRef(arr, ZERO), aRef(arr, ONE));
            parallel.addParallelAssignment(aRef(arr, ONE), aRef(arr, ZERO));

            main.appendStatement(parallel);
            main.appendStatement(check(and(eq(aRef(arr, ZERO), TWO), eq(aRef(arr, ONE), ONE)), "L21", "L22"));

            test.addExpected("L21");


            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 21: " + e);
        }
    }

    private void generateTest22() {
        try {
            Test test = makeBasicTest("basicUndefArrParAssign");

            RemoplaMethod main = test.remopla.getInitialMethod();
            RemoplaArrayVar arr = oneDimIntArr("a", 0, 5, true, true);
            main.addLocalVar(arr);
            main.addLocalVar(L1);
            main.addLocalVar(L2);

            main.appendStatement(assign(L1, UNDEF));
            main.appendStatement(assign(L2, UNDEF));
            main.appendStatement(skip(neq(L1, L2)));
            main.appendStatement(skip(and(gte(L1, ZERO), lte(L1, FIVE))));
            main.appendStatement(skip(and(gte(L2, ZERO), lte(L2, FIVE))));

            main.appendStatement(labL("check"));
            test.addExpected("check");


            RemoplaAssignStmt parallel = assign(aRef(arr, L1), ONE);
            parallel.addParallelAssignment(aRef(arr, L2) , TWO);

            main.appendStatement(parallel);
            main.appendStatement(labL("check2"));
            main.appendStatement(check(and(eq(aRef(arr, L1), ONE), eq(aRef(arr, L2), TWO)), "L11", "L12"));

            test.addExpected("check2", "L11");

            parallel = assign(aRef(arr, L1), aRef(arr, L2));
            parallel.addParallelAssignment(aRef(arr, L2), aRef(arr, L1));

            main.appendStatement(parallel);
            main.appendStatement(check(and(eq(aRef(arr, L1), TWO), eq(aRef(arr, L2), ONE)), "L21", "L22"));

            test.addExpected("L21");


            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 22: " + e);
        }
    }

    private void generateTest23() {
        try {
            Test test = makeBasicTest("indirArr");

            RemoplaMethod main = test.remopla.getInitialMethod();

            RemoplaArrayVar arr = oneDimIntArr("a", 0, 3, true, true);
            main.addLocalVar(arr);

            RemoplaAssignStmt assign = assign(aRef(arr, ZERO), THREE);
            assign.addParallelAssignment(aRef(arr, ONE), TWO);
            assign.addParallelAssignment(aRef(arr, TWO), ZERO);
            assign.addParallelAssignment(aRef(arr, THREE), ONE);
            main.appendStatement(assign);
            main.appendStatement(assign(aRef(arr, aRef(arr, aRef(arr, aRef(arr, aRef(arr, ONE))))), SEVEN));
            main.appendStatement(check(eq(aRef(arr, ONE), SEVEN), "L11", "L12"));

            test.addExpected("L11");

            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 23: " + e);
        }
    }


    private void generateTest24() {
        try {
            Test test = makeBasicTest("goto");

            RemoplaMethod main = test.remopla.getInitialMethod();
            main.addLocalVar(L1);

            main.appendStatement(assign(L1, ONE));
            main.appendStatement(rgoto("target"));
            main.appendStatement(assign(L1, ZERO));
            main.appendStatement(label(check(eq(L1, ONE), "L11", "L12"), "target"));

            test.addExpected("target", "L11");

            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 24: " + e);
        }
    }

    private void generateTest25() {
        try {
            Test test = makeBasicTest("do");

            RemoplaMethod main = test.remopla.getInitialMethod();
            main.addLocalVar(L1);

            main.appendStatement(assign(L1, ONE));

            RemoplaDoStmt doLoop = rdo();
            doLoop.addClause(eq(L1, ONE), assign(L1, add(L1, ONE)));
            doLoop.addClause(eq(L1, TWO), assign(L1, add(L1, TWO)));
            doLoop.addClause(eq(L1, FOUR), assign(L1, add(L1, ONE)));
            doLoop.addClause(eq(L1, FIVE), rbreak());
            doLoop.addElse(labL("error"));
            main.appendStatement(doLoop);
            main.appendStatement(check(eq(L1, FIVE), "L11", "L12"));

            test.addExpected("L11");

            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 25: " + e);
        }
    }

    private void generateTest26() {
        try {
            Test test = makeBasicTest("basicForAll");

            RemoplaMethod main = test.remopla.getInitialMethod();
            RemoplaArrayVar arr = oneDimIntArr("arr", 0, 5, true, true);
            main.addLocalVar(arr);
            main.addLocalVar(L1);

            main.appendStatement(assignForAll(L1, 0, 5, aRef(arr, L1), L1));
            main.appendStatement(check(eq(aRef(arr, ZERO), ZERO), "L11", "L12"));
            main.appendStatement(check(eq(aRef(arr, ONE), ONE), "L21", "L22"));
            main.appendStatement(check(eq(aRef(arr, TWO), TWO), "L31", "L32"));
            main.appendStatement(check(eq(aRef(arr, THREE), THREE), "L41", "L42"));
            main.appendStatement(check(eq(aRef(arr, FOUR), FOUR), "L51", "L52"));
            main.appendStatement(check(eq(aRef(arr, FIVE), FIVE), "L61", "L62"));

            test.addExpected("L11", "L21", "L31", "L41", "L51", "L61");

            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 26: " + e);
        }
    }

    private void generateTest27() {
        try {
            Test test = makeBasicTest("twistForAll");

            RemoplaMethod main = test.remopla.getInitialMethod();
            RemoplaArrayVar arr = oneDimIntArr("arr", 0, 5, true, true);
            main.addLocalVar(arr);
            main.addLocalVar(L1);

            main.appendStatement(assignForAll(L1, 0, 5, aRef(arr, L1), L1));
            main.appendStatement(assignForAll(L1, 1, 5, aRef(arr, L1), aRef(arr, sub(L1, ONE))));

            main.appendStatement(check(eq(aRef(arr, ZERO), ZERO), "L11", "L12"));
            main.appendStatement(check(eq(aRef(arr, ONE), ZERO), "L21", "L22"));
            main.appendStatement(check(eq(aRef(arr, TWO), ONE), "L31", "L32"));
            main.appendStatement(check(eq(aRef(arr, THREE), TWO), "L41", "L42"));
            main.appendStatement(check(eq(aRef(arr, FOUR), THREE), "L51", "L52"));
            main.appendStatement(check(eq(aRef(arr, FIVE), FOUR), "L61", "L62"));

            test.addExpected("L11", "L21", "L31", "L41", "L51", "L61");

            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 27: " + e);
        }
    }


     private void generateTest28() {
        try {
            Test test = makeBasicTest("intConstVar");

            RemoplaMethod main = test.remopla.getInitialMethod();
            test.remopla.addConstant(C1);
            main.addLocalVar(L1);
            main.addLocalVar(L2);

            main.appendStatement(assign(L1, C1));
            main.appendStatement(assign(L2, add(C1, ONE)));
            main.appendStatement(check(eq(L1, TEN), "L11", "L12"));
            main.appendStatement(check(eq(L2, ELEVEN), "L21", "L22"));

            test.addExpected("L11", "L21");

            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 28: " + e);
        }
    }

     private void generateTest29() {
        try {
            Test test = makeBasicTest("basicCallReturn");

            test.remopla.addGlobalVar(G1);

            RemoplaMethod m = method(rvoid(), "m");
            test.remopla.addMethod(m);
            m.appendStatement(labL("called"));

            test.addExpected("called");


            RemoplaMethod main = test.remopla.getInitialMethod();

            main.appendStatement(invoke("m"));
            main.appendStatement(labL("returned1"));
            main.appendStatement(invoke("m"));
            main.appendStatement(labL("returned2"));

            test.addExpected("returned1", "returned2");

            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 29: " + e);
        }
    }



     private void generateTest30() {
        try {
            Test test = makeBasicTest("callArgsGlobs");

            test.remopla.addGlobalVar(G1);

            RemoplaMethod m = method(rvoid(), "m", L1);
            test.remopla.addMethod(m);
            m.appendStatement(check(eq(L1, THREE), "L11", "L12"));
            m.appendStatement(assign(G1, SEVEN));
            m.appendStatement(labL("assigned"));

            test.addExpected("assigned");


            RemoplaMethod main = test.remopla.getInitialMethod();
            main.addLocalVar(L2);

            main.appendStatement(assign(L2, ONE));
            main.appendStatement(invoke("m", THREE));
            main.appendStatement(labL("returned"));
            main.appendStatement(check(eq(L2, ONE), "L21", "L22"));
            main.appendStatement(check(eq(G1, SEVEN), "L31", "L32"));

            test.addExpected("L11", "L21", "returned", "L31");

            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 30: " + e);
        }
    }


    private void generateTest31() {
        try {
            Test test = makeBasicTest("callArgsReturn");

            RemoplaMethod m = method(rint(), "m", L1, L2);
            test.remopla.addMethod(m);
            m.appendStatement(ifthenelse(eq(L2, ONE), labL("firstCall"), skip()));
            m.appendStatement(ifthenelse(eq(L2, FOUR), labL("secondCall"), skip()));
            m.appendStatement(assign(L2, add(L1, L2)));
            m.appendStatement(ifthenelse(eq(L2, FOUR), labL("firstCallAssigned"), skip()));
            m.appendStatement(ifthenelse(eq(L2, FIVE), labL("secondCallAssigned"), skip()));
            m.appendStatement(rreturn(L2));

            test.addExpected("firstCall", "secondCall", "firstCallAssigned", "secondCallAssigned");


            RemoplaMethod main = test.remopla.getInitialMethod();
            main.addLocalVar(L3);
            main.addLocalVar(L4);

            main.appendStatement(assign(L3, ONE));
            main.appendStatement(assign(L4, TEN));
            main.appendStatement(label(invAssign(L4, "m", THREE, L3), "call1"));
            main.appendStatement(check(eq(L3, ONE), "L21", "L22"));
            main.appendStatement(check(eq(L4, FOUR), "L31", "L32"));
            main.appendStatement(label(invAssign(L4, "m", L3, FOUR), "call2"));
            main.appendStatement(check(eq(L3, ONE), "L41", "L42"));
            main.appendStatement(check(eq(L4, FIVE), "L51", "L52"));

            test.addExpected("L21", "call1", "L31", "L41", "L51", "call2");

            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 31: " + e);
        }
    }

    private void generateTest32() {
        try {
            Test test = makeBasicTest("twoCall");

            test.remopla.addGlobalVar(G1);

            RemoplaMethod m1 = method(rint(), "m1");
            test.remopla.addMethod(m1);

            m1.appendStatement(rreturn(FIVE));

            RemoplaMethod m2 = method(rint(), "m2");
            test.remopla.addMethod(m2);

            m2.addLocalVar(L1);
            m2.appendStatement(invAssign(L1, "m1"));
            m2.appendStatement(rreturn(add(L1, L1)));

            RemoplaMethod main = test.remopla.getInitialMethod();
            main.addLocalVar(L2);
            main.appendStatement(invAssign(L2, "m2"));
            main.appendStatement(check(eq(L2, TEN), "L11", "L12"));

            test.addExpected("L11");

            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 32: " + e);
        }
    }

    private void generateTest33() {
        try {
            Test test = makeBasicTest("twoCallRecArg");

            test.remopla.addGlobalVar(G1);

            RemoplaMethod m1 = method(rint(), "m1", L1);
            m1.addLocalVar(L2);
            test.remopla.addMethod(m1);

            m1.appendStatement(ifthenelse(eq(L1, ZERO), assign(L2, ONE), invAssign(L2, "m2", sub(L1, ONE))));
            m1.appendStatement(rreturn(L2));

            RemoplaMethod m2 = method(rint(), "m2", L3);
            m2.addLocalVar(L4);
            test.remopla.addMethod(m2);

            m2.appendStatement(invAssign(L4, "m1", L3));
            m2.appendStatement(rreturn(add(L4, L4)));

            RemoplaMethod main = test.remopla.getInitialMethod();
            main.addLocalVar(L5);
            main.appendStatement(invAssign(L5, "m1", THREE));
            main.appendStatement(check(eq(L5, FOUR), "L11", "L12"));

            test.addExpected("L11");

            test.generateUnexpected();

//            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 33: " + e);
        }
    }

    private void generateTest34() {
        try {
            Test test = makeBasicTest("fac");

            test.remopla.addGlobalVar(G1);

            RemoplaMethod m1 = method(rint(), "fac", L1);
            m1.addLocalVar(L2);
            test.remopla.addMethod(m1);

            m1.appendStatement(ifthenelse(eq(L1, ZERO), rreturn(ONE), invAssign(L2, "fac", sub(L1, ONE))));
            m1.appendStatement(rreturn(mul(L1, L2)));

            RemoplaMethod main = test.remopla.getInitialMethod();
            main.addLocalVar(L4);
            main.appendStatement(invAssign(L4, "fac", THREE));
            main.appendStatement(check(eq(L4, SIX), "L11", "L12"));

            test.addExpected("L11");

            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 34: " + e);
        }
    }

    private void generateTest35() {
        try {
            Test test = makeBasicTest("ambiAssign");

            RemoplaMethod main = test.remopla.getInitialMethod();
            main.addLocalVar(L1);
            main.addLocalVar(L2);

            main.appendStatement(assign(L1, UNDEF));
            main.appendStatement(skip(or(eq(L1, ONE), eq(L1, TWO))));

            main.appendStatement(assign(L2, L1));

            main.appendStatement(ifthenelse(eq(L1, ONE), check(eq(L2, ONE), "L11", "L12"), skip()));
            main.appendStatement(ifthenelse(eq(L1, TWO), check(eq(L2, TWO), "L21", "L22"), skip()));

            test.addExpected("L11", "L21");

            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 35: " + e);
        }
    }

    private void generateTest36() {
        try {
            Test test = makeBasicTest("returnNoAssign");

            RemoplaMethod m1 = method(rint(), "fac", L1);
            m1.addLocalVar(L2);
            test.remopla.addMethod(m1);

            m1.appendStatement(ifthenelse(eq(L1, ZERO), rreturn(ZERO), invoke("fac", sub(L1, ONE))));
            m1.appendStatement(rreturn(L1));


            RemoplaMethod main = test.remopla.getInitialMethod();
            main.addLocalVar(L4);
            main.appendStatement(invAssign(L4, "fac", TWO));
            main.appendStatement(check(eq(L4, TWO), "L11", "L12"));

            test.addExpected("L11");

            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 36: " + e);
        }
    }

    private void generateTest37() {
        try {
            Test test = makeBasicTest("noReturnNoAssign");

            RemoplaMethod m1 = method(rvoid(), "recurse", L1);
            test.remopla.addMethod(m1);

            m1.appendStatement(ifthenelse(gt(L1, ZERO), invoke("recurse", sub(L1, ONE)), skip()));
//            m1.appendStatement(skip());
//            m1.appendStatement(rreturn(null));

            RemoplaMethod main = test.remopla.getInitialMethod();
            main.appendStatement(invoke("recurse", ONE));
            main.appendStatement(labL("L1"));

            test.addExpected("L1");

            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 37: " + e);
        }
    }

    private void generateTest38() {
        try {
            Test test = makeBasicTest("noReturnNoAssignSkip");

            RemoplaMethod m1 = method(rvoid(), "recurse", L1);
            test.remopla.addMethod(m1);

            m1.appendStatement(ifthenelse(eq(L1, ZERO), rreturn(null), invoke("recurse", sub(L1, ONE))));
            m1.appendStatement(skip());


            RemoplaMethod main = test.remopla.getInitialMethod();
            main.appendStatement(invoke("recurse", ONE));
            main.appendStatement(labL("L1"));

            test.addExpected("L1");

            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 38: " + e);
        }
    }

    private void generateTest39() {
        try {
            Test test = makeBasicTest("dangerParAssign");

            RemoplaMethod main = test.remopla.getInitialMethod();
            main.addLocalVar(L1);
            main.addLocalVar(L2);

            RemoplaAssignStmt parallel = assign(L1, ONE);
            parallel.addParallelAssignment(L2, TWO);

            main.appendStatement(parallel);
            main.appendStatement(check(and(eq(L1, ONE), eq(L2, TWO)), "L11", "L12"));

            test.addExpected("L11");

            parallel = assign(L1, L2);
            parallel.addParallelAssignment(L2, L1);
            StmtOptimisations opts = getAddOptimisations(parallel);
            opts.setDangerousAssign(true);

            main.appendStatement(parallel);
            main.appendStatement(check(and(eq(L1, TWO), eq(L2, TWO)), "L21", "L22"));

            test.addExpected("L21");


            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 39: " + e);
        }
    }


    private void generateTest40() {
        try {
            Test test = makeBasicTest("insensitiveIf");

            RemoplaMethod m1 = method(rvoid(), "here");
            test.remopla.addMethod(m1);
            RemoplaMethod m2 = method(rvoid(), "there");
            test.remopla.addMethod(m2);
            RemoplaMethod m3 = method(rvoid(), "here2");
            test.remopla.addMethod(m3);
            RemoplaMethod m4 = method(rvoid(), "there2");
            test.remopla.addMethod(m4);

            m1.appendStatement(labL("L1"));
            m2.appendStatement(labL("L2"));
            m3.appendStatement(labL("L3"));
            m4.appendStatement(labL("L4"));


            RemoplaMethod main = test.remopla.getInitialMethod();
            main.addLocalVar(L1);

            main.appendStatement(assign(L1, ONE));

            RemoplaIfStmt if1 = new RemoplaIfStmt();
            StmtOptimisations opts = getAddOptimisations(if1);
            opts.addInsensitiveGuard(if1.addClause(eq(L1, ONE), invoke("here")));
            if1.addElse(invoke("there"));
            opts.setElseSensitiveToIntermediateLabels(false);
            RemoplaIfStmt if2 = new RemoplaIfStmt();
            StmtOptimisations opts2 = getAddOptimisations(if2);
            opts.addInsensitiveGuard(if2.addClause(eq(L1, TWO), invoke("here2")));
            if2.addElse(invoke("there2"));
            opts.setElseSensitiveToIntermediateLabels(false);

            main.appendStatement(if1);
            main.appendStatement(if2);

            test.addExpected("L1", "L4");

            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 40: " + e);
        }
    }


    private void generateTest41() {
        try {
            Test test = makeBasicTest("insensitiveIfundef");

            RemoplaMethod m1 = method(rvoid(), "here");
            test.remopla.addMethod(m1);
            RemoplaMethod m2 = method(rvoid(), "there");
            test.remopla.addMethod(m2);
            RemoplaMethod m3 = method(rvoid(), "here2");
            test.remopla.addMethod(m3);
            RemoplaMethod m4 = method(rvoid(), "there2");
            test.remopla.addMethod(m4);

            m1.appendStatement(labL("L1"));
            m2.appendStatement(labL("L2"));
            m3.appendStatement(labL("L3"));
            m4.appendStatement(labL("L4"));


            RemoplaMethod main = test.remopla.getInitialMethod();
            main.addLocalVar(L1);

            main.appendStatement(assign(L1, UNDEF));

            RemoplaIfStmt if1 = new RemoplaIfStmt();
            StmtOptimisations opts1 = getAddOptimisations(if1);
            opts1.addInsensitiveGuard(if1.addClause(eq(L1, ONE), invoke("here")));
            if1.addElse(invoke("there"));
            opts1.setElseSensitiveToIntermediateLabels(false);
            RemoplaIfStmt if2 = new RemoplaIfStmt();
            StmtOptimisations opts2 = getAddOptimisations(if2);
            opts2.addInsensitiveGuard(if2.addClause(eq(L1, TWO), invoke("here2")));
            if2.addElse(invoke("there2"));
            opts2.setElseSensitiveToIntermediateLabels(false);

            main.appendStatement(if1);
            main.appendStatement(if2);

            test.addExpected("L1", "L2", "L3", "L4");

            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 41: " + e);
        }
    }

    private void generateTest42() {
        try {
            Test test = makeBasicTest("insensitiveIfblock");

            RemoplaMethod m1 = method(rvoid(), "here");
            test.remopla.addMethod(m1);
            RemoplaMethod m2 = method(rvoid(), "there");
            test.remopla.addMethod(m2);
            RemoplaMethod m3 = method(rvoid(), "here2");
            test.remopla.addMethod(m3);
            RemoplaMethod m4 = method(rvoid(), "there2");
            test.remopla.addMethod(m4);

            m1.appendStatement(labL("L1"));
            m2.appendStatement(labL("L2"));
            m3.appendStatement(labL("L3"));
            m4.appendStatement(labL("L4"));


            RemoplaMethod main = test.remopla.getInitialMethod();
            main.addLocalVar(L1);

            main.appendStatement(assign(L1, ONE));

            RemoplaIfStmt if1 = new RemoplaIfStmt();
            StmtOptimisations opts1 = getAddOptimisations(if1);
            opts1.addInsensitiveGuard(if1.addClause(eq(L1, ONE), block(skip(), invoke("here"))));
            if1.addElse(block(skip(), invoke("there")));
            RemoplaIfStmt if2 = new RemoplaIfStmt();
            StmtOptimisations opts2 = getAddOptimisations(if2);
            opts2.addInsensitiveGuard(if2.addClause(eq(L1, TWO), invoke("here2")));
            if2.addElse(invoke("there2"));
            opts2.setElseSensitiveToIntermediateLabels(false);

            main.appendStatement(if1);
            main.appendStatement(if2);

            test.addExpected("L1", "L4");

            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 42: " + e);
        }
    }

    private void generateTest43() {
        try {
            Test test = makeBasicTest("insensitiveIfmulti");

            RemoplaMethod main = test.remopla.getInitialMethod();
            main.addLocalVar(L1);

            main.appendStatement(assign(L1, UNDEF));

            RemoplaIfStmt if1 = new RemoplaIfStmt();
            StmtOptimisations opts1 = getAddOptimisations(if1);
            if1.addClause(eq(L1, ONE), labL("L1"));
            if1.addElse(labL("L2"));
            opts1.setElseSensitiveToIntermediateLabels(false);
            RemoplaIfStmt if2 = new RemoplaIfStmt();
            StmtOptimisations opts2 = getAddOptimisations(if2);
            opts2.addInsensitiveGuard(if2.addClause(eq(L1, TWO), labL("L3")));
            if2.addElse(labL("L4"));

            main.appendStatement(if1);
            main.appendStatement(if2);

            test.addExpected("L1", "L4");

            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 43: " + e);
        }
    }



    private void generateTest44() {
        try {
            Test test = makeBasicTest("selfAssign");

            RemoplaMethod main = test.remopla.getInitialMethod();
            main.addLocalVar(L1);
            main.appendStatement(assign(L1, ONE));
            main.appendStatement(check(eq(L1, ONE), "L11", "L12"));
            main.appendStatement(assign(L1, L1));
            main.appendStatement(check(eq(L1, ONE), "L21", "L22"));
            main.appendStatement(assign(L1, add(L1, ONE)));
            main.appendStatement(check(eq(L1, TWO), "L31", "L32"));

            test.addExpected("L11", "L21", "L31");
            test.generateUnexpected();

            addTest(test);
        } catch (Exception e) {
            logger.error("Exception generating test 44: " + e);
        }
    }

    private StmtOptimisations getAddOptimisations(RemoplaStmt s) {
        StmtOptimisations opts = stmtOptimisations.get(s);
        if (opts == null) {
            opts = new StmtOptimisations(s);
            stmtOptimisations.put(s, opts);
        }
        return opts;
    }
}
