/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


package treeped.css.analysis;

// TODO: remove unneeded imports
import java.util.Collection;
import java.util.List;
import java.util.Map;

import net.sf.javabdd.BDD;

import de.tum.in.wpds.Fa;
import de.tum.in.wpds.PreSat;
import de.tum.in.wpds.Semiring;

import treeped.css.Namer;
import treeped.css.representation.AbstractHTML;
import treeped.css.representation.CSSClass;
import treeped.css.representation.SimpleRuleGuard;
import treeped.css.translation.HTMLToMopedTranslator;
import treeped.exceptions.VariableNotFoundException;
import treeped.main.CmdOptions;
import treeped.remoplatopds.BDDSemiring;
import treeped.remoplatopds.RemoplaToPDS;
import treeped.remoplatopds.StmtOptimisations;
import treeped.remoplatopds.VarManager;
import treeped.representation.Remopla;
import treeped.representation.RemoplaMethod;
import treeped.representation.RemoplaStmt;
import treeped.representation.RemoplaVar;



/**
 * Class adding checker that constructs a single BDD from a single call to moped
 * for all tests
 */
public class OneBDDClassAddingChecker
extends ClassAddingChecker {

    private Semiring possInit = null;

    public OneBDDClassAddingChecker(AbstractHTML<SimpleRuleGuard> ahtml)
    throws Exception {
        this.ahtml = ahtml;

        // we need to add new global vars so we copy-paste the parent
        // constructor rather than do anything elegant...

        HTMLToMopedTranslator tr = new HTMLToMopedTranslator();
        HTMLToMopedTranslator.RemoplaAndVars rvs
            = tr.translateSimpleHTML(ahtml, ahtml.getRhsClasses(), true);

        Remopla remopla = rvs.remopt.remopla;
        Map<RemoplaStmt, StmtOptimisations> stmtOptimisations
            = rvs.remopt.stmtOptimisations;

        this.vars = rvs.vars;

        // make sure we have xa for all classes a with ya
        Collection<RemoplaVar> gs = remopla.getGlobals();
        for (CSSClass c : this.vars.getYVarClasses()) {
            RemoplaVar v = this.vars.getAddXVar(c);
            if (!gs.contains(v)) {
                remopla.addGlobalVar(v);
            }
        }

        this.pds = RemoplaToPDS.translate(remopla, stmtOptimisations);

        // Creates variable manager
        this.manager = new VarManager(CmdOptions.getBddPackage(),
                                      CmdOptions.getNodeNum(),
                                      CmdOptions.getCacheSize(),
                                      1, // no integers, just bools
                                      remopla,
                                      stmtOptimisations);

        // initial fun label
        RemoplaMethod nodeFunMethod = remopla.getMethod(Namer.getNodeMethodName());
        this.nodeFun = nodeFunMethod.getStmts().get(0).getLabel();
    }


    public List<PushdownRule> getWitness(PushdownJustification justification) {
        logger.error("OneBDDClassAddingChecker doesn't do witnesses yet");
        System.exit(-1);
        // TODO: this
        return null;
    }



    protected CheckResult doCheck(CSSClass addClass,
                                  Collection<CSSClass> initClasses,
                                  Collection<CSSClass> parentClasses,
                                  Collection<CSSClass> childClasses,
                                  boolean isRoot)
    throws Exception {

        //  + Build an automaton that accepts single element stack
        //    with addClass set to true
        //  + Do the pre*
        //  + True iff config that corresponds to I_{initClasses,
        //    parentClasses, isRoot} is accepted by the pre*

        Semiring possInit = getPossibleInitSemiring(addClass);


        // create the semiring describing the initial state we're testing
        BDD initBDD = createInitBDD(addClass,
                                    isRoot,
                                    initClasses,
                                    parentClasses,
                                    childClasses);
        BDDSemiring initRing = new BDDSemiring(manager, initBDD);

        Semiring res = possInit.id().andWith(initRing);

        boolean result = !res.isZero();

        res.free();

        CheckResult cres;
        if (getJustification()) {
            // TODO: get witness (memoize it!)
            throw new Exception("TODO: implemenent witness generation!");
        } else {
            cres = new CheckResult(result);
        }
        return cres;
    }


    protected CheckResult doCheckFullTree(CSSClass addClass,
                                          Collection<CSSClass> initClasses,
                                          Collection<CSSClass> parentClasses,
                                          Collection<CSSClass> childClasses,
                                          boolean isRoot)
    throws Exception {
        // not sure how to do this with one bdd in fact!
        // the trick of using the spare xs in G1 as a proxy will not work for an
        // arbitrary length target stack (we want to reach any stack with ya set
        // to true, not just the single node stack like in check()).
        throw new VariableNotFoundException("todo abuse");
    }




    /**
     * Memoization of createPossibleInitSemiring
     */
    private Semiring getPossibleInitSemiring(CSSClass addClass)
    throws VariableNotFoundException {
        if (possInit == null)
            possInit = createPossibleInitSemiring();
        return possInit;
    }

    /**
     * Uses moped to get a semiring giving all possible initial variable
     * assignments that can eventually lead to the addition of classes set to
     * true in G1
     *
     * I.e. a (G0, L0, G1) semiring where G0 is the xs, L0 the ys and zs, and G1
     * contains xs such that xb is true if a state is reached where class b is
     * labelling the tree node.
     *
     * @return a semiring representation of the above
     */
    private Semiring createPossibleInitSemiring()
    throws VariableNotFoundException {

        // Initializes target FA
        BDD targetBDD = createTargetBDD();
        // the target aut just needs one transition
        Fa fa = new Fa();
        fa.add(new BDDSemiring(manager, targetBDD),
               Fa.q_i,
               nodeFun,
               Fa.q_f);

        // pre*
        PreSat sat = new PreSat(pds, manager);
        PreSat.PreSatResult preFa = sat.prestar(fa, null);

        // get copy of result semiring
        Semiring res = preFa.fa.getWeight(Fa.q_i, nodeFun, Fa.q_f);

        if (res != null)
            res = res.id();
        else
            res = new BDDSemiring(manager, manager.zero());

        // free all others
        preFa.free();

        return res;
    }


    /**
     * Create bdd for initial config (x1,...,xn) nodeFun(root,y1,...,yn,z1,...,zn)
     * where
     *
     *      pop = true
     *      root = isRoot
     *      x1,...,xn = childClasses
     *      y1,...,yn = initClasses
     *      z1,...,zn = parentClasses
     *
     * and the G1 copy of xb is true, where b is addClass, all other xa can be
     * anything
     *
     * @param addClass the class to add
     * @param isRoot whether the parent of the node being tested is root
     * @param initClasses the classes the initial node has
     * @param parentClasses the classes its parent has
     * @param childClasses the classes its child has
     * @param vars the transvars manager of the abstract html created during conversion to remopla
     * @param manager the bdd variable manager
     * @return a bdd that can be the weight on an fa transition, asserting the
     * values of the variables as above
     */
    private BDD createInitBDD(CSSClass addClass,
                              boolean isRoot,
                              Collection<CSSClass> initClasses,
                              Collection<CSSClass> parentClasses,
                              Collection<CSSClass> childClasses)
    throws VariableNotFoundException {
        BDD bdd = manager.initVars();

        for (CSSClass c : vars.getXVarClasses()) {
            manager.initBoolVar(vars.getXVar(c),
                                childClasses.contains(c),
                                bdd);
        }

        for (CSSClass c : vars.getYVarClasses()) {
            manager.initBoolVar(vars.getYVar(c),
                                initClasses.contains(c),
                                bdd);
        }

        for (CSSClass c : vars.getZVarClasses()) {
            manager.initBoolVar(vars.getZVar(c),
                                parentClasses.contains(c),
                                bdd);
        }

        manager.initBoolVar(vars.getRootVar(), isRoot, bdd);
        manager.initBoolVar(vars.getPopVar(), true, bdd);
        manager.initG1BoolVar(vars.getXVar(addClass), true, bdd);

        return bdd;
    }


    /**
     * Create bdd for target config (x1,...,xn) nodeFun(root,y1,...,yn,z1,...,zn)
     * where yi for class addClass is true (anything else can be anything)
     *
     * @return a bdd that can be the weight on an fa transition, asserting the
     * values of the variables as above
     */
    private BDD createTargetBDD()
    throws VariableNotFoundException {
        BDD bdd = manager.initVars();

        for (CSSClass c : vars.getYVarClasses()) {
            RemoplaVar y = vars.getYVar(c);
            RemoplaVar x = vars.getXVar(c);
            manager.makeG0L0VarEqualG1Var(y, x, bdd);
        }

        return bdd;
    }


}
