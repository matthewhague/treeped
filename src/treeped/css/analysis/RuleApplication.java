/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * An application of a rule for traces
 */


package treeped.css.analysis;

import treeped.css.representation.Rule;
import treeped.css.representation.SimpleRuleGuard;

public class RuleApplication {
    // the rule applied
    public Rule<SimpleRuleGuard> rule;
    // the ids of the nodes matching the up, down, and flat components of
    // the rule.  See nodeIdToNodeMap.  Can be -1 if they don't need to
    // match up/down.
    public int upNodeId;
    public int flatNodeId;
    public int downNodeId;
    // if an add child, this is the new node id (else -1)
    public int newChildId;

    public RuleApplication(Rule<SimpleRuleGuard> rule,
                           int upNodeId,
                           int flatNodeId,
                           int downNodeId,
                           int newChildId) {
        this.rule = rule;
        this.upNodeId = upNodeId;
        this.flatNodeId = flatNodeId;
        this.downNodeId = downNodeId;
        this.newChildId = newChildId;
    }


    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(rule.toString());
        sb.append(" [[ ");
        if (upNodeId > -1) {
            sb.append("parentId = ");
            sb.append(upNodeId);
            sb.append(", ");
        }
        sb.append("nodeId = ");
        sb.append(flatNodeId);
        if (downNodeId > -1) {
            sb.append(", downMatchId = ");
            sb.append(downNodeId);
        }
        if (newChildId > -1) {
            sb.append(", newChildId = ");
            sb.append(newChildId);
        }
        sb.append(" ]]");

        return sb.toString();
    }
}




