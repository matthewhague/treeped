/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Class for storing enough information to get witnesses for class adding checks
 * that used pushdown analysis.  This is super simple at the moment, but
 * ultimately i think we'll need to do this lazily.
 */

package treeped.css.analysis;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import treeped.css.representation.AHTMLTree;
import treeped.css.representation.CSSClass;

public class PushdownJustification {

    private AHTMLTree node;
    private AHTMLTree child;
    private boolean fullTree;
    private ClassAddingChecker checker;
    private CSSClass addClass;
    // we have to save these in case the node or child is modified
    private Set<CSSClass> parentClasses = new HashSet<CSSClass>();
    private Set<CSSClass> nodeClasses = new HashSet<CSSClass>();
    private Set<CSSClass> childClasses = new HashSet<CSSClass>();

    /**
     * @param addClass the class being added
     * @param node the node the witness starts from
     * @param child the child node used to match initial down guards
     * @param fullTree true if it was a full tree check
     * @param checker the checker used to do the analysis
     */
    public PushdownJustification(CSSClass addClass,
                                 AHTMLTree node,
                                 AHTMLTree child,
                                 boolean fullTree,
                                 ClassAddingChecker checker) {
        this.addClass = addClass;
        setNode(node);
        setChild(child);
        this.fullTree = fullTree;
        this.checker = checker;
    }

    /**
     * @param addClass the class being added
     * @param fullTree true if it was a full tree check
     * @param checker the checker used to do the analysis
     */
    public PushdownJustification(CSSClass addClass,
                                 boolean fullTree,
                                 ClassAddingChecker checker) {
        this.addClass = addClass;
        this.fullTree = fullTree;
        this.checker = checker;
    }


    public List<PushdownRule> getWitness() {
        return checker.getWitness(this);
    }

    /**
     * @return the node the check was run from (or null if not known)
     */
    public AHTMLTree getNode() {
        return node;
    }

    /**
     * @return the given child the check was run from (or null if not known)
     */
    public AHTMLTree getChild() {
        return child;
    }

    /**
     * @return the class that was added
     */
    public CSSClass getAddClass() {
        return addClass;
    }

    /**
     * @return whether it was a full tree check
     */
    public boolean isFullTreeCheck() {
        return fullTree;
    }

    /**
     * @param node the node from which the justification first applies
     */
    public void setNode(AHTMLTree node) {
        this.node = node;
        nodeClasses.clear();
        parentClasses.clear();
        if (node != null) {
            nodeClasses.addAll(node.getCSSClasses());
            if (!node.isRoot())
                parentClasses.addAll(node.getParent().getCSSClasses());
        }
    }

    /**
     * @param child the child of the node passed to the class adding checker
     * (i.e. the child that matches down rules for children not added during
     * pushdown checking)
     */
    public void setChild(AHTMLTree child) {
        this.child = child;
        childClasses.clear();
        if (child != null)
            childClasses.addAll(child.getCSSClasses());
    }

    /**
     * @return the css classes labelling the parent when the justification
     * occurred
     */
    public Collection<CSSClass> getParentClasses() {
        return parentClasses;
    }

    /**
     * @return the css classes labelling the node when the justification
     * occurred
     */
    public Collection<CSSClass> getNodeClasses() {
        return nodeClasses;
    }

    /**
     * @return the css classes labelling the child when the justification
     * occurred
     */
    public Collection<CSSClass> getChildClasses() {
        return childClasses;
    }

}
