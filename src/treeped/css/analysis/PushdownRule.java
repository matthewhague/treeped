/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Each pushdown rule either represents a rule application or is a pop move.
 * this is a convenience class for storing the rule application, or representing
 * the pop
 */

package treeped.css.analysis;

import treeped.css.representation.Rule;
import treeped.css.representation.SimpleRuleGuard;


public class PushdownRule {

    private Rule<SimpleRuleGuard> rule;

    /**
     * Construct a representation of a tree rule in the pushdown run.
     *
     * @param rule the rule
     */
    public PushdownRule(Rule<SimpleRuleGuard> rule) {
        this.rule = rule;
    }

    /**
     * Construct a representation of a pop rule in the pushdown run
     */
    public PushdownRule() {
        this.rule = null;
    }

    /**
     * @return true if the rule is a pop
     */
    public boolean isPop() {
        return (rule == null);
    }


    /**
     * @return the tree rule represented, or null if not a tree rule
     */
    public Rule<SimpleRuleGuard> getRule() {
        return rule;
    }

    public String toString() {
        if (rule == null)
            return "Pop";
        else
            return rule.toString();
    }
}
