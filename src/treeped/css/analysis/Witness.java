/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Class for storing a witness for a class addition
 */

package treeped.css.analysis;

import java.util.List;
import java.util.Map;

import treeped.css.representation.AHTMLTree;
import treeped.css.representation.CSSClass;
import treeped.css.representation.Rule;
import treeped.css.representation.SimpleRuleGuard;

public class Witness {
    /**
     * For adding extra info to a trace, pass to toString
     */
    public interface WitnessAnnotator {
        /**
         * @return The annotation for a rule or null if none
         */
        public String getRuleAnnotation(Rule<SimpleRuleGuard> r);
    }


    private CSSClass addClass;
    private List<RuleApplication> trace;
    private AHTMLTree initTree;
    private Map<AHTMLTree, Integer> nodeIds;

    /**
     * @param addClass the class added by the trace
     * @param trace the list of rule applications
     * @param initTree the initial AHTML tree
     * @param nodeIds a map from nodes to ids for original tree
     */
    public Witness(CSSClass addClass,
                   List<RuleApplication> trace,
                   AHTMLTree initTree,
                   Map<AHTMLTree, Integer> nodeIds) {
        this.addClass = addClass;
        this.trace = trace;
        this.initTree = initTree;
        this.nodeIds = nodeIds;
    }

    public String toString() {
        return toString(null);
    }

    public String toString(WitnessAnnotator annotator) {
        StringBuffer sb = new StringBuffer();

        sb.append("Saturated tree: \n");
        initTree.toString(sb, new AHTMLTree.AHTMLAnnotator() {
            public String getAnnotation(AHTMLTree node) {
                return "id = " + nodeIds.get(node);
            }
        });

        sb.append("\nWitness for ");
        sb.append(addClass);
        sb.append(":\n");

        for (RuleApplication r : trace) {
            sb.append(r);
            if (annotator != null) {
                String annot = annotator.getRuleAnnotation(r.rule);
                if (annot != null) {
                    sb.append(" (");
                    sb.append(annot);
                    sb.append(")");
                }
            }
            sb.append("\n");
        }

        return sb.toString();
    }


}
