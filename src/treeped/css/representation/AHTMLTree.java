/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


package treeped.css.representation;

import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

import treeped.util.EnumIterator;

public class AHTMLTree extends DefaultMutableTreeNode {

    /**
     * For toString, can add custom annotations
     */
    public interface AHTMLAnnotator {
        /**
         * @param node a node of the tree
         * @return the string to annotate the node with
         */
        public String getAnnotation(AHTMLTree node);
    }

    private static final String INDENT = "    ";

    // serialisable -- we don't use this, so just give it ID 0
    static final long serialVersionUID = 0;

    /**
     * Creates a new tree (node) with no labelling css classes
     */
    public AHTMLTree() {
        setUserObject(new HashSet<CSSClass>());
    }

    /**
     * Creates a new tree (node)
     *
     * @param classes the css classes labelling the new node
     */
    public AHTMLTree(Set<CSSClass> classes) {
        setUserObject(new HashSet<CSSClass>(classes));
    }

    public void setCSSClasses(Collection<CSSClass> classes) {
        @SuppressWarnings("unchecked")
        Set<CSSClass> cs = (Set<CSSClass>)getUserObject();
        cs.clear();
        cs.addAll(classes);
    }

    public void addCSSClass(CSSClass c) {
        @SuppressWarnings("unchecked")
        Set<CSSClass> classes = (Set<CSSClass>)getUserObject();
        classes.add(c);
    }

    public void addCSSClasses(Collection<CSSClass> cs) {
        @SuppressWarnings("unchecked")
        Set<CSSClass> classes = (Set<CSSClass>)getUserObject();
        classes.addAll(cs);
    }


    /**
     * @return if class c is in the label of the node
     */
    @SuppressWarnings("unchecked")
    public boolean hasCSSClass(CSSClass c) {
        return ((Set<CSSClass>)getUserObject()).contains(c);
    }

    public Collection<CSSClass> getCSSClasses() {
        @SuppressWarnings("unchecked")
        Set<CSSClass> classes = (Set<CSSClass>)getUserObject();
        return Collections.unmodifiableSet(classes);
    }

    public Iterable<AHTMLTree> depthFirstIterator() {
        return new EnumIterator<AHTMLTree>(depthFirstEnumeration());
    }

    public Iterable<AHTMLTree> getChildren() {
        return new EnumIterator<AHTMLTree>(children());
    }

    public AHTMLTree getParent() {
        return (AHTMLTree)super.getParent();
    }

    /** Writes the expression to a string buffer
      *  @param out the buffer to write to
      *  @return the buffer
      */
    public StringBuffer toString(StringBuffer out) {
        return toString(out, "", null);
    }

    public String toString() {
        return toString(new StringBuffer()).toString();
    }

    public StringBuffer toString(StringBuffer out,
                                 AHTMLAnnotator annotator) {
        return toString(out, "", annotator);
    }


    /** Writes the expression to a string buffer
      *  @param out the buffer to write to
      *  @param indent the indent to put before the nodes
      *  @param annotator something to annotate nodes of tree, or null
      *  @return the buffer
      */
    private StringBuffer toString(StringBuffer out,
                                  String indent,
                                  AHTMLAnnotator annotator) {
        out.append(indent);

        out.append("<node");

        Collection<CSSClass> cs = getCSSClasses();
        if (!cs.isEmpty()) {
            out.append(" class=\"");
            for (CSSClass c : getCSSClasses()) {
                out.append(c);
                out.append(" ");
            }
            out.append("\"");
        }

        if (getChildCount() == 0) {
            out.append("/>");
            if (annotator != null) {
                out.append(" [[ ");
                out.append(annotator.getAnnotation(this));
                out.append(" ]]");
            }
        } else {
            out.append(">");
            if (annotator != null) {
                out.append(" [[ ");
                out.append(annotator.getAnnotation(this));
                out.append(" ]]");
            }
            out.append("\n");

            @SuppressWarnings("unchecked")
            Enumeration<TreeNode> e = children();
            while (e.hasMoreElements()) {
                AHTMLTree child = (AHTMLTree)e.nextElement();
                child.toString(out, indent + INDENT, annotator);
                out.append("\n");
            }

            out.append(indent);
            out.append("</node>");
        }

        return out;
    }


    public AHTMLTree clone() {
        return (AHTMLTree)super.clone();
    }

}
