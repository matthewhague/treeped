/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


package treeped.css.representation;

import java.util.LinkedList;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;

public class RuleStaticFactory {
    // if we want to return a ruleguard by visitor...
    private static class VisitorResult {
        RuleGuard guard;
    }
    private static final VisitorResult visitorResult = new VisitorResult();

    private static final SimpleRuleGuard trueGuard
        = new SimpleRuleGuard(CSSClass.makeSet(),
                              CSSClass.makeSet(),
                              CSSClass.makeSet());

    public static SimpleRuleGuard trueGuard() {
        return trueGuard;
    }

    public static SimpleRuleGuard flat(CSSClass c) {
        return new SimpleRuleGuard(null, c, null);
    }

    public static SimpleRuleGuard flat(Set<CSSClass> classes) {
        return new SimpleRuleGuard(null, classes, null);
    }

    public static SimpleRuleGuard up(Set<CSSClass> classes) {
        return new SimpleRuleGuard(classes, null, null);
    }

    public static SimpleRuleGuard down(Set<CSSClass> classes) {
        return new SimpleRuleGuard(null, null, classes);
    }

    public static RuleGuard upPlus(RuleGuard g) {
        return new RuleUpPlusGuard(g);
    }

    public static RuleGuard downPlus(RuleGuard g) {
        return new RuleDownPlusGuard(g);
    }

    public static RuleGuard upStar(RuleGuard g) {
        return new RuleUpStarGuard(g);
    }

    public static RuleGuard downStar(RuleGuard g) {
        return new RuleDownStarGuard(g);
    }


    public static RuleGuard up(final RuleGuard g) {
        g.accept(new AbstractRuleGuardVisitor() {
            public void visit(SimpleRuleGuard g) {
                if (g.getUpClasses().isEmpty() &&
                    g.getDownClasses().isEmpty()) {
                    visitorResult.guard = new SimpleRuleGuard(g.getFlatClasses(),
                                                              null,
                                                              null);
                } else {
                    visitorResult.guard = new RuleUpGuard(g);
                }
            }

            public void defaultCase(RuleGuard g) {
                visitorResult.guard = new RuleUpGuard(g);
            }
        });
        return visitorResult.guard;
    }

    public static RuleGuard down(RuleGuard g) {
        g.accept(new AbstractRuleGuardVisitor() {
            public void visit(SimpleRuleGuard g) {
                if (g.getUpClasses().isEmpty() &&
                    g.getDownClasses().isEmpty()) {
                    visitorResult.guard = new SimpleRuleGuard(null,
                                                              null,
                                                              g.getFlatClasses());
                } else {
                    visitorResult.guard = new RuleDownGuard(g);
                }
            }

            public void defaultCase(RuleGuard g) {
                visitorResult.guard = new RuleDownGuard(g);
            }
        });
        return visitorResult.guard;
    }

    public static RuleGuard and(RuleGuard... gs) {
        // accumulate as many rules as possible into one (can combine simple
        // guards with not down matches)
        final SimpleRuleGuard accumulated = new SimpleRuleGuard();
        final List<RuleGuard> unaccumulated = new LinkedList<RuleGuard>();

        for (int i = 0; i < gs.length; ++i) {
            gs[i].accept(new AbstractRuleGuardVisitor() {
                public void visit(SimpleRuleGuard g) {
                    if (g.getDownClasses().isEmpty()) {
                        accumulated.addUpClasses(g.getUpClasses());
                        accumulated.addFlatClasses(g.getFlatClasses());
                    } else {
                        unaccumulated.add(g);
                    }
                }

                public void defaultCase(RuleGuard g) {
                    unaccumulated.add(g);
                }
            });
        }

        // don't check down classes because we don't accumulate them
        if (!accumulated.getUpClasses().isEmpty() ||
            !accumulated.getFlatClasses().isEmpty()) {
            unaccumulated.add(accumulated);
        }

        switch(unaccumulated.size()) {
        case 0:
            return trueGuard();
        case 1:
            return unaccumulated.get(0);
        default:
            return new RuleAndGuard(unaccumulated);
        }
    }

    public static RuleGuard or(RuleGuard... gs) {
        // accumulate as many rules as possible into one (can combine multiple
        // ors into one big one
        final List<RuleGuard> unaccumulated = new LinkedList<RuleGuard>();

        for (int i = 0; i < gs.length; ++i) {
            gs[i].accept(new AbstractRuleGuardVisitor() {
                public void visit(RuleOrGuard g) {
                        unaccumulated.addAll(g.getSubFormulas());
                }

                public void defaultCase(RuleGuard g) {
                    unaccumulated.add(g);
                }
            });
        }

        switch(unaccumulated.size()) {
        case 0:
            return trueGuard();
        case 1:
            return unaccumulated.get(0);
        default:
            return new RuleOrGuard(unaccumulated);
        }
    }
}
