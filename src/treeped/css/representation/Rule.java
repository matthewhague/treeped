/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


package treeped.css.representation;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;


public abstract class Rule<GuardType extends RuleGuard> {

    static Logger logger = Logger.getLogger(Rule.class);

    // generic CSS rule
    private GuardType guard;
    private Set<CSSClass> rhsClasses = new HashSet<CSSClass>();

    public Rule(GuardType guard,
                Collection<CSSClass> rhsClasses) {
        assert guard != null;
        this.guard = guard;
        if (rhsClasses != null)
            this.rhsClasses.addAll(rhsClasses);
    }

    public Rule(GuardType guard,
                CSSClass rhsClass) {
        assert guard != null;
        this.guard = guard;
        if (rhsClasses != null)
            this.rhsClasses.add(rhsClass);
    }

    public void addRHSClass(CSSClass c) {
        rhsClasses.add(c);
    }

    public GuardType getGuard() {
        return guard;
    }

    public Set<CSSClass> getRhsClasses() {
        return Collections.unmodifiableSet(rhsClasses);
    }

    /** Writes the expression to a string buffer
      *  @param out the buffer to write to
      *  @return the buffer
      */
    public StringBuffer toString(StringBuffer out) {
        logger.warn("Rule output not implemented.");
        out.append("[Rule output not defined]");
        return out;
    }

    public String toString() {
        return toString(new StringBuffer()).toString();
    }

    @SuppressWarnings("unchecked")
    public boolean equals(Object o) {
        if (o == null)
            return false;
        return this.getClass().equals(o.getClass()) &&
               this.guard.equals(((Rule<GuardType>)o).guard) &&
               this.rhsClasses.equals(((Rule<GuardType>)o).rhsClasses);
    }

    public int hashCode() {
        return guard.hashCode() +
               rhsClasses.hashCode() +
               this.getClass().hashCode();
    }

    public abstract void accept(RuleVisitor<GuardType> visitor);

    protected void rhsClassesToString(StringBuffer out) {
        classesToString(rhsClasses, out);
    }

    private void classesToString(Set<CSSClass> s, StringBuffer out) {
        out.append("{ ");
        for (CSSClass c : s) {
            if (c == null)
                out.append("<null>");
            else
                c.toString(out);
            out.append(" ");
        }
        out.append("}");
    }
}
