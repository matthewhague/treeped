/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// TranVars.java
// Author: Matthew Hague
//
// Class to manage the variables x1, ... , xk, y1, ..., yk, root and pop


package treeped.css.translation;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.HashMap;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import treeped.representation.RemoplaBoolVar;
import treeped.css.Namer;
import treeped.css.representation.CSSClass;
import treeped.css.representation.Rule;
import treeped.css.representation.SimpleRuleGuard;
import treeped.exceptions.VariableNotFoundException;

public class TranVars {

    static Logger logger = Logger.getLogger(TranVars.class);

    private Map<CSSClass, RemoplaBoolVar> xs
        = new HashMap<CSSClass, RemoplaBoolVar>();
    private Map<CSSClass, RemoplaBoolVar> ys
        = new HashMap<CSSClass, RemoplaBoolVar>();
    private Map<CSSClass, RemoplaBoolVar> zs
        = new HashMap<CSSClass, RemoplaBoolVar>();

    private Map<RemoplaBoolVar, CSSClass> xsRev
        = new HashMap<RemoplaBoolVar, CSSClass>();
    private Map<RemoplaBoolVar, CSSClass> ysRev
        = new HashMap<RemoplaBoolVar, CSSClass>();
    private Map<RemoplaBoolVar, CSSClass> zsRev
        = new HashMap<RemoplaBoolVar, CSSClass>();

    private RemoplaBoolVar root
        = new RemoplaBoolVar(Namer.getRootVar(), true, true);
    private RemoplaBoolVar pop
        = new RemoplaBoolVar(Namer.getPopVar(), false, true);
    private SortedSet<CSSClass> xClasses = new TreeSet<CSSClass>();
    private SortedSet<CSSClass> yClasses = new TreeSet<CSSClass>();
    private SortedSet<CSSClass> zClasses = new TreeSet<CSSClass>();

    /**
     * Name variables x0, x1, x2, ... y0, y1, y2, ... z0, z1...
     * So that remopla with the same number of variables has the same variable
     * names
     *
     * use, e.g. getNewXName()
     */
    private int nextXVar = 0;
    private int nextYVar = 0;
    private int nextZVar = 0;

    /**
     * Get all variable info for a given abstract html object
     *
     * @param rules the ahtml rules to build from
     * @param interestingClasses css classes that might not be needed for
     * allowing rules to be fired, but if we're interested in whether they're
     * added or not we will need a variable for them (doesn't add X vars, just
     * Y)
     */
    public TranVars(Collection<Rule<SimpleRuleGuard>> rules,
                    Collection<CSSClass> interestingClasses) {
        buildTranVars(rules, interestingClasses, false);
    }

    /**
     * Get all variable info for a given abstract html object
     *
     * @param rules the ahtml rules to build from
     * @param interestingClasses css classes that might not be needed for
     * allowing rules to be fired, but if we're interested in whether they're
     * added or not we will need a variable for them
     * @param interestingXVars whether to add X variables for interesting
     * classes or not
     */
    public TranVars(Collection<Rule<SimpleRuleGuard>> rules,
                    Collection<CSSClass> interestingClasses,
                    boolean interestingXVars) {
        buildTranVars(rules, interestingClasses, interestingXVars);
    }


    public RemoplaBoolVar getRootVar() {
        return root;
    }

    public RemoplaBoolVar getPopVar() {
        return pop;
    }

    /**
     * Will find a variable corresponding to the class
     */
    public RemoplaBoolVar getXVar(CSSClass c)
    throws VariableNotFoundException {
        RemoplaBoolVar v = xs.get(c);
        if (v == null) {
            throw new VariableNotFoundException("No X variable for CSS class " + c + ".");
        }
        return v;
    }

    /**
     * Will find a variable corresponding to the class, looks up alias if no var
     * found with same name
     */
    public RemoplaBoolVar getYVar(CSSClass c)
    throws VariableNotFoundException {
        RemoplaBoolVar v = ys.get(c);
        if (v == null) {
            throw new VariableNotFoundException("No Y variable for CSS class " + c + ".");
        }
        return v;
    }

    /**
     * @param c css class
     * @param true if the class has a y var (either direct or an alias)
     */
    public boolean hasYVar(CSSClass c) {
        return ys.containsKey(c);
    }

    /**
     * Will find a variable corresponding to the class
     */
    public RemoplaBoolVar getZVar(CSSClass c)
    throws VariableNotFoundException {
        RemoplaBoolVar v = zs.get(c);
        if (v == null)
            throw new VariableNotFoundException("No Z variable for CSS class " + c + ".");
        return v;
    }

    /**
     * @return CSSClasses in fixed order each time (as long as you don't add
     * classes in the meantime)
     */
    public SortedSet<CSSClass> getXVarClasses() {
        return Collections.unmodifiableSortedSet(xClasses);
    }

    /**
     * @return CSSClasses in fixed order each time (as long as you don't add
     * classes in the meantime)
     */
    public SortedSet<CSSClass> getYVarClasses() {
        return Collections.unmodifiableSortedSet(yClasses);
    }

    /**
     * @return CSSClasses in fixed order each time (as long as you don't add
     * classes in the meantime)
     */
    public SortedSet<CSSClass> getZVarClasses() {
        return Collections.unmodifiableSortedSet(zClasses);
    }

    /**
     * does the get but adds if not found, does not alias
     */
    public RemoplaBoolVar getAddXVar(CSSClass c) {
        RemoplaBoolVar v = xs.get(c);
        if (v == null) {
            v = new RemoplaBoolVar(getNewXName(), false, true);
            xs.put(c, v);
            xsRev.put(v, c);
            xClasses.add(c);
        }
        return v;
    }

    /**
     * does the get but adds if not found, does not alias
     */
    public RemoplaBoolVar getAddYVar(CSSClass c) {
        RemoplaBoolVar v = ys.get(c);
        if (v == null) {
            v = new RemoplaBoolVar(getNewYName(), true, true);
            ys.put(c, v);
            ysRev.put(v, c);
            yClasses.add(c);
        }
        return v;
    }


    /**
     * does the get but adds if not found, does not alias
     */
    public RemoplaBoolVar getAddZVar(CSSClass c) {
        RemoplaBoolVar v = zs.get(c);
        if (v == null) {
            v = new RemoplaBoolVar(getNewZName(), true, true);
            zs.put(c, v);
            zsRev.put(v, c);
            zClasses.add(c);
        }
        return v;
    }


    /**
     * Variables are given impenetrable names, use this function to find the
     * class associated to a given variable (e.g. y1 could be y(a) for class a)
     *
     * @param v the remopla variable
     * @return the css class associated to it or null if not found
     */
    public CSSClass getVarClass(RemoplaBoolVar v) {
        CSSClass c = xsRev.get(v);
        if (c != null)
            return c;
        c = ysRev.get(v);
        if (c != null)
            return c;
        return zsRev.get(v);
    }


    public String toString() {
        return toString(new StringBuffer()).toString();
    }

    public StringBuffer toString(StringBuffer sb) {
        sb.append(xs);
        sb.append(xsRev);
        sb.append(ys);
        sb.append(ysRev);
        sb.append(zs);
        sb.append(zsRev);
        return sb;
    }


    /**
     * Get all variable info for a given abstract html object
     *
     * @param rules the ahtml rules to build from
     * @param interestingClasses css classes that might not be needed for
     * allowing rules to be fired, but if we're interested in whether they're
     * added or not we will need a variable for them
     * @param interestingXVars whether to add X variables for interesting
     * classes or not
     */
    private void buildTranVars(Collection<Rule<SimpleRuleGuard>> rules,
                               Collection<CSSClass> interestingClasses,
                               boolean interestingXVars) {
        for (Rule<SimpleRuleGuard> r : rules) {
            for (CSSClass c : r.getGuard().getUpClasses()) {
                getAddZVar(c);
                getAddYVar(c);
            }
            for (CSSClass c : r.getGuard().getDownClasses()) {
                getAddXVar(c);
                getAddYVar(c);
            }
            for (CSSClass c : r.getGuard().getFlatClasses()) {
                getAddYVar(c);
            }
        }
        for (CSSClass c : interestingClasses) {
            getAddYVar(c);
            if (interestingXVars)
                getAddXVar(c);
        }
    }


    /**
     * @return a fresh name
     */
    private String getNewXName() {
        return Namer.getXVar(nextXVar++);
    }

    /** * @return a fresh name
     */
    private String getNewYName() {
        return Namer.getYVar(nextYVar++);
    }

    /**
     * @return a fresh name
     */
    private String getNewZName() {
        return Namer.getZVar(nextZVar++);
    }
}
