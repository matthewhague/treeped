// Generated from src/treeped/css/parser/AbstractHTML.g4 by ANTLR 4.2.2
package treeped.css.parser;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class AbstractHTMLParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__23=1, T__22=2, T__21=3, T__20=4, T__19=5, T__18=6, T__17=7, T__16=8, 
		T__15=9, T__14=10, T__13=11, T__12=12, T__11=13, T__10=14, T__9=15, T__8=16, 
		T__7=17, T__6=18, T__5=19, T__4=20, T__3=21, T__2=22, T__1=23, T__0=24, 
		ID=25, WS=26;
	public static final String[] tokenNames = {
		"<INVALID>", "'Up'", "'Up+'", "'AddClass('", "'True'", "'And'", "'Up*'", 
		"')'", "','", "'node'", "'<'", "'='", "'Down'", "'Down*'", "'Or'", "'</'", 
		"'{'", "'>'", "'/>'", "'AddChild('", "'}'", "'class'", "'CSS Classes:'", 
		"'Down+'", "'\"'", "ID", "WS"
	};
	public static final int
		RULE_prog = 0, RULE_abshtmlrule = 1, RULE_abshtmlguard = 2, RULE_upguardr = 3, 
		RULE_flatguardr = 4, RULE_downguardr = 5, RULE_classSet = 6, RULE_fullabshtmltree = 7, 
		RULE_abshtmltree = 8, RULE_nodeclassdecl = 9, RULE_cssclassesr = 10;
	public static final String[] ruleNames = {
		"prog", "abshtmlrule", "abshtmlguard", "upguardr", "flatguardr", "downguardr", 
		"classSet", "fullabshtmltree", "abshtmltree", "nodeclassdecl", "cssclassesr"
	};

	@Override
	public String getGrammarFileName() { return "AbstractHTML.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public AbstractHTMLParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgContext extends ParserRuleContext {
		public CssclassesrContext cssclassesr() {
			return getRuleContext(CssclassesrContext.class,0);
		}
		public List<AbshtmlruleContext> abshtmlrule() {
			return getRuleContexts(AbshtmlruleContext.class);
		}
		public AbshtmlruleContext abshtmlrule(int i) {
			return getRuleContext(AbshtmlruleContext.class,i);
		}
		public FullabshtmltreeContext fullabshtmltree() {
			return getRuleContext(FullabshtmltreeContext.class,0);
		}
		public ProgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prog; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).enterProg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).exitProg(this);
		}
	}

	public final ProgContext prog() throws RecognitionException {
		ProgContext _localctx = new ProgContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_prog);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(25);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 2) | (1L << 4) | (1L << 5) | (1L << 6) | (1L << 12) | (1L << 13) | (1L << 14) | (1L << 16) | (1L << 23))) != 0)) {
				{
				{
				setState(22); abshtmlrule();
				}
				}
				setState(27);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(29);
			_la = _input.LA(1);
			if (_la==10) {
				{
				setState(28); fullabshtmltree();
				}
			}

			setState(32);
			_la = _input.LA(1);
			if (_la==22) {
				{
				setState(31); cssclassesr();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AbshtmlruleContext extends ParserRuleContext {
		public AbshtmlruleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_abshtmlrule; }
	 
		public AbshtmlruleContext() { }
		public void copyFrom(AbshtmlruleContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AddChildContext extends AbshtmlruleContext {
		public AbshtmlguardContext guard;
		public ClassSetContext changeClasses;
		public ClassSetContext classSet() {
			return getRuleContext(ClassSetContext.class,0);
		}
		public AbshtmlguardContext abshtmlguard() {
			return getRuleContext(AbshtmlguardContext.class,0);
		}
		public AddChildContext(AbshtmlruleContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).enterAddChild(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).exitAddChild(this);
		}
	}
	public static class AddClassContext extends AbshtmlruleContext {
		public AbshtmlguardContext guard;
		public ClassSetContext changeClasses;
		public ClassSetContext classSet() {
			return getRuleContext(ClassSetContext.class,0);
		}
		public AbshtmlguardContext abshtmlguard() {
			return getRuleContext(AbshtmlguardContext.class,0);
		}
		public AddClassContext(AbshtmlruleContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).enterAddClass(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).exitAddClass(this);
		}
	}

	public final AbshtmlruleContext abshtmlrule() throws RecognitionException {
		AbshtmlruleContext _localctx = new AbshtmlruleContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_abshtmlrule);
		try {
			setState(46);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				_localctx = new AddClassContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(34); ((AddClassContext)_localctx).guard = abshtmlguard();
				setState(35); match(8);
				setState(36); match(3);
				setState(37); ((AddClassContext)_localctx).changeClasses = classSet();
				setState(38); match(7);
				}
				break;

			case 2:
				_localctx = new AddChildContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(40); ((AddChildContext)_localctx).guard = abshtmlguard();
				setState(41); match(8);
				setState(42); match(19);
				setState(43); ((AddChildContext)_localctx).changeClasses = classSet();
				setState(44); match(7);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AbshtmlguardContext extends ParserRuleContext {
		public AbshtmlguardContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_abshtmlguard; }
	 
		public AbshtmlguardContext() { }
		public void copyFrom(AbshtmlguardContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class UpPlusGuardContext extends AbshtmlguardContext {
		public AbshtmlguardContext abshtmlguard() {
			return getRuleContext(AbshtmlguardContext.class,0);
		}
		public UpPlusGuardContext(AbshtmlguardContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).enterUpPlusGuard(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).exitUpPlusGuard(this);
		}
	}
	public static class OrGuardContext extends AbshtmlguardContext {
		public AbshtmlguardContext abshtmlguard(int i) {
			return getRuleContext(AbshtmlguardContext.class,i);
		}
		public List<AbshtmlguardContext> abshtmlguard() {
			return getRuleContexts(AbshtmlguardContext.class);
		}
		public OrGuardContext(AbshtmlguardContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).enterOrGuard(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).exitOrGuard(this);
		}
	}
	public static class UpGuardContext extends AbshtmlguardContext {
		public AbshtmlguardContext abshtmlguard() {
			return getRuleContext(AbshtmlguardContext.class,0);
		}
		public UpGuardContext(AbshtmlguardContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).enterUpGuard(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).exitUpGuard(this);
		}
	}
	public static class UpStarGuardContext extends AbshtmlguardContext {
		public AbshtmlguardContext abshtmlguard() {
			return getRuleContext(AbshtmlguardContext.class,0);
		}
		public UpStarGuardContext(AbshtmlguardContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).enterUpStarGuard(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).exitUpStarGuard(this);
		}
	}
	public static class AndGuardContext extends AbshtmlguardContext {
		public AbshtmlguardContext abshtmlguard(int i) {
			return getRuleContext(AbshtmlguardContext.class,i);
		}
		public List<AbshtmlguardContext> abshtmlguard() {
			return getRuleContexts(AbshtmlguardContext.class);
		}
		public AndGuardContext(AbshtmlguardContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).enterAndGuard(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).exitAndGuard(this);
		}
	}
	public static class DownPlusGuardContext extends AbshtmlguardContext {
		public AbshtmlguardContext abshtmlguard() {
			return getRuleContext(AbshtmlguardContext.class,0);
		}
		public DownPlusGuardContext(AbshtmlguardContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).enterDownPlusGuard(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).exitDownPlusGuard(this);
		}
	}
	public static class SimpleGuardContext extends AbshtmlguardContext {
		public UpguardrContext up;
		public FlatguardrContext flat;
		public DownguardrContext down;
		public FlatguardrContext flatguardr() {
			return getRuleContext(FlatguardrContext.class,0);
		}
		public DownguardrContext downguardr() {
			return getRuleContext(DownguardrContext.class,0);
		}
		public UpguardrContext upguardr() {
			return getRuleContext(UpguardrContext.class,0);
		}
		public SimpleGuardContext(AbshtmlguardContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).enterSimpleGuard(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).exitSimpleGuard(this);
		}
	}
	public static class TrueGuardContext extends AbshtmlguardContext {
		public TrueGuardContext(AbshtmlguardContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).enterTrueGuard(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).exitTrueGuard(this);
		}
	}
	public static class DownStarGuardContext extends AbshtmlguardContext {
		public AbshtmlguardContext abshtmlguard() {
			return getRuleContext(AbshtmlguardContext.class,0);
		}
		public DownStarGuardContext(AbshtmlguardContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).enterDownStarGuard(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).exitDownStarGuard(this);
		}
	}
	public static class DownGuardContext extends AbshtmlguardContext {
		public AbshtmlguardContext abshtmlguard() {
			return getRuleContext(AbshtmlguardContext.class,0);
		}
		public DownGuardContext(AbshtmlguardContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).enterDownGuard(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).exitDownGuard(this);
		}
	}

	public final AbshtmlguardContext abshtmlguard() throws RecognitionException {
		AbshtmlguardContext _localctx = new AbshtmlguardContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_abshtmlguard);
		int _la;
		try {
			setState(91);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				_localctx = new SimpleGuardContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(48); ((SimpleGuardContext)_localctx).up = upguardr();
				setState(50);
				switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
				case 1:
					{
					setState(49); ((SimpleGuardContext)_localctx).flat = flatguardr();
					}
					break;
				}
				setState(53);
				switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
				case 1:
					{
					setState(52); ((SimpleGuardContext)_localctx).down = downguardr();
					}
					break;
				}
				}
				break;

			case 2:
				_localctx = new SimpleGuardContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(55); ((SimpleGuardContext)_localctx).flat = flatguardr();
				setState(57);
				switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
				case 1:
					{
					setState(56); ((SimpleGuardContext)_localctx).down = downguardr();
					}
					break;
				}
				}
				break;

			case 3:
				_localctx = new SimpleGuardContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(59); ((SimpleGuardContext)_localctx).down = downguardr();
				}
				break;

			case 4:
				_localctx = new UpPlusGuardContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(60); match(2);
				setState(61); abshtmlguard();
				}
				break;

			case 5:
				_localctx = new DownPlusGuardContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(62); match(23);
				setState(63); abshtmlguard();
				}
				break;

			case 6:
				_localctx = new UpStarGuardContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(64); match(6);
				setState(65); abshtmlguard();
				}
				break;

			case 7:
				_localctx = new DownStarGuardContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(66); match(13);
				setState(67); abshtmlguard();
				}
				break;

			case 8:
				_localctx = new UpGuardContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(68); match(1);
				setState(69); abshtmlguard();
				}
				break;

			case 9:
				_localctx = new DownGuardContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(70); match(12);
				setState(71); abshtmlguard();
				}
				break;

			case 10:
				_localctx = new AndGuardContext(_localctx);
				enterOuterAlt(_localctx, 10);
				{
				setState(72); match(5);
				setState(73); match(16);
				setState(77);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 2) | (1L << 4) | (1L << 5) | (1L << 6) | (1L << 12) | (1L << 13) | (1L << 14) | (1L << 16) | (1L << 23))) != 0)) {
					{
					{
					setState(74); abshtmlguard();
					}
					}
					setState(79);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(80); match(20);
				}
				break;

			case 11:
				_localctx = new OrGuardContext(_localctx);
				enterOuterAlt(_localctx, 11);
				{
				setState(81); match(14);
				setState(82); match(16);
				setState(86);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 1) | (1L << 2) | (1L << 4) | (1L << 5) | (1L << 6) | (1L << 12) | (1L << 13) | (1L << 14) | (1L << 16) | (1L << 23))) != 0)) {
					{
					{
					setState(83); abshtmlguard();
					}
					}
					setState(88);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(89); match(20);
				}
				break;

			case 12:
				_localctx = new TrueGuardContext(_localctx);
				enterOuterAlt(_localctx, 12);
				{
				setState(90); match(4);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UpguardrContext extends ParserRuleContext {
		public ClassSetContext guardClasses;
		public ClassSetContext classSet() {
			return getRuleContext(ClassSetContext.class,0);
		}
		public UpguardrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_upguardr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).enterUpguardr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).exitUpguardr(this);
		}
	}

	public final UpguardrContext upguardr() throws RecognitionException {
		UpguardrContext _localctx = new UpguardrContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_upguardr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(93); match(1);
			setState(94); ((UpguardrContext)_localctx).guardClasses = classSet();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FlatguardrContext extends ParserRuleContext {
		public ClassSetContext guardClasses;
		public ClassSetContext classSet() {
			return getRuleContext(ClassSetContext.class,0);
		}
		public FlatguardrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_flatguardr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).enterFlatguardr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).exitFlatguardr(this);
		}
	}

	public final FlatguardrContext flatguardr() throws RecognitionException {
		FlatguardrContext _localctx = new FlatguardrContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_flatguardr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(96); ((FlatguardrContext)_localctx).guardClasses = classSet();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DownguardrContext extends ParserRuleContext {
		public ClassSetContext guardClasses;
		public ClassSetContext classSet() {
			return getRuleContext(ClassSetContext.class,0);
		}
		public DownguardrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_downguardr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).enterDownguardr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).exitDownguardr(this);
		}
	}

	public final DownguardrContext downguardr() throws RecognitionException {
		DownguardrContext _localctx = new DownguardrContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_downguardr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(98); match(12);
			setState(99); ((DownguardrContext)_localctx).guardClasses = classSet();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassSetContext extends ParserRuleContext {
		public List<TerminalNode> ID() { return getTokens(AbstractHTMLParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(AbstractHTMLParser.ID, i);
		}
		public ClassSetContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classSet; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).enterClassSet(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).exitClassSet(this);
		}
	}

	public final ClassSetContext classSet() throws RecognitionException {
		ClassSetContext _localctx = new ClassSetContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_classSet);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(101); match(16);
			setState(105);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ID) {
				{
				{
				setState(102); match(ID);
				}
				}
				setState(107);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(108); match(20);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FullabshtmltreeContext extends ParserRuleContext {
		public FullabshtmltreeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fullabshtmltree; }
	 
		public FullabshtmltreeContext() { }
		public void copyFrom(FullabshtmltreeContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AHTMLTreeContext extends FullabshtmltreeContext {
		public AbshtmltreeContext rootNode;
		public AbshtmltreeContext abshtmltree() {
			return getRuleContext(AbshtmltreeContext.class,0);
		}
		public AHTMLTreeContext(FullabshtmltreeContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).enterAHTMLTree(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).exitAHTMLTree(this);
		}
	}

	public final FullabshtmltreeContext fullabshtmltree() throws RecognitionException {
		FullabshtmltreeContext _localctx = new FullabshtmltreeContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_fullabshtmltree);
		try {
			_localctx = new AHTMLTreeContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(110); ((AHTMLTreeContext)_localctx).rootNode = abshtmltree();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AbshtmltreeContext extends ParserRuleContext {
		public NodeclassdeclContext nodeClasses;
		public AbshtmltreeContext nodeChildren;
		public AbshtmltreeContext abshtmltree(int i) {
			return getRuleContext(AbshtmltreeContext.class,i);
		}
		public NodeclassdeclContext nodeclassdecl() {
			return getRuleContext(NodeclassdeclContext.class,0);
		}
		public List<AbshtmltreeContext> abshtmltree() {
			return getRuleContexts(AbshtmltreeContext.class);
		}
		public AbshtmltreeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_abshtmltree; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).enterAbshtmltree(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).exitAbshtmltree(this);
		}
	}

	public final AbshtmltreeContext abshtmltree() throws RecognitionException {
		AbshtmltreeContext _localctx = new AbshtmltreeContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_abshtmltree);
		int _la;
		try {
			setState(133);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(112); match(10);
				setState(113); match(9);
				setState(115);
				_la = _input.LA(1);
				if (_la==21) {
					{
					setState(114); ((AbshtmltreeContext)_localctx).nodeClasses = nodeclassdecl();
					}
				}

				setState(117); match(18);
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(118); match(10);
				setState(119); match(9);
				setState(121);
				_la = _input.LA(1);
				if (_la==21) {
					{
					setState(120); ((AbshtmltreeContext)_localctx).nodeClasses = nodeclassdecl();
					}
				}

				setState(123); match(17);
				setState(127);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==10) {
					{
					{
					setState(124); ((AbshtmltreeContext)_localctx).nodeChildren = abshtmltree();
					}
					}
					setState(129);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(130); match(15);
				setState(131); match(9);
				setState(132); match(17);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NodeclassdeclContext extends ParserRuleContext {
		public List<TerminalNode> ID() { return getTokens(AbstractHTMLParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(AbstractHTMLParser.ID, i);
		}
		public NodeclassdeclContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nodeclassdecl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).enterNodeclassdecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).exitNodeclassdecl(this);
		}
	}

	public final NodeclassdeclContext nodeclassdecl() throws RecognitionException {
		NodeclassdeclContext _localctx = new NodeclassdeclContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_nodeclassdecl);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(135); match(21);
			setState(136); match(11);
			setState(137); match(24);
			setState(141);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ID) {
				{
				{
				setState(138); match(ID);
				}
				}
				setState(143);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(144); match(24);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CssclassesrContext extends ParserRuleContext {
		public CssclassesrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cssclassesr; }
	 
		public CssclassesrContext() { }
		public void copyFrom(CssclassesrContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class CSSClassesContext extends CssclassesrContext {
		public ClassSetContext classSet() {
			return getRuleContext(ClassSetContext.class,0);
		}
		public CSSClassesContext(CssclassesrContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).enterCSSClasses(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof AbstractHTMLListener ) ((AbstractHTMLListener)listener).exitCSSClasses(this);
		}
	}

	public final CssclassesrContext cssclassesr() throws RecognitionException {
		CssclassesrContext _localctx = new CssclassesrContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_cssclassesr);
		try {
			_localctx = new CSSClassesContext(_localctx);
			enterOuterAlt(_localctx, 1);
			{
			setState(146); match(22);
			setState(147); classSet();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\34\u0098\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\3\2\7\2\32\n\2\f\2\16\2\35\13\2\3\2\5\2 \n\2\3\2\5\2#\n"+
		"\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3\61\n\3\3\4\3\4"+
		"\5\4\65\n\4\3\4\5\48\n\4\3\4\3\4\5\4<\n\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4"+
		"\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\7\4N\n\4\f\4\16\4Q\13\4\3\4\3\4\3"+
		"\4\3\4\7\4W\n\4\f\4\16\4Z\13\4\3\4\3\4\5\4^\n\4\3\5\3\5\3\5\3\6\3\6\3"+
		"\7\3\7\3\7\3\b\3\b\7\bj\n\b\f\b\16\bm\13\b\3\b\3\b\3\t\3\t\3\n\3\n\3\n"+
		"\5\nv\n\n\3\n\3\n\3\n\3\n\5\n|\n\n\3\n\3\n\7\n\u0080\n\n\f\n\16\n\u0083"+
		"\13\n\3\n\3\n\3\n\5\n\u0088\n\n\3\13\3\13\3\13\3\13\7\13\u008e\n\13\f"+
		"\13\16\13\u0091\13\13\3\13\3\13\3\f\3\f\3\f\3\f\2\2\r\2\4\6\b\n\f\16\20"+
		"\22\24\26\2\2\u00a6\2\33\3\2\2\2\4\60\3\2\2\2\6]\3\2\2\2\b_\3\2\2\2\n"+
		"b\3\2\2\2\fd\3\2\2\2\16g\3\2\2\2\20p\3\2\2\2\22\u0087\3\2\2\2\24\u0089"+
		"\3\2\2\2\26\u0094\3\2\2\2\30\32\5\4\3\2\31\30\3\2\2\2\32\35\3\2\2\2\33"+
		"\31\3\2\2\2\33\34\3\2\2\2\34\37\3\2\2\2\35\33\3\2\2\2\36 \5\20\t\2\37"+
		"\36\3\2\2\2\37 \3\2\2\2 \"\3\2\2\2!#\5\26\f\2\"!\3\2\2\2\"#\3\2\2\2#\3"+
		"\3\2\2\2$%\5\6\4\2%&\7\n\2\2&\'\7\5\2\2\'(\5\16\b\2()\7\t\2\2)\61\3\2"+
		"\2\2*+\5\6\4\2+,\7\n\2\2,-\7\25\2\2-.\5\16\b\2./\7\t\2\2/\61\3\2\2\2\60"+
		"$\3\2\2\2\60*\3\2\2\2\61\5\3\2\2\2\62\64\5\b\5\2\63\65\5\n\6\2\64\63\3"+
		"\2\2\2\64\65\3\2\2\2\65\67\3\2\2\2\668\5\f\7\2\67\66\3\2\2\2\678\3\2\2"+
		"\28^\3\2\2\29;\5\n\6\2:<\5\f\7\2;:\3\2\2\2;<\3\2\2\2<^\3\2\2\2=^\5\f\7"+
		"\2>?\7\4\2\2?^\5\6\4\2@A\7\31\2\2A^\5\6\4\2BC\7\b\2\2C^\5\6\4\2DE\7\17"+
		"\2\2E^\5\6\4\2FG\7\3\2\2G^\5\6\4\2HI\7\16\2\2I^\5\6\4\2JK\7\7\2\2KO\7"+
		"\22\2\2LN\5\6\4\2ML\3\2\2\2NQ\3\2\2\2OM\3\2\2\2OP\3\2\2\2PR\3\2\2\2QO"+
		"\3\2\2\2R^\7\26\2\2ST\7\20\2\2TX\7\22\2\2UW\5\6\4\2VU\3\2\2\2WZ\3\2\2"+
		"\2XV\3\2\2\2XY\3\2\2\2Y[\3\2\2\2ZX\3\2\2\2[^\7\26\2\2\\^\7\6\2\2]\62\3"+
		"\2\2\2]9\3\2\2\2]=\3\2\2\2]>\3\2\2\2]@\3\2\2\2]B\3\2\2\2]D\3\2\2\2]F\3"+
		"\2\2\2]H\3\2\2\2]J\3\2\2\2]S\3\2\2\2]\\\3\2\2\2^\7\3\2\2\2_`\7\3\2\2`"+
		"a\5\16\b\2a\t\3\2\2\2bc\5\16\b\2c\13\3\2\2\2de\7\16\2\2ef\5\16\b\2f\r"+
		"\3\2\2\2gk\7\22\2\2hj\7\33\2\2ih\3\2\2\2jm\3\2\2\2ki\3\2\2\2kl\3\2\2\2"+
		"ln\3\2\2\2mk\3\2\2\2no\7\26\2\2o\17\3\2\2\2pq\5\22\n\2q\21\3\2\2\2rs\7"+
		"\f\2\2su\7\13\2\2tv\5\24\13\2ut\3\2\2\2uv\3\2\2\2vw\3\2\2\2w\u0088\7\24"+
		"\2\2xy\7\f\2\2y{\7\13\2\2z|\5\24\13\2{z\3\2\2\2{|\3\2\2\2|}\3\2\2\2}\u0081"+
		"\7\23\2\2~\u0080\5\22\n\2\177~\3\2\2\2\u0080\u0083\3\2\2\2\u0081\177\3"+
		"\2\2\2\u0081\u0082\3\2\2\2\u0082\u0084\3\2\2\2\u0083\u0081\3\2\2\2\u0084"+
		"\u0085\7\21\2\2\u0085\u0086\7\13\2\2\u0086\u0088\7\23\2\2\u0087r\3\2\2"+
		"\2\u0087x\3\2\2\2\u0088\23\3\2\2\2\u0089\u008a\7\27\2\2\u008a\u008b\7"+
		"\r\2\2\u008b\u008f\7\32\2\2\u008c\u008e\7\33\2\2\u008d\u008c\3\2\2\2\u008e"+
		"\u0091\3\2\2\2\u008f\u008d\3\2\2\2\u008f\u0090\3\2\2\2\u0090\u0092\3\2"+
		"\2\2\u0091\u008f\3\2\2\2\u0092\u0093\7\32\2\2\u0093\25\3\2\2\2\u0094\u0095"+
		"\7\30\2\2\u0095\u0096\5\16\b\2\u0096\27\3\2\2\2\22\33\37\"\60\64\67;O"+
		"X]ku{\u0081\u0087\u008f";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}