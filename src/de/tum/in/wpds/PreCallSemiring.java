/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2009, Matthew Hague, Anthony Widjaja Lin, C.-H. Luke Ong
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// PreCallSemiring.java
// Author: Matthew Hague
//
// Represents a semiring call(R) as described below.  The construction of R is
// done in prependMethodCallPush in BDDSemiring.java.
//
// When dealing with a push rule during Pre* analysis we have
//
//      p a --R--> p' b c
//
// leads to p --a, R'--> q' for each
//
//      p' --b, R1--> q --c, R2--> q'
//
// where we go in stages.  When we see
//
//      q' --b, R1--> q
//
// we add a mock rule
//
//      p a --call(R'')--> q c (*)
//
// where R'' is the constraint of R to provide the correct local values to
// R1 (i.e. do the method call).  That is for a method call
//
//      myfun(e1, ..., en)
//
// where myfun has arguments av1, ..., avn, we have
//
//      R'' = (E)tmpi ( (E)locals . R1[tmp1/av1, ..., tmpn/avn] & tmpi = ei )
//
// that is, save the locals in myfun to temps, abstract them all because
// they're not in scope at the call site, use the temps to constrain the
// values of the variables at the call site, abstract the temps because
// they're no longer needed.
//
// Thus we're left with R''(G0,L0,G1) where G0 and L0 are the value of the
// local and globals before the function call, and G1 are the global values
// at q.
//
// From R'' we construct an expr semiring call(R'') that will label the mock
// rule (*).  Then when we see
//
//      q --c, R2-->q'
//
// we add
//
//      p --a, R'--> q'
//
// where R'(G0, L0, G1) = (E)G . R''(G0, L0, G) & R2(G, L0, G1).
//
// Note we need L0 in G2 because the local values transfer from a to c.

package de.tum.in.wpds;

import treeped.remoplatopds.NullSemiring;
import treeped.remoplatopds.BDDSemiring;

public class PreCallSemiring extends NullSemiring {

    BDDSemiring bdd;

    public PreCallSemiring(BDDSemiring bdd) {
        this.bdd = bdd;
    }

    public BDDSemiring getBDD() { return bdd; }

    public String toString() { return "PreCallSemiring(...)"; }
}
